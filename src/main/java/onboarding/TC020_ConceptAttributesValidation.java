package onboarding;

import utils.GenericWrappers;
import utils.GetAttributeWithBL;
import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC020_ConceptAttributesValidation extends ProjectWrappers {
//	String role = "1A";
//	String role = System.getenv("ROLE");
	GenericWrappers gw = new GenericWrappers();
	String role = gw.role;

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = (this.getClass().getSimpleName())+"_"+role;
		description="To verify the Concept attributes";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void conceptsCheck() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//*[@id='aud-disc-audience-concept-search-expandMore-icon']", "Concepts Filter Dropdown");
		Thread.sleep(2000);
		ArrayList<String> attList = new ArrayList<String>();
		List<WebElement> attObj = driver.findElements(By.xpath("//*[@id='aud-disc-audience-concept-search-component-popup']//div[contains(@class,'MuiGrid-grid-xs-4')]"));
		for(int j=0;j<attObj.size();j++) {
			attList.add(attObj.get(j).getText());
		}
		Collections.sort(attList);
		System.out.println(attList.size()+" Concepts are listed in the application");
		reportStep("Pass", attList.size()+" Concepts are listed in the application");

		ArrayList<String> excelAttList=GetAttributeWithBL.getAttributesWithBlacklist("Concepts", role);
		System.out.println(excelAttList.size()+" Concepts are listed from excel");
		reportStep("Pass", excelAttList.size()+" Concepts are listed from excel");
		ArrayList<String> temp = new ArrayList<String>();
		//		attList.remove(1);// Retain this for 'else' block debug
		if(attList.equals(excelAttList)) {
			System.out.println("\n***SUCCESS***\nConcepts extracted from Filter are same as concepts mentioned in requirements\n");
			reportStep("Pass","SUCCESS: Concepts extracted from Filter are same as concepts mentioned in requirements");
		}else {
			if(attList.size() >= excelAttList.size()) {
				temp=tempArrList(attList, temp);
				temp.removeAll(excelAttList);
				excelAttList.removeAll(attList);
				System.err.println("ERROR1: "+temp.size()+" Concepts attributes that are missing from Requirements - \n"+temp+"\n"+excelAttList.size()+" Requirement Concepts that are missing in application - \n"+excelAttList);
				reportStep("Fail",("ERROR1: "+temp.size()+" Concepts attributes that are missing from Requirements - \n"+temp+"\n"+excelAttList.size()+" Requirement Concepts that are missing in application - \n"+excelAttList));
			}else {
				temp=tempArrList(excelAttList, temp);
				temp.removeAll(attList);
				attList.removeAll(excelAttList);
				System.err.println("ERROR2: "+attList.size()+" Concepts attributes that are missing from Requirements - \n"+attList+"\n"+temp.size()+" Requirement Concepts that are missing in application - \n"+temp);
				reportStep("Fail",("ERROR2: "+attList.size()+" Concepts attributes that are missing from Requirements - \n"+attList+"\n"+temp.size()+" Requirement Concepts that are missing in application - \n"+temp));
			}
		}
	}
	public ArrayList<String> tempArrList(ArrayList<String> x, ArrayList<String> temp) {
		for(int i=0;i<x.size();i++) {
			temp.add(x.get(i));
		}
		return temp;
	}
}
