package onboarding;


import utils.GenericWrappers;
import utils.GetAttributeForRole;
import utils.GetAttributeWithBL;
import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AttributeOnboarding extends ProjectWrappers {
//	String role = "1A";
//	String role = System.getenv("ROLE");
	GenericWrappers gw = new GenericWrappers();
	String role = gw.role;

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = (this.getClass().getSimpleName())+"_"+role;
		description="To verify the Audience Filters Attributes";
		author="Aruna";
		category="Onboarding";
	}

	public WebDriver driver;

	@Test
	public void onboardingVerification() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
//		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			reportStep("Pass", "Audience Page is loaded");
			System.out.println("Audience Page is loaded");
		}else {
			reportStep("Fail","Audience Page not loaded");
			System.err.println("Audience Page not loaded");
		}


		//		List of elements which are case incorrect in attribute filter (attlist)
		String[] applicationString= {"Monthly Roaming Uc", "Monthly Vas Uc", "Nsat Indicator", "Nsat Probability", "Postpaid Arpu Oc 30days", "Postpaid Arpu Oc 60days", "Postpaid Arpu Oc 90days", "Postpaid Arpu Rc 30days", "Postpaid Arpu Rc 60days", "Postpaid Arpu Rc 90days", "Postpaid Arpu Uc 30days", "Postpaid Arpu Uc 60days", "Postpaid Arpu Uc 90days","Nps Consistent Detractor Indicator","Nps Consistent Non Detractor Indicator","Nps Journey Detractor Indicator","Nps Promo Indicator","Nps Rewards Indicator","Nps Topup Indicator","Lifestage Aa","Imsi Number Value"};
		ArrayList<String> applicationList=new ArrayList<String>(Arrays.asList(applicationString));

		//		List of elements which are case incorrect in requirement
		String[] requirementString= {"Monthly Roaming UC", "Monthly Vas UC", "NSAT Indicator", "NSAT Probability", "Postpaid Arpu OC 30days", "Postpaid Arpu OC 60days", "Postpaid Arpu OC 90days", "Postpaid Arpu RC 30days", "Postpaid Arpu RC 60days", "Postpaid Arpu RC 90days", "Postpaid Arpu UC 30days", "Postpaid Arpu UC 60days", "Postpaid Arpu UC 90days","NPS Consistent Detractor Indicator","NPS Consistent Non Detractor Indicator","NPS Journey Detractor Indicator","NPS Promo Indicator","NPS Rewards Indicator","NPS Topup Indicator","Lifestage AA","IMSI Number Value"};
		ArrayList<String> requirementList=new ArrayList<String>(Arrays.asList(requirementString));

		clickByXpath("//button[contains(@class,'MuiAutocomplete-popupIndicator')]/span", "Attribute Filter Dropdown");
		Thread.sleep(1000);

		//		Get attributes in FILTER
		System.out.println("\nFetching attributes from Attribute Filter in Audience");
		ArrayList<String> attList = new ArrayList<String>();
		List<WebElement> attObj = driver.findElements(By.xpath("//li[contains(@id,'autocomplete-filter-option-')]"));
		for(int j=0;j<attObj.size();j++) {
			attList.add(attObj.get(j).getText());
		}
		Collections.sort(attList);


		//		Get attributes in ADVANCED FILTER
		System.out.println("\nFetching attributes from Advanced Filter in Audience");
		ArrayList<String> advAttList = new ArrayList<String>();
		clickByXpath("//button/span[text()='More']", "More Button");
		clickByXpath("//span[text()='Advanced Filters']", "Advanced Filter");
		clickByXpath("//div[text()='Select field']", "Select Filter");
		attObj.clear();
		attObj = driver.findElements(By.xpath("//li[@role='option']"));
		for(int j=0;j<attObj.size();j++) {
			advAttList.add(attObj.get(j).getText());
		}
		Collections.sort(advAttList);


		//		GET ATTRIBUTES FROM PROFILES
		System.out.println("\nFetching attributes from Profiles");
		clickByXpath("//*[@id='user_5_']", "Profile Icon");//Comment this step for UAT execution
		Thread.sleep(2000);
		ArrayList<String> profileAttList = new ArrayList<String>();
		if(isDisplayedByXpath("//*[@id='profileSearchBar']", "Profiles Search Bar")) {
			System.out.println("Profile Search page is loaded");
			reportStep("Pass", "Profile Search page is loaded");
			enterByXpath("//input[@id='profileSearchBar']", new GenericWrappers().msisdnData(), "MSISDN value");//9173097496
			clickByXpath("//button[@id='profile-search-icon']", "Search icon");
			explicitWaitForVisibility("//button[@id='profile-res-summary']/span", "Summary");
			Thread.sleep(1000);
			if(isDisplayedByXpath("//button[@id='profile-res-summary']/span", "Profile Summary Text")) {
				reportStep("Pass", "MSISDN search is successful");
				System.out.println("MSISDN search is successful");
				clickByXpath("//*[@id='profile-res-details']/span", "Details");
				Thread.sleep(1000);
				List<WebElement> tablist = driver.findElements(By.xpath("//*[contains(@id,'profiles-details-tab-key-')]/span"));
				for(int i=0;i<tablist.size();i++) {
					try {
						if((i==4)&&(driver.findElement(By.cssSelector("div[class='MuiButtonBase-root MuiTabScrollButton-root MuiTabs-scrollButtons MuiTabs-scrollButtonsDesktop']>svg")).isDisplayed())) {
							clickByCSS("div[class='MuiButtonBase-root MuiTabScrollButton-root MuiTabs-scrollButtons MuiTabs-scrollButtonsDesktop']>svg", "ProfilesDetails Move Right Arrow");
						}
					} catch (Exception e) {
						System.out.println("Side scroll is not present");
					}
					driver.findElement(By.xpath("//button[@aria-controls='scrollable-auto-tabpanel-"+i+"']")).click();
					List<WebElement> temp1 = driver.findElements(By.xpath("//td[contains(@id,'profile-detailes-sub-tab-key-')]"));
					for(WebElement j:temp1) {
						profileAttList.add(j.getText());
					}
				}
				Collections.sort(profileAttList);
			}
			else if(isDisplayedByXpath("//div[text()='The number you entered is invalid']", "Invalid MSISDN Message")) {
				reportStep("Fail","MSISDN is not present. Please check MSISDN");
				System.out.println("MSISDN is not present. Please check MSISDN");
			}
			else {
				reportStep("Fail", "MSISDN search is unsuccessful");
				System.out.println("MSISDN search is unsuccessful");
			}
		}else {
			reportStep("Fail","Profile Search page not loaded");
			System.err.println("Profile Search page not loaded");
		}


		//		GET ATTRIBUTES FROM GLOSSARY
		System.out.println("\nFetching attributes from Attribute Glossary");
		try {
			clickByXpath("//*[@id='Path_1831_1_']", "Glossary Icon");
			Thread.sleep(2000);
			if(driver.findElement(By.xpath("//div[text()='Attribute Glossary Description']")).isDisplayed()) {
				System.out.println("Glossary page is loaded");
				reportStep("Pass", "Glossary page is loaded");
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","Glossary page not loaded");
			System.err.println("Glossary page not loaded");
		}

		//		Get Attributes list from glossary page
		List<WebElement> glossaryTablist = driver.findElements(By.xpath("//button[contains(@id,'vertical-tab')]/span"));
		ArrayList<String> glossaryAttList = new ArrayList<String>();
		for(int i=0;i<glossaryTablist.size();i++) {
			driver.findElement(By.xpath("//button[@id='vertical-tab-"+i+"']/span")).click();
			List<WebElement> glossaryTemp = driver.findElements(By.xpath("//tr/th/b"));
			for(int j=0;j<glossaryTemp.size();j++) {
				glossaryAttList.add(glossaryTemp.get(j).getText());
			}
		}
		Collections.sort(glossaryAttList);

		//		Get the attributes list for the role from excel
		ArrayList<String> excelAttList=GetAttributeForRole.getAttributesWOBlacklist("Attributes", role);
		System.out.println("\n***ATTRIBUTES***\n "+excelAttList.size()+" Attributes are listed from excel without Blacklist items");
		reportStep("Pass", "\n***ATTRIBUTES***\n "+excelAttList.size()+" Attributes are listed from excel without Blacklist items");

		System.out.println(attList.size()+" Attributes are listed in the Filter");
		reportStep("Pass", attList.size()+" Attributes are listed in the Filter");


		//		Attribute Validation from Attribute Filter

		ArrayList<String> temp = new ArrayList<String>();
		if(attList.equals(excelAttList)) {
			System.out.println("SUCCESS: Attributes extracted from Filter is same as attributes mentioned in requirements");
			reportStep("Pass","SUCCESS: Attributes extracted from Filter is same as attributes mentioned in requirements");
		}else {
			if(attList.size() >= excelAttList.size()) {
				temp=tempArrList(attList, temp);
				temp.removeAll(excelAttList);
				temp.removeAll(applicationList);
				excelAttList.removeAll(attList);
				excelAttList.removeAll(requirementList);
				if(temp.size()==0 && (excelAttList.size()==0)) {
					System.out.println("SUCCESS: Attributes are listed as expected in Attribute Filter");
					reportStep("Pass", "SUCCESS: Attributes are listed as expected in Attribute Filter");
				}
				else {
					System.err.println("ERROR1: "+temp.size()+" Filter attributes listed in the application but missing in Requirements:\n"+temp+"\n"+excelAttList.size()+" Attributes listed in requirment but missing in Filter:\n"+excelAttList);
					reportStep("Fail",("ERROR1: "+temp.size()+" Filter attributes listed in the application but missing in Requirements:\n"+temp+"\n"+excelAttList.size()+" Attributes listed in requirment but missing in Filter:\n"+excelAttList));
				}
			}
			else {
				temp=tempArrList(excelAttList, temp);
				temp.removeAll(attList);
				temp.removeAll(requirementList);
				attList.removeAll(excelAttList);
				attList.removeAll(applicationList);
				if(temp.equals(applicationList) && (excelAttList.equals(requirementList))) {
					System.out.println("SUCCESS: Filter Attributes are listed as expected in Attribute Filter");
					reportStep("Pass", "SUCCESS: Filter Attributes are listed as expected in Attribute Filter");
				}
				else {
					System.err.println("ERROR2: "+attList.size()+" Filter attributes listed in the application but missing in Requirements:\n"+attList+"\n"+temp.size()+" Attributes listed in requirment but missing in Filter:\n"+temp);
					reportStep("Fail",("ERROR2: "+attList.size()+" Filter attributes listed in the application but missing in Requirements:\n"+attList+"\n"+temp.size()+" Attributes listed in requirment but missing in Filter:\n"+temp));
				}
			}
		}
		//			Get the attributes list for the role from excel
		System.out.println();
		excelAttList.clear();
		excelAttList=GetAttributeForRole.getAttributesWOBlacklist("Attributes", role);
		System.out.println("\n***ADVANCED FILTER***\n "+excelAttList.size()+" Attributes are list from excel without Blacklist items");
		reportStep("Pass", "\n***ADVANCED FILTER***\n "+excelAttList.size()+" Attributes are list from excel without Blacklist items");
		System.out.println(advAttList.size()+" Attributes are listed in the Advanced Filter");
		reportStep("Pass", advAttList.size()+" Attributes are listed in the Advanced Filter");


		//			Attribute Validation from Advanced Attribute Filter

		temp.clear();
		if(advAttList.equals(excelAttList)) {
			System.out.println("SUCCESS: Attributes extracted from Advanced Filter is same as attributes mentioned in requirements");
			reportStep("Pass","SUCCESS: Attributes extracted from Advanced Filter is same as attributes mentioned in requirements");
		}else {
			if(advAttList.size() >= excelAttList.size()) {
				temp=tempArrList(advAttList, temp);
				temp.removeAll(excelAttList);
				temp.removeAll(applicationList);
				excelAttList.removeAll(advAttList);
				excelAttList.removeAll(requirementList);
				if(temp.size()==0 && (excelAttList.size()==0)) {
					System.out.println("SUCCESS: Attributes are listed as expected in Advanced Attribute Filter");
					reportStep("Pass", "SUCCESS: Attributes are listed as expected in Advanced Attribute Filter");
				}
				else {
					System.err.println("ERROR1: "+temp.size()+" Advanced Filter attributes listed in the application but missing in Requirements:\n"+temp+"\n"+excelAttList.size()+" Attributes listed in requirment but missing in Advanced Filter:\n"+excelAttList);
					reportStep("Fail",("ERROR1: "+temp.size()+" Advanced Filter attributes listed in the application but missing in Requirements:\n"+temp+"\n"+excelAttList.size()+" Attributes listed in requirment but missing in Advanced Filter:\n"+excelAttList));
				}
			}
			else {
				temp=tempArrList(excelAttList, temp);
				temp.removeAll(advAttList);
				temp.removeAll(requirementList);
				advAttList.removeAll(excelAttList);
				advAttList.removeAll(applicationList);
				if(temp.equals(applicationList) && (excelAttList.equals(requirementList))) {
					System.out.println("SUCCESS: Filter Attributes are listed as expected in Advanced Attribute Filter");
					reportStep("Pass", "SUCCESS: Filter Attributes are listed as expected in Advanced Attribute Filter");
				}
				else {
					System.err.println("ERROR2: "+advAttList.size()+" Advanced Filter attributes listed in the application but missing in Requirements:\n"+advAttList+"\n"+temp.size()+" Attributes listed in requirment but missing in Advanced Filter:\n"+temp);
					reportStep("Fail",("ERROR2: "+advAttList.size()+" Advanced Filter attributes listed in the application but missing in Requirements:\n"+advAttList+"\n"+temp.size()+" Attributes listed in requirment but missing in Advanced Filter:\n"+temp));
				}
			}
		}

		//			Get the attributes list for the role from excel for profiles
		ArrayList<String> excelProfileList=GetAttributeWithBL.getAttributesWithBlacklist("Attributes", role);
		System.out.println("\n***PROFILES***\n "+excelProfileList.size()+" attributes are listed from excel including Blacklisted items");
		reportStep("Pass", "\n***PROFILES***\n "+excelProfileList.size()+" attributes are listed from excel including Blacklisted items");

		@SuppressWarnings("unchecked")
		ArrayList<String> excelGlossaryList = (ArrayList<String>)excelProfileList.clone();


		//			Attribute validation in Profiles
		System.out.println(profileAttList.size()+" Attributes are listed in Profiles");
		reportStep("Pass", profileAttList.size()+" Attributes are listed in Profiles");

		ArrayList<String> profileTemp = new ArrayList<String>();
		if(profileAttList.equals(excelProfileList)) {
			System.out.println("SUCCESS: Attributes extracted from MSISDN Profile is same as attributes mentioned in requirements");
			reportStep("Pass","SUCCESS: Attributes extracted from MSISDN Profile is same as attributes mentioned in requirements");
		}else {
			if(profileAttList.size() >= excelProfileList.size()) {
				profileTemp=tempArrList(profileAttList, profileTemp);
				profileTemp.removeAll(excelProfileList);
				profileTemp.removeAll(requirementList);
				excelProfileList.removeAll(profileAttList);
				excelProfileList.removeAll(applicationList);
				if(profileTemp.size()==0 && (excelProfileList.size()==0)) {
					System.out.println("SUCCESS: Profile attributes are listed as expected in Profiles");
					reportStep("Pass", "SUCCESS: Profile attributes are listed as expected in Profiles");
				}
				else {
					System.err.println("ERROR1: "+profileTemp.size()+" MSISDN Profile attributes that are listed in the application but missing in requirements:\n"+profileTemp+"\n"+excelProfileList.size()+" Attributes that are listed in requirement but missing from Profile:\n"+excelProfileList);
					reportStep("Fail",("ERROR1: "+profileTemp.size()+" MSISDN Profile attributes that are listed in the application but missing in requirements:\n"+profileTemp+"\n"+excelProfileList.size()+" Attributes that are listed in requirement but missing from Profile:\n"+excelProfileList));
				}
			}
			else {
				profileTemp=tempArrList(excelProfileList, profileTemp);
				profileTemp.removeAll(profileAttList);
				profileTemp.removeAll(applicationList);
				profileAttList.removeAll(excelProfileList);
				profileAttList.removeAll(requirementList);
				if(profileTemp.size()==0 && profileAttList.size()==0) {
					System.out.println("SUCCESS: Profile attributes are listed as expected in Profiles");
					reportStep("Pass", "SUCCESS: Profile attributes are listed as expected in Profiles");
				}
				else {
					System.err.println("ERROR2: "+profileAttList.size()+" MSISDN Profile attributes that are listed in the application but missing in requirements:\n"+profileAttList+"\n"+profileTemp.size()+" Attributes that are listed in requirement but missing from Profile:\n"+profileTemp);
					reportStep("Fail",("ERROR2: "+profileAttList.size()+" MSISDN Profile attributes that are listed in the application but missing in requirements:\n"+profileAttList+"\n"+profileTemp.size()+" Attributes that are listed in requirement but missing from Profile:\n"+profileTemp));
				}
			}
		}


		//			Validation for Attribute Glossary
		Thread.sleep(2000);
		System.out.println("\n***GLOSSARY***\n "+glossaryAttList.size()+" Attributes are listed in the Glossary page");
		reportStep("Pass", "\n***GLOSSARY***\n "+glossaryAttList.size()+" Attributes are listed in the Glossary page");
		ArrayList<String> tempGlossary = new ArrayList<String>();
		if(glossaryAttList.equals(excelGlossaryList)) {
			System.out.println("SUCCESS: Attributes extracted from Glossary is same as attributes mentioned in requirements");
			reportStep("Pass","SUCCESS: Attributes extracted from Glossary is same as attributes mentioned in requirements");
		}else {
			if(glossaryAttList.size() >= excelGlossaryList.size()) {
				tempGlossary=tempArrList(glossaryAttList, tempGlossary);
				tempGlossary.removeAll(excelGlossaryList);
				tempGlossary.removeAll(requirementList);
				excelGlossaryList.removeAll(glossaryAttList);
				excelGlossaryList.removeAll(applicationList);
				if(tempGlossary.size()==0 && excelGlossaryList.size()==0) {
					System.out.println("SUCCESS: Glossary attributes are listed as expected in Glossary");
					reportStep("Pass", "SUCCESS: Glossary attributes are listed as expected in Glossary");	
				}else {
					System.err.println("ERROR1: "+tempGlossary.size()+" Glossary attributes that are missing from Requirements - "+tempGlossary+"\n"+excelGlossaryList.size()+" Requirement Attributes that are missing in Glossary - \n"+excelGlossaryList);
					reportStep("Fail",("ERROR1: "+tempGlossary.size()+" Glossary attributes that are missing from Requirements - "+tempGlossary+"\n"+excelGlossaryList.size()+" Requirement Attributes that are missing in Glossary - \n"+excelGlossaryList));
				}
			}else {
				tempGlossary=tempArrList(excelGlossaryList, tempGlossary);
				tempGlossary.removeAll(glossaryAttList);
				tempGlossary.removeAll(applicationList);
				glossaryAttList.removeAll(excelGlossaryList);
				glossaryAttList.removeAll(requirementList);
				if(tempGlossary.size()==0 && glossaryAttList.size()==0) {
					System.out.println("SUCCESS: Glossary attributes are listed as expected in Glossary");
					reportStep("Pass", "SUCCESS: Glossary attributes are listed as expected in Glossary");	
				}else {
					System.err.println("ERROR2: "+glossaryAttList.size()+" Glossary attributes that are missing from Requirements - \n"+glossaryAttList+"\n"+tempGlossary.size()+" Requirement Attributes that are missing in Glossary - \n"+tempGlossary);
					reportStep("Fail",("ERROR2: "+glossaryAttList.size()+" Glossary attributes that are missing from Requirements - \n"+glossaryAttList+"\n"+tempGlossary.size()+" Requirement Attributes that are missing in Glossary - \n"+tempGlossary));
				}
			}
		}

	}
	public ArrayList<String> tempArrList(ArrayList<String> x, ArrayList<String> temp) {
		for(int i=0;i<x.size();i++) {
			temp.add(x.get(i));
		}
		return temp;
	}
}
