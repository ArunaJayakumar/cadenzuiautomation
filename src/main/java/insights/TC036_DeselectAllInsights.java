package insights;

import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC036_DeselectAllInsights extends ProjectWrappers{

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify deselect all insight functionality";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void toggleMaxChart() throws InterruptedException, IOException, ParseException {


		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		String audienceSize1=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize1);
		System.out.println("Audience size is: "+audienceSize1);

		//Remove All Insights
		System.out.println("Removing Insights");
		clickByXpath("//*[@id='edit-audience-insights-moreIcon']", "Edit Audience Insight");
		clickByXpath("//*[@id='editInsight-dialogBox-deselectAll-btn']/span", "Deselect All");
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span", "Submit Edit Insight");
		
//		Verify Zero Insights
		explicitWaitForVisibility("//p[contains(text(),'Please select insight')]", "No Insight Info Message");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		if(isDisplayedByXpath("//p[contains(text(),'Please select insight')]", "No Insight Info Message")) {
			reportStep("Pass", "'Please select insight to get a view on audience distribution.' message is displayed");
			System.out.println("'Please select insight to get a view on audience distribution.' message is displayed");
			if(isDisplayedByXpath("//h4[@id='audience-sizeValue']", "Audience Size Value")) {
				String audienceSize2=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
				reportStep("Pass", "Audience size is: "+audienceSize2);
				System.out.println("Audience size is: "+audienceSize2);
				if(audienceSize1.equalsIgnoreCase(audienceSize2)) {
					reportStep("Pass", "Audience size before and after deselecting all insights is still the same as expected");
					System.out.println("Audience size before and after deselecting all insights is still the same as expected");
				}else {
					String errmsg = "Audience size before and after deselecting all insights has mismatch. Audience size before deselect = "+audienceSize1+" and Audience size after deselect = "+audienceSize2+".";
					reportStep("Fail", errmsg);
					System.err.println(errmsg);
				}
			}
		}
	}
}
