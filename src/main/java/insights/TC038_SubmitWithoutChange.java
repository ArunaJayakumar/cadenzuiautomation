package insights;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC038_SubmitWithoutChange extends ProjectWrappers{

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify cancel edit insight functionality";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void toggleMaxChart() throws InterruptedException, IOException, ParseException {


		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		//		Default insights name extraction
		List<WebElement> audienceInsightObj = driver.findElements(By.xpath("//div[@class='paper-chart-title']"));
		ArrayList<String> audienceInsightNames = new ArrayList<String>();
		for(int i=0;i<audienceInsightObj.size();i++) {
			audienceInsightNames.add(audienceInsightObj.get(i).getText());
		}
		ArrayList<String> defaultInsightNames = new ArrayList<String>(audienceInsightNames);
		audienceInsightNames.clear();
		audienceInsightObj.clear();
		System.out.println("Default insights list is: "+defaultInsightNames);
		reportStep("Pass", "Default insights list is: "+defaultInsightNames);

		//		Submit without Change Operation
		System.out.println("Checking Submit without change Operation");
		clickByXpath("//*[@id='edit-audience-insights-moreIcon']", "Edit Audience Insight");
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span", "Submit Edit Insight");

		//		Verify no change occurred in insights
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		audienceInsightObj = driver.findElements(By.xpath("//div[@class='paper-chart-title']"));
		for(int i=0;i<audienceInsightObj.size();i++) {
			audienceInsightNames.add(audienceInsightObj.get(i).getText());
		}
		System.out.println("Insights list after submit operation is: "+audienceInsightNames);
		reportStep("Pass", "Insights list after submit operation is: "+audienceInsightNames);
		if(audienceInsightNames.equals(defaultInsightNames)) {
			reportStep("Pass", "No changes occured in insights as expected");
			System.out.println("No changes occured in insights as expected");
		}else {
			reportStep("Fail", "Insights view changed even though user clicked on submit button without any changes");
			System.err.println("Insights view changed even though user clicked on submit button without any changes");
		}
	}
}
