package insights;

import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC033_MaxMinView extends ProjectWrappers{

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify Minimized and Maximized Insight";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void maxMinView() throws InterruptedException, IOException, ParseException {


		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		//		Maximize Affluence Insight
		clickByXpath("//*[@id='Affluence-maxView-icon']", "Maximize Affluence Insight");
		explicitWaitForVisibility("//*[@id='Affluence-maxChart-wrapper']//canvas", "Affluence Chart");
		isDisplayedByXpath("//*[@id='Affluence-maxChart-wrapper']//canvas", "Affluence Chart");
		isDisplayedByXpath("//*[@id='Affluence-maxChart-title']", "Affluence Chart Title");

		//		Minimize Affluence Insight
		clickByXpath("//button[@id='max-chart-dialog-closebtn']/span", "Close Affluence Insight");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//*[@id='Affluence-minChart-title']", "Minimized Affluence Insight");
	}
}
