package insights;

import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC034_ToggleMinBarChart extends ProjectWrappers{

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify toggling between Bar and Pie chart in Minimized view";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void toggleMinChart() throws InterruptedException, IOException, ParseException {


		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		//		Minimized Default Chart Check
		String buttonXpath = checkActiveToggleButton("selected by default");

		//		Toggle Chart
		clickByXpath(("//*[@id='Affluence-minChart-"+buttonXpath+"-icon']"), buttonXpath);
		Thread.sleep(1000);
		toggledChartDisplayCheck("Minimized Chart Toggle was successful");
		buttonXpath = checkActiveToggleButton("displayed successfully");

		//		Toggle back to default chart
		reportStep("Pass", "Toggling back to Default Chart");
		System.out.println("Toggling back to Default Chart");
		clickByXpath(("//*[@id='Affluence-minChart-"+buttonXpath+"-icon']"), buttonXpath);
		Thread.sleep(1000);
		buttonXpath = checkActiveToggleButton("displayed successfully");
		toggledChartDisplayCheck("Minimized Chart is toggled back to Default chart");
	}
	public void toggledChartDisplayCheck(String reportMsg) {
		if(isDisplayedByXpath("//*[@id='Affluence-minChart-wrapper']//canvas", "Affluence Chart")) {
			reportStep("Pass", reportMsg);
			System.out.println(reportMsg);
		}
	}
	public String checkActiveToggleButton(String reportActionMsg) {
		String BTCPieBtn_Class = driver.findElement(By.xpath("//*[@id='Affluence-minChart-toggle-pieBtn']")).getAttribute("class");
		String BTCBarBtn_Class = driver.findElement(By.xpath("//*[@id='Affluence-minChart-toggle-barBtn']")).getAttribute("class");
		String buttonXpath = "";
		if(BTCPieBtn_Class.contains("activeButton")) {
			buttonXpath = "barBtn";
			if(isDisplayedByXpath("//*[@id='Affluence-minChart-wrapper']//canvas", "Affluence Chart")) {
				reportStep("Pass", "Affluence Pie Chart is "+reportActionMsg);
				System.out.println("Affluence Pie Chart is "+reportActionMsg);
			}
		}else if(BTCBarBtn_Class.contains("activeButton")) {
			buttonXpath = "pieBtn";
			if(isDisplayedByXpath("//*[@id='Affluence-minChart-wrapper']//canvas", "Affluence Chart")) {
				reportStep("Pass", "Affluence Bar Graph is "+reportActionMsg);
				System.out.println("Affluence Bar Graph is "+reportActionMsg);
			}
		}else {
			reportStep("Fail", "Affluence Graph toggle buttons are missing");
			System.err.println("Affluence Graph toggle buttons are missing");
		}
		return buttonXpath;
	}
}
