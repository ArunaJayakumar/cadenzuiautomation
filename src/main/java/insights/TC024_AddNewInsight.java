package insights;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC024_AddNewInsight extends ProjectWrappers {

	@BeforeClass
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if newly added Insight is displayed along with default audience";
		author="Aruna";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void addAnInsights() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		//Verify if default insights are displayed
		List<WebElement> audienceInsightObj = driver.findElements(By.xpath("//div[@class='paper-chart-title']"));
		ArrayList<String> audienceInsightNames = new ArrayList<String>();
		for(int i=0;i<audienceInsightObj.size();i++) {
			audienceInsightNames.add(audienceInsightObj.get(i).getText());
		}

		System.out.println("Default insights list is: "+audienceInsightNames.toString());

		//Edit Audience Insight to add SIM LTE Capable Indicator View
		clickByCSS("div.MuiGrid-root.MuiGrid-item > svg.MuiSvgIcon-root.MuiSvgIcon-colorPrimary", "Edit Audience Insight");
		clickByXpath("//div[text()='Sim Lte Capable Indicator']", "SIM LTE Capable Indicator Insight");
		try {
			if(driver.findElement(By.xpath("//span[contains(@class,'Mui-checked')]/span/input[@name='sim_lte_capable_indicator']")).isSelected()) {
				System.out.println("SIM LTE Capable Indicator is checked");
				reportStep("Pass","SIM LTE Capable Indicator is checked");
				clickByXpath("//span[text()='Submit']", "Submit New Insight");//Submit the newly added insight
				Thread.sleep(2000);
			}
		} catch (Exception e) {
			System.err.println("SIM LTE Capable Indicator is not checked");
			reportStep("Fail","SIM LTE Capable Indicator is not checked");
		}

		//Verify explicitly if SIM LTE Capable Indicator insight is displayed
		isDisplayedByXpath("//div[text()='Sim Lte Capable Indicator']","SIM LTE Capable Indicator");
	}
}
