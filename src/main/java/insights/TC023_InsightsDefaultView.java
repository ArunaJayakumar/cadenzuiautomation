package insights;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC023_InsightsDefaultView extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if default Insights are displayed in default audience";
		author="Aruna";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void defaultInsights() throws InterruptedException, IOException, ParseException {

//		int defaultInsightCount = 4;
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		//Verify if default insights are displayed
		List<WebElement> audienceInsightObj = driver.findElements(By.xpath("//div[@class='paper-chart-title']"));
		ArrayList<String> audienceInsightNames = new ArrayList<String>();
		for(int i=0;i<audienceInsightObj.size();i++) {
			audienceInsightNames.add(audienceInsightObj.get(i).getText());
		}

		System.out.println("Default insights list is: "+audienceInsightNames.toString());

		ArrayList<String> devInsightsList=new ArrayList<String>();
		devInsightsList.add("Brand Type Code");
		devInsightsList.add("Customer Facing Unit Sub Type Code");
		devInsightsList.add("Marital Status Type Description");
		devInsightsList.add( "Payment Category Code");

		ArrayList<String> prodInsightsList = new ArrayList<String>();
		prodInsightsList.add("Age Bracket Name");
		prodInsightsList.add("Affluence");
		prodInsightsList.add("Lifestage");
		prodInsightsList.add("Globeone User Indicator");

		String title=driver.getCurrentUrl();

		if(title.contains("35.154.166.184") && devInsightsList.equals(audienceInsightNames)) {
			System.out.println("SUCCESS: All required default insights are displayed as expected in DEV environment");
			reportStep("Pass", "SUCCESS: All required default insights are displayed as expected in DEV environment");
		}
		else if(prodInsightsList.equals(audienceInsightNames)){
			System.out.println("SUCCESS: All required default insights are displayed as expected");
			reportStep("Pass", "SUCCESS: All required default insights are displayed as expected");		    	
		}
		else {
			System.err.println("ERROR: All required default insights are NOT displayed as expected");
			reportStep("Fail", "ERROR: All required default insights are NOT displayed as expected");		
		}
	}
}
