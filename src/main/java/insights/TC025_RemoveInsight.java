package insights;

import java.io.IOException;
import java.text.ParseException;
//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC025_RemoveInsight extends ProjectWrappers{

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if an existing Insights can be removed";
		author="Aruna";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void removeInsights() throws InterruptedException, IOException, ParseException {


		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		//Remove Affluence Insight
		clickByXpath("//*[@id='edit-audience-insights-moreIcon']", "Edit Audience Insight");
		clickByXpath("//div[@id='editInsight-Affluence-label']", "Uncheck Affluence");
		Thread.sleep(2000);

		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span", "Submit Edit Insight");//Submit the newly added insight
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		Thread.sleep(2000);
		//		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
			if(driver.findElement(By.xpath("//div[@id='editInsight-Affluence-label']")).isDisplayed()) {
				reportStep("Fail", "Affluence is not removed from Insights");
			}
		} catch (NoSuchElementException e) {
			reportStep("Pass", "Affluence is removed from Insights");
		}
	}
}
