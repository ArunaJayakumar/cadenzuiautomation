package sharedAudienceAttributes;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC269_DeleteLocalSharedCancel  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the user can delete the copy of his audience from the shared audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void deleteLocalShared() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true,true);
				
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//button[@title='Delete Rows']", "Delete Audience");
		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']","Cancel Delete");

		try {
			if(driver.findElement(By.xpath("//div[contains(text(),'You have successfully removed yourself from the shared audience')]")).isDisplayed())
				reportStep("Fail", "Shared user has deleted the shared audience copy from the shared audience section");
			else
				reportStep("Pass", "Audience is not deleted, as viewer cancelled the operation");
		}
		catch(Exception e) {
			reportStep("Pass", "Alert message was not displayed as the operation was cancelled");
		}	
		driver.close();
		driver=getDriver();
		invokeApp(testName, true);
		String Username_2 = GenericWrappers.Username_2;
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Shared Audiences");
		
		if(driver.findElement(By.xpath("//span[contains(text(),'"+Username_2+"')]")).isDisplayed())
			reportStep("Pass", "As the shared user cancelled the operation, changes are not reflected to the Owner");
		else
			reportStep("Fail", "Shared user deleted their copy of the audience, changes are reflected to the Owner");
	}
}
