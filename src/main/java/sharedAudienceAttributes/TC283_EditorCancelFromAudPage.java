package sharedAudienceAttributes;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC283_EditorCancelFromAudPage extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience is not shared to other users from the shared audience page when the operation is cancelled by users with the role of editor with reshare";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void editorCancelFromAudPage() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true,true);
				
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");

		Actions actions = new Actions(driver);
		
		clickByXpath("//span[contains(text(),'UpdateNewAud')]", "Click Audience Name");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(GenericWrappers.Username_3);
		Thread.sleep(1000);
		
		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();
		
		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']/span","Cancel");
		
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Updated the audience successfully')]")).isDisplayed())
				reportStep("Fail", "The audience is not shared from the shared audience section by user with editor with reshare role as user cancelled the operation");
			else
				reportStep("pass", "The audience is shared from the shared audience section by user with editor with reshare role");
		}
		catch(Exception e) {
			reportStep("pass","Shared Audience alert message is not shown as user cancelled the operation");
		}	
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");

		try {
			if(driver.findElement(By.xpath("//span[contains(text(),'"+GenericWrappers.Username_3+"')]")).isDisplayed())
				reportStep("Fail", "Audience shared to others by editor with reshare");
			else
				reportStep("Pass", "User3 is not present as the operation was cancelled by editor with reshare user");
			
		}
		catch(Exception e) {
			reportStep("pass","User3 is not present as the operation was cancelled by editor with reshare user");
		}
	}
}