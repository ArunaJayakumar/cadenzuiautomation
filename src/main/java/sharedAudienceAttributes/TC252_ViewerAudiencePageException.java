package sharedAudienceAttributes;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC252_ViewerAudiencePageException extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the share button is disabled for the audience where the user has the role of viewer in the shared audience page";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;
	
	

	@Test(groups= {"SmokeTest"})
	public void shareFromSavedSection() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true,true);
				
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'ShareExistingException')]", "Click Audience Name");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		Thread.sleep(1000);
		String viewer = driver.findElement(By.xpath("//div[@id='aud-disc-audience-search-share-aud-button']")).getAttribute("aria-disabled");
		if(viewer.equals("true")) {
			reportStep("pass", "The share button is disabled as the user only has viewer access");
		}
		else
			reportStep("fail","The share button is not disabled");
	}
}
