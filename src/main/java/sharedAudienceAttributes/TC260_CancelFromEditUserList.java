package sharedAudienceAttributes;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC260_CancelFromEditUserList extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the shared audience user list is not modified by clicking the edit icon from the shared audience section when the user cancels the operation";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void cancelFromEditUserList() throws InterruptedException ,IOException {
		
		driver=getDriver();
		String originalWindow = driver.getWindowHandle();	
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
		String Username_2 = GenericWrappers.Username_2;
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-save-aud-button']//span", "Save");
		for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
			driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
		enterByXpath("//input[@id='name']", "UserListAud", "Audience Name");

		clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span","Save Audience"); 
		Thread.sleep(2000);
		Actions actions = new Actions(driver);
		
		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(GenericWrappers.Username_3);
		Thread.sleep(1000);
		
		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();
		
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Share"); 

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "UserListAud", "Search Audiences");
		Thread.sleep(2000);
		//Add User to User List
		clickByXpath("//button[(@title='Edit')]", "Pencil Icon is Clicked");
		explicitWaitForVisibility("//*[name()='svg' and @data-icon='user-edit']","User List Edit Icon");
		WebElement clickEditIcon = driver.findElement(By.xpath("//*[name()='svg' and @data-icon='user-edit']"));
		
		actions.click(clickEditIcon).build().perform();
		
		Thread.sleep(1000);
		
		WebElement selectUser1=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser1.sendKeys(Username_2);
		Thread.sleep(1000);
		
		WebElement element1 = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element1).click().perform();
		
		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']/span","Cancel"); 
		
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Updated the audience successfully')]")).isDisplayed())
				reportStep("fail", "Changes made to the audience users");
			else
				reportStep("pass", "No changes made to the audience as the operation is cancelled by the owner");
		}
		catch(Exception e) {
			reportStep("pass", "No alert message is displayed as the operation is cancelled by the owner");
		}	
		TrackActivity(Username_2);
		
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
	
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
		
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has shared UserListAud with you as viewer')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("fail","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("pass","Notification is not visible as the user cancelled the operation");
		}
		
		cleanData();
	}	
	public void cleanData() throws InterruptedException,IOException {
		driver.close();
		driver=getDriver();
		invokeApp(testName, true);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "UserListAud", "Search Audiences");

		clickByXpath("//button[@title='Delete Rows']","Delete");
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']","Confirm Delete");
		Thread.sleep(2000);
		if(driver.findElement(By.xpath("//div[contains(text(),'Deleted the audience successfully')]")).isDisplayed())
			reportStep("Pass", "Successfully cleaned up test data");
		else
			reportStep("Fail", "Failed to delete test data");
		
	}

	
	public void TrackActivity(String Username_2) throws InterruptedException {
		
		clickByXpath("//span[contains(text(),'UserListAud')]", "Click Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		List<WebElement> trackUsername = driver.findElements(By.xpath("//span[text()='"+Username_2+"']"));
		List<WebElement> activity = driver.findElements(By.xpath("//span[text()='Shared']"));
		if(trackUsername.size()>0 && activity.size()>0) { 
			reportStep("fail","Shared Activity is present in the Track Activity Section");
		}
		else {
			reportStep("pass","Shared Activity is not visible as user did not share the audience");
			
		}
		clickAction.click(Trackicon).build().perform();
		
		Thread.sleep(2000);
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "UserListAud", "Search Audiences");
				
	}
}

