package sharedAudienceAttributes;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC257_SameAudNameException extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if error is thrown when the shared audience name is same as the existing saved audience name of the owner";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;	
	

	@Test(groups= {"SmokeTest"})
	public void sameAudNameException() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");
		clickByXpath("//button[(@title='Edit')]", "Edit Audience Name");
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath("//td[@class='MuiTableCell-root MuiTableCell-body MuiTableCell-alignLeft']//input[@value='ShareExistingException']"));
		element.sendKeys(Keys.CONTROL + "a");
		element.sendKeys(Keys.DELETE);
		element.sendKeys("CreateandShare");
		clickByXpath("//button[(@title='Save')]", "Save Audience Changes");
		Thread.sleep(2000);
		if(driver.findElement(By.xpath("//div[contains(text(),'A saved audience already exists with the same name') or contains(text(),'An error occured while updating the audience')]")).isDisplayed())
			reportStep("Pass", "User is not able to change the audience name same as the existing audience name");
		else
			reportStep("Fail", "Audience name is changed eventhough the name is same as the existing audience");

	}
}


