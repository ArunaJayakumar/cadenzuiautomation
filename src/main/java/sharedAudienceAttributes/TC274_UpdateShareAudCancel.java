package sharedAudienceAttributes;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC274_UpdateShareAudCancel  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the existing shared audience is not updated when the user cancels the operation";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;	

	@Test(groups= {"SmokeTest"})
	public void updateShareAudNoRace() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
		String Username_2 = GenericWrappers.Username_2;
//		Thread.sleep(500000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		updateSharedAudTestData(Username_2);
		
		Thread.sleep(1000);
		updateFilters(true);
		
		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-save-aud-button']//span", "Save");

		clickByXpath("//button[@id='saveAud-dialogModel-closeIconBtn']","Close Dialog"); 

		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Sucessfully updated the audience !')]")).isDisplayed())
				reportStep("fail", "The audience is updated");
			else
				reportStep("pass", "Audience is not updated as user cancelled the operation ");
		}
		catch(Exception e) {
			reportStep("pass","Update Audience alert message is not shown as user cancelled the operation");
		}

		TrackActivity();
		Thread.sleep(1000);
		//Login as 2nd user
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
//		Thread.sleep(5000);	
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
				
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has modified the EditedAudName.')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("fail","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("pass","Notification is not visible as the update operation was cancelled");
		}		
	}
	public void updateSharedAudTestData(String Username_2) throws InterruptedException {
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'EditedAudName')]", "Created Audience Name");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");			
		Thread.sleep(1000);
		clickByXpath("//button[@testid='userAccess']", "User role Popup");
		
		clickByXpath("//div[contains(text(),'Editor')]","Editor Role"); 
		Thread.sleep(1000);
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Share"); 	
	}
	public void TrackActivity() throws InterruptedException {
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		List<WebElement> activity = driver.findElements(By.xpath("//span[text()='Filters Added']"));
		if(activity.size()>0) { 
			reportStep("fail","Update Activity is present in the Track Activity Section");
		}
		else {
			reportStep("pass","Update Activity is not visible as user did not update the audience");
			
		}
		clickAction.click(Trackicon).build().perform();	
	}
}

