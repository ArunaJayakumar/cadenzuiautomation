package sharedAudienceAttributes;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC261_SearchSharedAud extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if an audience can be searched in the shared audien";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;	

	@Test(groups= {"SmokeTest"})
	public void searchSharedAud() throws InterruptedException ,IOException {
		
		driver=getDriver();	
		invokeApp(testName, true);
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		Thread.sleep(1000);
		List<WebElement> audDisplay = driver.findElements(By.xpath("//span[contains(text(),'EditedAudName')]"));
		if(audDisplay.size()>0)
			reportStep("Pass", "The searched audience is filtered in the shared audience section");
		else
			reportStep("fail", "Error while searching for an audience");
		
		WebElement element = driver.findElement(By.xpath("//input[@placeholder='Search Audiences']"));
		element.sendKeys(Keys.CONTROL + "a");
		element.sendKeys(Keys.DELETE);
		element.sendKeys("NoAudience");
		Thread.sleep(1000);
		List<WebElement> noRecordsDisplay = driver.findElements(By.xpath("//*[text()='No records to display']"));
		if(noRecordsDisplay.size()>0 )
			reportStep("pass", "'No records to display' message is displayed because no audience is present in the search audience section");
		else {
			reportStep("fail", "Error while searching for an audience");
		}
	}
}
