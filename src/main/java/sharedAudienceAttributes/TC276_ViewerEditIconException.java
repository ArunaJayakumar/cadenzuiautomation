package sharedAudienceAttributes;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC276_ViewerEditIconException extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the share button is disabled for the audience where the user has the role of viewer in the shared audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void viewerSharedSectionException() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true,true);
				
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");

		Thread.sleep(1000);
		List<WebElement> editButton = driver.findElements(By.xpath("//button[@title='Edit']")); 
		if((editButton.size()) == 0) {
			reportStep("pass", "Edit Icon is disabled for the viewer");
		}
		else {
			reportStep("fail", "Edit Icon is enabled for the viewer");
		}
		
		Thread.sleep(2000);
	} 
}