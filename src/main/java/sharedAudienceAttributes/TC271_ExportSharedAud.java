package sharedAudienceAttributes;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC271_ExportSharedAud extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience preview can be accessed post loading saved audience";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;
	

	@Test(groups= {"SmokeTest"})
	public void previewSharedAud() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'EditedAudName')]", "Created Audience Name");

		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			System.out.println("SUCCESS: Audience Discovery is loaded");
			reportStep("Pass", "SUCCESS: Audience Discovery is loaded");
			clickByXpath("//*[@id='aud-result-previewOption-tab']/span", "Audience Preview and Export");
			explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");
			//			Navigate to Preview to extract attributes
			clickByXpath("//*[@id='target-audience-saveOptionBtn']", "Export Popup Button");
			explicitWaitForVisibility("//*[@id='audience-export-dialog-title-Grid']", "Select attribute for Export Popup");
			if(isDisplayedByXpath("//*[@id='audience-export-dialog-title-Grid']", "Select attribute for Export Popup")) {
				System.out.println("SUCCESS: Audience Export Popup is loaded");
				reportStep("Pass", "SUCCESS: Audience Export Popup is loaded");
				clickByXpath("//*[@id='audience-preview-export-select-all']", "Select All Preview");
				clickByXpath("//*[@id='audience-export-dialog-content-operation-add-btn']/span", "Add Export");
				clickByXpath("//button[@id='audience-export-dialogBox-submit-btn']", "Export Button");
				Thread.sleep(1000);
				explicitWaitForVisibility("//div[text()='Download will begin shortly']", "Export InProgress Alert Message");
				if(isDisplayedByXpath("//div[text()='Download will begin shortly']", "Export InProgress Alert Message")) {
					System.out.println("SUCCESS: Audience Export was initiated successfully post loading shared audience");
					reportStep("Pass", "SUCCESS: Audience Export was initiated successfully post loading shared audience");

				}else {
					reportStep("Fail", "ERROR: Audience Export was not initiated successfully post loading shared audience");
					System.err.println("ERROR: Audience Export was not initiated successfully post loading shared audience");
				}
			}else {
				reportStep("Fail", "ERROR: Audience Export Popup is not loaded");
				System.err.println("ERROR: Audience Export Popup is not loaded");
			}
		}else {
			reportStep("Fail", "ERROR: Audience Discovery is not loaded");
			System.err.println("ERROR: Audience Discovery is not loaded");
		}		
	}
}