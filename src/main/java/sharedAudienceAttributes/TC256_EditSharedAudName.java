package sharedAudienceAttributes;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC256_EditSharedAudName extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the shared audience name can be edited by the owner from the shared audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;
	

	@Test(groups= {"SmokeTest"})
	public void editSharedAudName() throws InterruptedException ,IOException {
		
		driver=getDriver();	
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
		
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");
		clickByXpath("//button[(@title='Edit')]", "Edit Audience Name");
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//td[@class='MuiTableCell-root MuiTableCell-body MuiTableCell-alignLeft']//input[@value='ShareExistingException']"));
		element.sendKeys(Keys.CONTROL + "a");
		element.sendKeys(Keys.DELETE);
		element.sendKeys("EditedAudName");
		clickByXpath("//button[(@title='Save')]", "Save Audience Changes");
		Thread.sleep(1000);
		if(driver.findElement(By.xpath("//div[contains(text(),'Updated the shared audience successfully')]")).isDisplayed())
			reportStep("Pass", "The name of the audience is changed successfully");
		else
			reportStep("Fail", "Error while editing the Audience name");
			
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
	
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
		
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'renamed ShareExistingException to EditedAudName.')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("pass","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("fail","Notification is not visible");
		}
	}
}
