package sharedAudienceAttributes;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC263_SaveSharedAudException  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience is not saved when the audience is saved with the existing audience name of the user";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void saveSharedAudException() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);

//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'EditedAudName')]", "Created Audience Name");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		updateFilters(true);

		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-save-aud-button']//span", "Save");
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.CONTROL + "a");
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.DELETE);
		enterByXpath("//input[@id='name']", "CreateandShare", "Audience Name");

		clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span","Save Audience"); 
		Thread.sleep(1000);
		if(driver.findElement(By.xpath("//div[contains(text(),'A Saved Audience already exists with the same name !')]")).isDisplayed())
			reportStep("Pass", "User is not able to change the audience name same as the existing audience name");
		else
			reportStep("Fail", "Audience name is changed eventhough the name is same as the existing audience");		
	}
}

