package sharedAudienceAttributes;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC255_ViewSharedAudience extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the shared audience can be loaded from the shared audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void viewSharedAudience() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");
		clickByXpath("//span[contains(text(),'ShareExistingException')]", "Click Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		if(driver.findElement(By.xpath("//div[contains(text(),'Audience fetched successfully !')]")).isDisplayed())
			reportStep("Pass", "'Audience is fetched successully' message is displayed");
		else
			reportStep("Fail", "Success message is not displayed");
		try {
			if(isDisplayedByXpath("//p[text()='ShareExistingException']","Audience Name")) 
				reportStep("Pass","Shared Audience is loaded in the Audience page successfully");
			else 
				reportStep("Fail","Shared Audience Name is not displayed");
		} catch (NoSuchElementException e) {
			reportStep("Fail","Shared Audience is not loaded");
		}		
	}
}

