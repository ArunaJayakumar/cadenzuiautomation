package sharedAudienceAttributes;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
import utils.GenericWrappers;

public class TC246_CancelisClickedFromSharedSection extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if there are not changes made to users of an audience if the operation is cancelled by the owner from the shared audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void cancelisClickedFromSharedSection() throws InterruptedException ,IOException {
		
		driver=getDriver();	
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
		String Username_2 = GenericWrappers.Username_2;
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		addSharedAudTestData();

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");

		
		clickByXpath("//button[@title='Share Audience']", "Share");
		Actions actions = new Actions(driver);
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(Username_2);
				
		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();

		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']/span","Cancel"); 
		
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Updated the audience successfully')]")).isDisplayed())
				reportStep("fail", "Changes made to the audience users");
			else
				reportStep("pass", "No changes made to the audience as the operation is cancelled by the owner");
		}
		catch(Exception e) {
			reportStep("pass", "No alert message is displayed as the operation is cancelled by the owner");
		}	
		TrackActivity(Username_2);
		Thread.sleep(2000);
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);

				
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
		
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has shared ShareExistingException with you as viewer')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("fail","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("pass","Notification is not visible as the user is not added to the audience by the owner");
		}	
		cleanUsers();
	}
	
	public void cleanUsers() throws InterruptedException,IOException {
		driver.close();
		driver=getDriver();
		invokeApp(testName, true);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");

		
		clickByXpath("//button[@title='Share Audience']", "Share");
		clickByXpath("//button[@testid='userAccess']", "User role Popup");
		
		clickByXpath("//li[contains(text(),'Remove')]","Remove"); 
		
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Confirm Remove"); 	
	}
			
	public void addSharedAudTestData() throws InterruptedException {
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");

		
		Actions actions = new Actions(driver);
		
		clickByXpath("//button[@title='Share Audience']", "Share");
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(GenericWrappers.Username_3);

		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();
			
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 
			
	}
	public void TrackActivity(String Username_2) throws InterruptedException {
		
		clickByXpath("//span[contains(text(),'ShareExistingException')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		if(isDisplayedByXpath("//div[contains(text(),'No Recent Activity')]","No Activity"))
			reportStep("pass","Shared Activity is not visible as user did not share the audience");
		else
			reportStep("fail","Shared Activity is present in the Track Activity Section");
		clickAction.click(Trackicon).build().perform();
	}
	
}