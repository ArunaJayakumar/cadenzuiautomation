package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.time.Duration;
import java.util.List;
import java.util.Properties;
//import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.jboss.aerogear.security.otp.Totp;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;


public class GenericWrappers extends Reports{
	public WebDriver driver;
	public String testName, description;
	public String browser;
	public String author;
	public String category;
	public Method method;
	public int i=1;
	public String role=System.getenv("ROLE"); //System.getenv("ROLE");//Default Role = 1A
	public String env=System.getenv("ENV"); //System.getenv("ENV");//Default Env = prod
	public static String Username_1; //Value is set when user is logged in
	public static String Username_2; //Value is set when user is logged in
	public static String Username_3 = "Rajanish"; //Can be anyother username other than username 1 and 2
	public static String SetFilter;
	public static String UpdateFilter;
	public static String SetConcept;
	public static String UpdateConcept;
	
	

	//	DesiredCapabilities dc = DesiredCapabilities.chrome();

	//	dc.setBrowserName("chrome");

	public WebDriver getDriver() throws MalformedURLException {
		WebDriverManager.chromedriver().setup();//for local execution
		//		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver");
		//DesiredCapabilities dc = DesiredCapabilities.chrome();
		//dc.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--no-sandbox","--headless","incognito","--window-size=1920,1080");
		//		options.addArguments("--no-sandbox","incognito","--window-size=1920,1080"); //removed headless for local execution
		//			"--auto-open-devtools-for-tabs");
		//options.merge(dc);
		options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		//	driver=new RemoteWebDriver(new URL("http://localhost:9515"), DesiredCapabilities.chrome());
		driver=new ChromeDriver(options);        
		System.out.println("Driver created");
		return this.driver;
	}



	public void invokeApp(String testName,boolean flag,boolean ...seconduser) throws InterruptedException, IOException {
		System.out.println("Invoking application");		 
		Properties prop=new Properties();
		BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
		prop.load(reader);  
//		String env=prop.getProperty("env");
		String user,pwd;
		boolean user2= (seconduser.length == 1) ? seconduser[0] : false;
		if(!user2) {
			user=prop.getProperty(env+"username");
			pwd=prop.getProperty(env+"password"); 
		}
		else {
			user=prop.getProperty(env+"2username");
			pwd=prop.getProperty(env+"2password"); 
			
		}
		
		if(user.contains("zamuniyapppa")) {
			Username_1="Aravind Muniyappa";
			Username_2="Abhinav Rai";
		}else if (user.contains("zaarai")) {
			Username_1="Abhinav Rai";
			Username_2="Aravind Muniyappa";
		}

		String url=null;
		if(env.equalsIgnoreCase("dev"))
			url="https://35.154.181.199:8040/cadenz/audiences/audienceDiscovery";
		else if(env.equalsIgnoreCase("globedev"))
			url="https://uup-dev.globe.com.ph/cpp/audiences/audienceDiscovery";
		else if(env.equalsIgnoreCase("prod"))
			url="https://uup.globe.com.ph/cpp/audiences/audienceDiscovery";
		else if(env.equalsIgnoreCase("uat"))//included UAT env
			url="https://uup-uat.globe.com.ph/cpp/audiences/audienceDiscovery";
		System.out.println("URL is: "+url);

		try {
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
			reportStep("Pass", "Launching Cadenz in '"+env+"' environment");
			System.out.println("The browser launched successfully and loaded the url");
			System.out.println("Logging in as: "+user);
			reportStep("The browser launched successfully and loaded the url - "+url, "PASS");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("FAIL", "Unable to launch the browser due to unknown error");
			takeSnap(testName);		
		}		
		System.out.println("Starting test: "+testName);

		clickByXpath("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle");

		//		if(env.equalsIgnoreCase("dev"))
		//			clickByXpath("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle");
		//		if(env.equalsIgnoreCase("prod") || env.equalsIgnoreCase("uat"))//included UAT env
		//			clickByXpath("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle");
		//		if(env.equalsIgnoreCase("globedev")) {
		//			clickByXpath("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle");
		//		}


		Thread.sleep(2000);

		//		String pageSource=driver.getPageSource();
		//		System.out.println("source is: \n"+pageSource);
		//    	driver.findElement(By.xpath("(//span[text()='Continue with Google'])[2]")).click();
		//    	driver.findElement(By.xpath("//button[@id='details-button']")).click();
		//    	driver.findElement(By.xpath("//a[text()='Proceed to 35.154.166.184 (unsafe)']")).click();

		//    	THIS BLOCK OF CODE IS FOR JENKINS MASTER/HEADLESS EXECUTION

		//       	driver.findElement(By.xpath("//input[@id='identifierId']")).sendKeys(user);
		driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(user);
		System.out.println("Entering username: "+user);
		takeSnap(testName);
		driver.findElement(By.xpath("//input[@id='next']")).click();
		System.out.println("clicking on Next");
		takeSnap(testName);
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pwd);
		System.out.println("entered password");
		takeSnap(testName); 	

		driver.findElement(By.xpath("//input[@value='Sign in']")).click();
		System.out.println("clicked on password next");
		takeSnap(testName);

		//    	2STEP Verification for Jenkins
		if(env.equalsIgnoreCase("prod") || env.equalsIgnoreCase("uat")) { //included UAT env
			Thread.sleep(1000);
//			if (user.contains("zpgs") || user.contains("zaraghunath")) {
			if (user.contains("zpgs") || user.contains("zssmarank")) {
				if(isEnabledByXpath("//*[contains(text(),'Google Authenticator')]", "Google Authenticator")) {
					clickByXpath("//*[contains(text(),'Google Authenticator')]", "Google Authenticator");
				}else if(isEnabledByXpath("//*[@jsname='HbQSQd']/*[@jsname='c6xFrd']//*[text()='Try another way']", "Try another way popup")) {
					clickByXpath("//*[@jsname='HbQSQd']/*[@jsname='c6xFrd']//*[text()='Try another way']", "Try another way popup");
					clickByXpath("//div[contains(text(),'Get a verification code from the')]", "Google Authenticator");				
//					clickByXpath("//*[contains(text(),'Google Authenticator')]", "Google Authenticator");
				}else {
					clickByXpath("//*[text()='Try another way']", "Try another way single option");
					clickByXpath("//div[contains(text(),'Get a verification code from the')]", "Google Authenticator");				
//					clickByXpath("//*[contains(text(),'Google Authenticator')]", "Google Authenticator");
				}
			}
			if (user.contains("zaarai")) {
				List<WebElement> button = driver.findElements(By.id("skipChallenge"));
				if (!button.isEmpty()) {
					clickById("skipChallenge");
				}
				clickByXpath("//span[contains(text(),'Get a verification code from the')]", "Google Authenticator");				
			}			

			String sk="";
			if(user.contains("zaraghunath")) {
				sk="fikzqmzjgasj4ljz27pfjmwm356ap555";
			}else if (user.contains("zajayakumar")) {
				sk="fikzqmzjgasj4ljz27pfjmwm356ap555";
			}else if (user.contains("zpgs")) {
				sk="j7dodim63dsjzn6pjdkmqih3wangecmr";
			}else if (user.contains("zamuniyapppa")) {
				sk="gfgq454emyqs4rgzdlxhlgam5mzwf3bl";
			}else if (user.contains("zssmarank")) {
				sk="6yhsan7kchap42qyzuca6p5v7itxnsrb";
			}else if (user.contains("zaarai")) {
				sk="lger4awgwm4wdqhzb7kdcvwjc6n6sfg6";
			}
			Totp totp = new Totp(sk); // 2FA secret key
			//			Totp totp = new Totp("fikzqmzjgasj4ljz27pfjmwm356ap555"); // 2FA secret key
			String twoFactorCode = totp.now(); //Generated 2FA code here
			//			clickByXpath("//span[contains(text(),'Get a verification code from the')]", "Google Authenticator");
			enterByIdTab("totpPin", twoFactorCode, "OTP");
			Thread.sleep(1000);
			//		String pageSource=driver.getPageSource();
			//		System.out.println("source is: \n"+pageSource);	
			driver.findElement(By.id("submit")).click();
			try {
				if(driver.findElement(By.xpath("//div/*[contains(text(),'Wrong code. Try again.')]")).isDisplayed()) {
					reportStep("Fail", "ERROR: Wrong OTP. Please Enter Correct OTP.");
					System.err.println("ERROR: Wrong OTP. Please Enter Correct OTP.");
				}
			} catch (Exception e) {
				reportStep("Pass", "Correct OTP was used");
				System.out.println("Correct OTP was used");
			}
		}

/*

		//THIS BLOCK OF CODE IS FOR NORMAL EXECUTION
		enterByXpath("//input[@id='identifierId']", user, "Username");
		//    	driver.findElement(By.xpath("//input[@id='identifierId']")).sendKeys(user);
		//    	takeSnap(testName);
		System.out.println("Entering username: "+user);
		clickByXpath("//div[@id='identifierNext']", "Next");
		//    	driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
		Thread.sleep(2000);
		//    	enterByXpath("//input[@name='password']", pwd,"Password");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pwd);
		takeSnap(testName);
		//    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
		clickByXpath("//div[@id='passwordNext']", "Next");
		*/



		/*
		//    	2STEP Verification for local
		if(env.equalsIgnoreCase("prod") || env.equalsIgnoreCase("uat")) { //included UAT env
			List<WebElement> button = driver.findElements(By.xpath("//div[contains(text(),'Get a verification code from the')]"));
			if (!button.isEmpty()) {
				for(WebElement option : button) {
					JavascriptExecutor exec1=(JavascriptExecutor)driver;
					exec1.executeScript("arguments[0].click();", option);
				}
			}
			String sk="";
			if(user.contains("zaraghunath")) {
				sk="fikzqmzjgasj4ljz27pfjmwm356ap555";
			}else if (user.contains("zajayakumar")) {
				sk="fikzqmzjgasj4ljz27pfjmwm356ap555";
			}else if (user.contains("zamuniyapppa")) {
				sk="gfgq454emyqs4rgzdlxhlgam5mzwf3bl";
			}else if (user.contains("zssmarank")) {
				sk="6yhsan7kchap42qyzuca6p5v7itxnsrb";
			}
			Totp totp = new Totp(sk); // 2FA secret key
//			Totp totp = new Totp("fikzqmzjgasj4ljz27pfjmwm356ap555"); // 2FA secret key
			String twoFactorCode = totp.now(); //Generated 2FA code here
			Thread.sleep(2000);
//			OTP value is returned from getTwoFactor method
			enterByIdTab("totpPin", twoFactorCode, "OTP");
			driver.findElement(By.id("totpNext")).click();
			try {
				if(driver.findElement(By.xpath("//div/*[contains(text(),'Wrong code. Try again.')]")).isDisplayed()) {
					reportStep("Fail", "ERROR: Wrong OTP. Please Enter Correct OTP.");
					System.err.println("ERROR: Wrong OTP. Please Enter Correct OTP.");
				}
			} catch (Exception e) {
				reportStep("Pass", "Correct OTP was used");
				System.out.println("Correct OTP was used");
			}
		}
		*/


		//    	driver.findElement(By.xpath("//span[text()='Back']")).click();
		//    	Thread.sleep(20000);
		//    	takeSnap(testName);
		/*}
    	catch(Exception e) {
    		reportStep("Fail","Not able to launch Cadenz");
    	}*/

	}


	public void enterByXpath(String xpathValue, String data, String fieldName) {
		testName = this.getClass().getSimpleName();
		try {
			driver.findElement(By.xpath(xpathValue)).sendKeys(data);
			System.out.println("The value '"+data+"' is entered in the field '"+fieldName);
			reportStep("Pass","The value '"+data+"' is entered in the field '"+fieldName);
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			System.err.println("The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
			takeSnap(testName);
		}
	}


	public void clickByXpath(String xpathVal, String fieldName) {
		testName = this.getClass().getSimpleName();
		try {
			driver.findElement(By.xpath(xpathVal)).click();
			reportStep("Pass","The field '"+fieldName+"' is clicked");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
			takeSnap(testName);
		}

	}

	public void clickByXpathNoSnap(String xpathVal, String fieldName) {

		try {
			driver.findElement(By.xpath(xpathVal)).click();
			reportStep("Pass","The field with XPath '"+xpathVal+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
		}
	}

	public String getTextByXpath(String xpathVal, String fieldName) {
		String value=null;
		testName = this.getClass().getSimpleName();
		try {
			value=driver.findElement(By.xpath(xpathVal)).getText();
			reportStep("Pass","The text value at the element '"+fieldName+"' is: '"+value+"' ");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
			takeSnap(testName);
		}	

		return value;
	}

	public boolean isDisplayedByXpath(String xpathVal, String fieldName) {
		boolean value=false;
		testName = this.getClass().getSimpleName();
		try {
			value=driver.findElement(By.xpath(xpathVal)).isDisplayed();
			if(value) {
				reportStep("Pass","The element '"+fieldName+"' is displayed");
				takeSnap(testName);
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
			takeSnap(testName);
		}	

		return value;
	}

	public boolean isEnabledByXpath(String xpathVal, String fieldName) {
		boolean value=false;
		testName = this.getClass().getSimpleName();
		try {
			value=driver.findElement(By.xpath(xpathVal)).isEnabled();
			if(value) {
				reportStep("Pass","The element '"+fieldName+"' is enabled");
				takeSnap(testName);
			}else {
				reportStep("Pass","The element '"+fieldName+"' is not enabled");
				takeSnap(testName);
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
			takeSnap(testName);
		}
		return value;
	}

	public boolean isSelectedByXpath(String xpathVal, String fieldName) {
		boolean value=false;
		testName = this.getClass().getSimpleName();
		try {
			value=driver.findElement(By.xpath(xpathVal)).isSelected();
			if(value) {
				reportStep("Pass","The element '"+fieldName+"' is selected");
				takeSnap(testName);
			}else {
				reportStep("Pass","The element '"+fieldName+"' is not selected");
				takeSnap(testName);
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
			takeSnap(testName);
		}
		return value;
	}

	public void clickByCSS(String CSSVal, String fieldName) {
		testName = this.getClass().getSimpleName();
		try {
			driver.findElement(By.cssSelector(CSSVal)).click();
			reportStep("Pass","The field '"+fieldName+"' is clicked");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
		}
	}

	public void clickById(String id) {

		try {
			driver.findElement(By.id(id)).click();
			takeSnap(testName);
			reportStep("Pass","The field with id '"+id+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
			takeSnap(testName);
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id);
			takeSnap(testName);
		}

	}

	public void explicitWaitForVisibility(String xpathVal, String fieldName) {
		testName = this.getClass().getSimpleName();
		int seconds = 30;
		try {
			WebDriverWait expwait = new WebDriverWait(driver,Duration.ofSeconds(seconds));
			expwait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathVal)));
			reportStep("Pass","The field '"+fieldName+"' is visible");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (TimeoutException e) {
			reportStep("Fail","The field '"+fieldName+"' was not displayed within "+seconds+" seconds");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
			takeSnap(testName);
		}
	}


	public void closeAllBrowsers() {
		try { 
			driver.close();	
			System.out.println("Closing browser");
			reportStep("Pass","Browser is closed successfully");
			driver.quit();
		}
		catch(NoSuchWindowException e) {
			reportStep("Fail","No window/windows found");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error while closing the browser window/windows");
		}
	}




	/*public void verifyTextById(String id, String text) {
		try {
			String textValue=driver.findElementById(id).getText();
			if(textValue.equals(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The expected text value at the field '"+id+"' is '"+text+"' but the actual text is '"+textValue+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id);
		}
		}

	public void verifyTextByXpath(String xpath, String text) {
		try {
			String textValue=driver.findElement(xpath).getText();
			if(textValue.equals(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The expected text value at the field '"+xpath+"' is '"+text+"' but the actual text is '"+textValue+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with XPath '"+xpath);
		}
}

	public void verifyTextContainsByXpath(String xpath, String text) {

		try {
			String textValue=driver.findElement(xpath).getText();
			if(textValue.contains(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The actual text '"+textValue+"' does not contain the expected text '"+text+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is no longer available in the WebPage");
		}  catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with XPath '"+xpath);
		}
	}



	public void clickByClassName(String classVal) {

		try {
			driver.findElementByClassName(classVal).click();
			reportStep("Pass","The field with ClassName '"+classVal+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with ClassName '"+classVal);
		}

	}

	public void clickByName(String name) {
		try {
			driver.findElementByName(name).click();
			reportStep("Pass","The field with Name '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Name '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Name '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Name '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Name '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Name '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Name '"+name);
		}

	}

	public void clickByLink(String name) {
		try {
			driver.findElementByLinkText(name).click();
			reportStep("Pass","The field with Link '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Link '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Link '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Link '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Link '"+name);
		}

	}

	public void clickByLinkNoSnap(String name) {
		try {
			driver.findElementByLinkText(name).click();
			reportStep("Pass","The field with Link '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Link '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Link '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Link '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Link '"+name);
		}


	}


	public String getTextById(String idVal) {
		String textValue=null;
		try {
			 textValue=driver.findElementById(idVal).getText();
				reportStep("Pass","The text value at the id '"+idVal+"'is "+textValue+"' ");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+idVal);
		}

		return textValue;

	}*/

	/*public void selectVisibileTextById(String id, String value) {
		try {
		WebElement element=driver.findElementById(id);
		Select elementObj=new Select(element);
		elementObj.selectByVisibleText(value);
		reportStep("Pass","The dropdown with id '"+id+"' is found and the value '"+value+"' is selected from the dropdown.");
		}
		catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id);
		}
	}

	public void selectIndexById(String id, int value) {

		try {
			WebElement element=driver.findElementById(id);
			Select elementObj=new Select(element);
			elementObj.selectByIndex(value);
			reportStep("Pass","The dropdown with id '"+id+"' is found and the value at the position'"+value+"' is selected from the dropdown.");
			}
			catch (NoSuchElementException e) {
				reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
			} catch (ElementNotVisibleException e) {
				reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
			} catch (ElementNotInteractableException e) {
				reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
			} catch (ElementNotSelectableException e) {
				reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
			} catch (StaleElementReferenceException e) {
				reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
			} catch (WebDriverException e) {
				reportStep("Fail","Not able to find/interact with the element with Id '"+id);
			}

	}

	public void switchToParentWindow() {
		try {
		Set<String> winIDs=driver.getWindowHandles();
		for(String winID:winIDs) {
			driver.switchTo().window(winID);
			break;
		}
		reportStep("Pass","Switched to the last available Window");
		}
		catch (NoSuchWindowException e) {
			reportStep("Fail","The Parent window was not availble");
		}
		catch(NoSuchSessionException e) {
			reportStep("Fail","The session for Parent window is not available");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Problem encountered while switching to the Parent window");
		}

	}

	public void switchToLastWindow() {

		try {
		Set<String> winIDs=driver.getWindowHandles();
		for(String winID:winIDs) {
			driver.switchTo().window(winID);
		}
		reportStep("Pass","Switched to the last available Window");
		}
		catch (NoSuchWindowException e) {
			reportStep("Fail","The window was not availble");
		}
		catch(NoSuchSessionException e) {
			reportStep("Fail","The session is not available");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Problem encountered while switching to the last window");
		}


	}

	public void acceptAlert() {

		try {
			driver.switchTo().alert().accept();
			reportStep("Pass","The alert is accepted successfully");
		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while accepting the Alert");
		}

	}

	public void dismissAlert() {

		try {
			driver.switchTo().alert().dismiss();
			reportStep("Pass","The alert is dismissed successfully");

		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while dismisisng the Alert");
		}

	}

	public String getAlertText() {
		String alertMessage=null;

		try {
			alertMessage= driver.switchTo().alert().getText();
			reportStep("Pass","The alert message is '"+alertMessage+"'");
		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while switching to the Alert");
		}
		return alertMessage;
	}*/

	public void takeSnap(String testName) {
		//		 i=0;
		try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String filepath="./screenshots/"+testName+"/screen"+i+".jpeg";
			File dest= new File(filepath);
			FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
			reportStep("Fail","Error while taking screenshot");

		} catch (IOException e) {
			reportStep("Fail","Error while creating/copying the image file");
		}
		i++;
	}

	public void closeBrowser() {
		try { 
			driver.close();	
			reportStep("Pass","The browser is closed successfully");
		}
		catch(NoSuchWindowException e) {
			reportStep("Fail","No window found");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error while closing the browser");
		}
	}
	public void enterByIdTab(String idValue, String data,String fieldName) {
		try {
			driver.findElement(By.id(idValue)).sendKeys(data);
			reportStep("Pass","The value '"+data+"' is entered in the field '"+fieldName+" with id '"+idValue+"'");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+idValue);
		}


	}

	public String msisdnData() {
		String msisdn = "";
		if(env.equalsIgnoreCase("prod")) {
			msisdn = "9173097496";
		}else if (env.equalsIgnoreCase("uat")) {
			msisdn = "9171290693";
		}else if (env.equalsIgnoreCase("dev")) {
			msisdn = "1234567890";
		}
		return msisdn;
	}

	/*	public void selectIndexByXpath(String xpathValue, int index) {
	try {
		WebElement element=driver.findElement(xpathValue);
		Select obj=new Select(element);
		obj.selectByIndex(index);
		reportStep("Pass","IRCTC Lounge Element' is clicked and the dropdown value at index '"+index+"' is selected");
	}
		catch(NoSuchElementException e) {
			reportStep("Fail","IRCTC Executive lounge Element' not found");
		}
		catch(ElementNotVisibleException e)
		{
			reportStep("Fail","Element' not Visible");
		}
		catch(ElementNotInteractableException e) {
			reportStep("Fail","Element' not interactable");
		}
		catch(ElementNotSelectableException e)
		{
			reportStep("Fail","Element' not Selectable");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error while clicking on element'");
		}


	}

	public void selectVisibleTextByXpath(String xpathValue, String text) {
		try {
		WebElement element=driver.findElement(xpathValue);
		Select obj=new Select(element);
		obj.selectByVisibleText(text);
		reportStep("Pass","Element' is clicked and the dropdown value with value '"+text+"' is selected");
		}
		catch(NoSuchElementException e) {
			reportStep("Fail","Element' not found");
		}
		catch(ElementNotVisibleException e)
		{
			reportStep("Fail","Element' not Visible");
		}
		catch(ElementNotInteractableException e) {
			reportStep("Fail","Element' not interactable");
		}
		catch(ElementNotSelectableException e)
		{
			reportStep("Fail","Element' not Selectable");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error while clicking on element'");
		}




	}*/
	public void selectFilters(boolean isAttribute) throws InterruptedException {
		
		if(isAttribute) {
			clickByXpath("//button[@id='aud-disc-audience-attr-clear-btn']","Clear Audience Filters");

			clickByXpath("//*[@id='aud-disc-filter-dropdown']", "Attribute Filter Dropdown");
			Thread.sleep(1000);
			clickByXpath("//li[text()='Income Segment Code']", "Income Segment Code");
			clickByXpath("//*[@id='condition-select-outlined']", "Condition Dropdown");
			clickByXpath("//li[text()='=='] | //li[text()='Equals']", "Equal Condition");
			clickByXpath("//*[@id='attribute-select-outlined']", "Attribute Value Dropdown");
			clickByXpath("//li[text()='AB']", "Attribute Value");
			clickByCSS("div[id='aud-disc-audience-search-add-button']>svg", "Add Filter");
			Thread.sleep(2000);
			clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");
			try {
				if(driver.findElement(By.xpath("//span[contains(text(),'Income Segment Code')]")).isDisplayed()) {
					reportStep("Pass","Attribute Filter added successfully");
					System.out.println("Attribute Filter added successfully");
				}

			} catch (NoSuchElementException e) {
				reportStep("Fail","Attribute Filter was not added");
				System.err.println("Attribute Filter was not added");
			}
			SetFilter = "income_segment_code";
		}
		else {
			explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
			clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
			if(driver.findElements(By.xpath("//button[@id='aud-disc-audience-attr-clear-btn']")).size()>0)
				clickByXpath("//button[@id='aud-disc-audience-attr-clear-btn']","Clear Audience Filters");
			clickByXpath("//*[@id='aud-disc-audience-concept-search-component']", "Concept Filter Dropdown");
			Thread.sleep(1000);
			clickByXpath("//li[contains(.,'Active Subscription')]", "Active Subscription");
			clickByXpath("//button[@id='aud-disc-audience-concept-search-btn']/span", "Search Button");
			try {
				if(driver.findElement(By.xpath("//span[contains(text(),'Active Subscription')]")).isDisplayed()) {
					reportStep("Pass","Concepts Filter added successfully");
					System.out.println("Concepts Filter added successfully");
				}

			} catch (NoSuchElementException e) {
				reportStep("Fail","Concepts Filter was not added");
				System.err.println("Concepts Filter was not added");
			}
			SetConcept = "active_subscription";
		}	
	}
	
	public void updateFilters(boolean isAttribute) throws InterruptedException {
		if(isAttribute) {
			clickByXpath("//button[@id='aud-disc-audience-attr-clear-btn']","Clear Audience Filters");

			clickByXpath("//*[@id='aud-disc-filter-dropdown']", "Attribute Filter Dropdown");
			Thread.sleep(1000);
			clickByXpath("//li[text()='Digital Creative Bucket']", "Digital Creative Bucket");
			clickByXpath("//*[@id='condition-select-outlined']", "Condition Dropdown");
			clickByXpath("//li[text()='=='] | //li[text()='Equals']", "Equal Condition");
			clickByXpath("//*[@id='attribute-select-outlined']", "Attribute Value Dropdown");
			clickByXpath("//li[text()='LOW']", "Attribute Value");
			clickByCSS("div[id='aud-disc-audience-search-add-button']>svg", "Add Filter");
			Thread.sleep(2000);
			clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");
			try {
				if(driver.findElement(By.xpath("//span[contains(text(),'Digital Creative Bucket')]")).isDisplayed()) {
					reportStep("Pass","Filter added successfully");
					System.out.println("Filter added successfully");
				}

			} catch (NoSuchElementException e) {
				reportStep("Fail","Filter was not added");
				System.err.println("Filter was not added");
			}
			UpdateFilter = "digital_creative_bucket";
		}
		else {
			explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
			clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
			if(driver.findElements(By.xpath("//button[@id='aud-disc-audience-attr-clear-btn']")).size()>0)
				clickByXpath("//button[@id='aud-disc-audience-attr-clear-btn']","Clear Audience Filters");
			clickByXpath("//*[@id='aud-disc-audience-concept-search-component']", "Concept Filter Dropdown");
			Thread.sleep(1000);
			clickByXpath("//li[contains(.,'Gen Z')]", "Gen Z");
			clickByXpath("//button[@id='aud-disc-audience-concept-search-btn']/span", "Search Button");
			try {
				if(driver.findElement(By.xpath("//span[contains(text(),'Gen Z')]")).isDisplayed()) {
					reportStep("Pass","Concepts Filter added successfully");
					System.out.println("Concepts Filter added successfully");
				}

			} catch (NoSuchElementException e) {
				reportStep("Fail","Concepts Filter was not added");
				System.err.println("Concepts Filter was not added");
			}
			UpdateConcept = "gen_z";
		}	
	}
}
