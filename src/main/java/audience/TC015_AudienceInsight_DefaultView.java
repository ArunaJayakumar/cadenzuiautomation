package audience;


//import java.io.File;

import utils.ProjectWrappers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

//import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC015_AudienceInsight_DefaultView extends ProjectWrappers {

	public  TC015_AudienceInsight_DefaultView() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the Audience Insight Graphs";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void dateCheck() throws InterruptedException, IOException, ParseException {
		int audienceInsightDefCount = 4;
		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(2000);
		
		try {
			driver.findElement(By.xpath("//h6[text()='Audience Size']")).isDisplayed();
		} catch (NoSuchElementException e) {
			reportStep("Fail","Audience Insight not loaded");
		}
		try {
			driver.findElement(By.xpath("//div[@class='search-tag-main']/div/span[contains(text(),'payment category code')]")).isDisplayed();
		} catch (NoSuchElementException e) {
			reportStep("Fail","'Payment category code' default filter is missing");
		}
		List<WebElement> audienceInsightObj = driver.findElements(By.xpath("//div[@class='paper-chart-title']"));
		ArrayList<String> audienceInsightNames = new ArrayList<String>();
		for(int i=0;i<audienceInsightObj.size();i++) {
			audienceInsightNames.add(audienceInsightObj.get(i).getText());
		}
		ArrayList<String> missingInsightNames = new ArrayList<String>();
		int flag=0;
		for(int i=0;i<audienceInsightDefCount;i++) {
			if(!(audienceInsightNames.contains(missingDefaultInsight(i)))) {
				missingInsightNames.add(missingDefaultInsight(i));
				flag++;
			}
		}
		if(flag==0) {
			System.out.println("All Default Insights are present");
			reportStep("Pass","All Default Insights are present");
		}else {
			System.out.println("Missing Default Insights - "+missingInsightNames);
			reportStep("Fail","Default Insights that are absent - "+missingInsightNames);
		}
	}
	public String missingDefaultInsight(int i) {
		switch (i) {
		case 0:return "Home Region Name";
		case 1:return "Brand Type Code";
		case 2:return "Customer Facing Unit Type Description";
		case 3:return "Payment Category Code";
		default: return "";
		}
	}
}
