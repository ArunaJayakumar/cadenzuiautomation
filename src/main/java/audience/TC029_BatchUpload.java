package audience;

import java.io.IOException;
import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

import utils.ProjectWrappers;

public class TC029_BatchUpload extends ProjectWrappers{
	
	@BeforeClass
	public void beforeClass() {
		testName = "TC003_Audience";
		description="To verify Audience Filter search";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver;
	
	//@Parameters({ "environment" })
	@Test (groups= {"SmokeTest"})
	public void AudienceTestFilter() throws InterruptedException ,IOException {
		
	
		driver=getDriver();
		invokeApp(testName, true);
    	Thread.sleep(7000);
    	WebElement audienceSize=driver.findElement(By.xpath("//h6[text()='Audience Size']"));
    	WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(20));
    	wait1.until(ExpectedConditions.visibilityOf(audienceSize));
    	isDisplayedByXpath("//h6[text()='Audience Size']", "AudienceSize");
    	/*try {
			if(driver.findElement(By.xpath("//h6[text()='Audience Size']")).isDisplayed()) {
				System.out.println("Audience Page is loaded");
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","Audience Page not loaded");
		}*/
		clickByXpath("//button[contains(@class,'MuiAutocomplete-popupIndicator')]/span", "Attribute Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//li[text()='Active Subscriber Indicator']", "Msisdn Value");
	//	clickByXpath("//*[@id='condition-select-outlined']", "Condition Dropdown");
		clickByXpath("//span[text()='Choose']", "Choose");
		clickByXpath("//*[@id='attribute-select-outlined']", "Attribute Value Dropdown");
		clickByXpath("//li[text()='true']", "Attribute Value");
		driver.findElement(By.cssSelector("div.MuiGrid-root.MuiGrid-item.MuiGrid-grid-xs-2.MuiGrid-grid-md-1 > svg")).click();
		Thread.sleep(2000);
		clickByXpath("//button/span[text()='Search']", "Search Button");
		try {
			if(driver.findElement(By.xpath("//span[contains(text(),'Active Subscriber Indicator')]")).isDisplayed()) {
				reportStep("Pass","Filter added successfully");
				System.out.println("Filter added successfully");
			}

		} catch (NoSuchElementException e) {
			reportStep("Fail","Filter was not added");
			System.err.println("Filter was not added");
		}	

 
    	
    	try {
        	FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    		wait.pollingEvery(Duration.ofSeconds(5));
    		wait.withTimeout(Duration.ofSeconds(1));
    		wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
    		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>()
    				{
    					public WebElement apply(WebDriver arg0) {
    						System.out.println("Checking for the element!!");
    						WebElement element = arg0.findElement(By.xpath("//h6[text()='Audience Size']"));
    						if(element != null)
    						{
    							System.out.println("Target element found");
    							element.click();
    							 
    						}
    						return element;
    					}
    				};

    		wait.until(function);
    		 reportStep("Pass", "The Search results are ready to the user");
        	}
    	
        	catch(Exception e) {
        		reportStep("Fail","Not able to get Search results");
        		
        	}
    	
    	try {
    		String audienceSize1=getTextByXpath("//h4[@class='MuiTypography-root MuiTypography-h4']","Audience Size");
    		reportStep("Pass", "Audience size is: "+audienceSize1);
    		System.out.println("Audience size is: "+audienceSize1);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to get Audience Size");
    		System.err.println("Not able to get Audience Size");
    		
    	}

    	try {
			if(driver.findElement(By.xpath("//span[contains(text(),'filter conditions has zero results')]")).isDisplayed()) {
				System.out.println("Filter Condition has zero results");
				reportStep("Pass","Filter Condition has zero results");
			}
		} catch (NoSuchElementException e2) {
			System.err.println("Audience Insight is not loaded and Filter results are not displayed");
			reportStep("Fail","Audience Insight is not loaded and Filter results are not displayed");					
		}
    
   }
}
