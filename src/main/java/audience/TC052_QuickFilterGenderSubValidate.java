package audience;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;

public class TC052_QuickFilterGenderSubValidate extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To validate Gender Quick Filters";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test (groups= {"SmokeTest"})
	public void quickFilterGenderValidate() throws InterruptedException ,IOException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(1000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//span[text()='Clear']", "Clear Button");

		if(isDisplayedByXpath("//div[@id='qf-card']", "Quick Filter Icon")) {
			clickByXpath("//div[@id='qf-card']", "Quick Filter Icon");
			reportStep("Pass","Clicked on Quick Filter Search button");
			System.out.println("Clicked on Quick Filter Search button");
			Thread.sleep(1000);
			selectAllGenderOptions(); //Workaround for STRAC-978 - https://thedatateam.atlassian.net/browse/STRAC-978

			/*
//			Actual code once STRAC-978 is fixed. So Do not delete this
//			Field Validation

			clickByXpath("//span[text()='Female']", "Female Filter Value");
			try {
				if(driver.findElement(By.xpath("//*[@id='gender_type_description-formGroup']//span[text()='Unknown']")).isDisplayed()) {
					clickByXpath("//*[@id='gender_type_description-formGroup']//span[text()='Unknown']", "Unknown Gender Filter Value");
				}else {
					reportStep("Pass", "Unknown value is absent");
					System.out.println("Unknown value is absent");
				}
			} catch (Exception e) {
				reportStep("Pass", "Unknown value is absent");
				System.out.println("Unknown value is absent");
			}
			clickByXpath("//span[text()='Male']", "Male Filter Value");
			 */

			clickByXpath("//span[text()='Apply']", "QuickFilter Apply Button");
			explicitWaitForVisibility("//div[@class='search-tag-main']//span[contains(text(),'Gender Type Description CONTAINS')]", "Gender Type Filter");
			String defAudSize = driver.findElement(By.xpath("//*[@id='audience-sizeValue']")).getText();
			if(isDisplayedByXpath("//div[@class='search-tag-main']//span[contains(text(),'Gender Type Description CONTAINS')]", "Gender Type Filter")) {
				clickByXpath("//*[@id='aud-disc-audience-search-button']/span", "Search Button");
				explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
				if(isDisplayedByXpath("//*[@id='audience-sizeValue']", "Audience Size Value")) {
					String audienceSize = driver.findElement(By.xpath("//*[@id='audience-sizeValue']")).getText();
					if(audienceSize.equalsIgnoreCase(defAudSize)) {
						reportStep("Pass", "SUCCESS: Audience size of "+audienceSize+" is displayed successfully after applying Gender Type Filter and is same as default audience size as expected");
						System.out.println("SUCCESS: Audience size of "+audienceSize+" is displayed successfully after applying Gender Type Filter and is same as default audience size as expected");
					}else {
						reportStep("Fail", "ERROR: Updated Audience Size value of "+audienceSize+" is not same as default size value which is "+defAudSize);
						System.err.println("ERROR: Updated Audience Size value of "+audienceSize+" is not same as default size value which is "+defAudSize);
					}
				}else {
					reportStep("Fail", "ERROR: Audience Size value is not displayed");
					System.err.println("ERROR: Audience Size value is not displayed");
				}
			}else {
				reportStep("Fail", "ERROR: Gender Type filter is not displayed");
				System.err.println("ERROR: Gender Type filter is not displayed");
			}
		}else {
			reportStep("Fail", "ERROR: Quick Filter Icon is not displayed");
			System.err.println("ERROR: Quick Filter Icon is not displayed");
		}

	}

	//	Creating this function to select all options available in Gender Quick Filter as a workaround for STRAC-978
	public void selectAllGenderOptions() {
		List<WebElement> genderTypeAllOptionElements = driver.findElements(By.xpath("//*[@id='gender_type_description-formGroup']//span[contains(@class,'body1')]"));
		ArrayList<String> genderTypeAllOptions = new ArrayList<String>();
		for(WebElement i:genderTypeAllOptionElements) {
			genderTypeAllOptions.add(i.getText());
		}
		reportStep("Pass", "List of options available under Gender Type Quick Filter - "+genderTypeAllOptions);
		System.out.println("List of options available under Gender Type Quick Filter - "+genderTypeAllOptions);
		for(int i=genderTypeAllOptions.size();i>0;i--) {
			reportStep("Pass", "Selecting "+genderTypeAllOptions.get(i-1)+" from Gender Type Quick Filter");
			System.out.println("Selecting "+genderTypeAllOptions.get(i-1)+" from Gender Type Quick Filter");
			clickByXpath("//*[@id='gender_type_description-formGroup']//label["+i+"]/span[contains(@class,'body1')]", genderTypeAllOptions.get(i-1));
		}
	}
}
