package audience;


import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC057_ContinuousAttributeSearch extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To successfully apply categorical attribute filters";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void dateCheck() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//button[contains(@class,'MuiAutocomplete-popupIndicator')]/span", "Attribute Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//li[text()='Gross Monthly Income Amount']", "Gross Monthly Income Amount");//Gender Type Description
		clickByXpath("//*[@id='condition-select-outlined']", "Condition Dropdown");
		clickByXpath("//li[text()='Equals']", "Equal Condition");
		enterByXpath("//*[@id='outlined-basic']", "25000", "Attribute Value Text Box");
		clickByCSS("div.MuiGrid-root.MuiGrid-item.MuiGrid-grid-xs-2.MuiGrid-grid-md-1 > svg", "Add Filter");
		Thread.sleep(2000);
		clickByXpath("//button/span[text()='Search']", "Search Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		if(isDisplayedByXpath("//span[contains(text(),'Gross Monthly Income')]", "Gross Monthly Income Filter")) {
			reportStep("Pass","Filter added successfully");
			System.out.println("Filter added successfully");
		}else {
			reportStep("Fail","Filter was not added");
			System.err.println("Filter was not added");
		}
		if(isDisplayedByXpath("//h6[text()='Audience Size']", "Audience Size Text")) {
			reportStep("Pass","Audience Insight is loaded");
			System.out.println("Audience Insight is loaded");
		}else if(isDisplayedByXpath("//*[contains(text(),'filter conditions has zero results')]", "Zero Result Message")) {
			System.out.println("Filter Condition has zero results");
			reportStep("Pass","Filter Condition has zero results");
		}else {
			System.err.println("Audience Insight is not loaded and Filter results are not displayed");
			reportStep("Fail","Audience Insight is not loaded and Filter results are not displayed");					
		}
	}
}
