package audience;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;

public class TC055_QuickFilterResetNoFilter extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To validate reset button and submit no filters";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test (groups= {"SmokeTest"})
	public void quickFilterResetNoFilter() throws InterruptedException ,IOException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		if(isDisplayedByXpath("//div[@id='qf-card']", "Quick Filter Icon")) {
			clickByXpath("//div[@id='qf-card']", "Quick Filter Icon");
			reportStep("Pass","Clicked on Quick Filter Search button");
			System.out.println("Clicked on Quick Filter Search button");
			Thread.sleep(1000);

			//			Select all filters
			clickByXpath("//span[text()='No']", "'No' Filter Value");
			clickByXpath("//span[text()='Yes']", "'Yes' Filter Value");
			clickByXpath("//span[text()='Female']", "Female Filter Value");
			try {
				if(driver.findElement(By.xpath("//*[@id='gender_type_description-formGroup']//span[text()='Unknown']")).isDisplayed()) {
					clickByXpath("//*[@id='gender_type_description-formGroup']//span[text()='Unknown']", "Unknown Gender Filter Value");
				}else {
					reportStep("Pass", "Unknown value is absent");
					System.out.println("Unknown value is absent");
				}
			} catch (Exception e) {
				reportStep("Pass", "Unknown value is absent");
				System.out.println("Unknown value is absent");
			}
			clickByXpath("//span[text()='Male']", "Male Filter Value");
//			clickByXpath("//*[@id='payment-category-code-formGroup']//span[text()='Unknown']", "Unknown Payment Category Filter Value");
			clickByXpath("//*[@id='PRE-formLabel']//span[text()='PRE']", "PRE Filter Value");
			clickByXpath("//*[@id='POST-formLabel']//span[text()='POST']", "POST Filter Value");

			//			Click reset and then apply
			clickByXpath("//*[@id='quick-filter-resetbutton']/span", "Reset Quick Filter");
			clickByXpath("//span[text()='Apply']", "QuickFilter Apply Button");

			explicitWaitForVisibility("//div[text()='Please select at least one value to apply']", "Select One Filter ALert Message");
			Thread.sleep(1000);
			if(isDisplayedByXpath("//div[text()='Please select at least one value to apply']", "Select One Filter ALert Message")) {
				reportStep("Pass", "SUCCESS: 'Please select at least one value to apply' alert message is displayed as expected");
				System.out.println("SUCCESS: 'Please select at least one value to apply' alert message is displayed as expected");
			}else {
				reportStep("Fail", "ERROR: 'Please select at least one value to apply' alert message is not displayed");
				System.err.println("ERROR: 'Please select at least one value to apply' alert message is not displayed");
			}
		}else {
			reportStep("Fail", "ERROR: Quick Filter Icon is not displayed");
			System.err.println("ERROR: Quick Filter Icon is not displayed");
		}

	}
}
