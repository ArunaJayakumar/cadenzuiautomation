package audience;


import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC060_ContradictoryAttributeSearch extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To successfully apply categorical attribute filters";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void dateCheck() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//button[contains(@class,'MuiAutocomplete-popupIndicator')]/span", "Attribute Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//li[text()='Gender Type Description']", "Gender Type Description");//Gender Type Description
		clickByXpath("//*[@id='condition-select-outlined']", "Condition Dropdown");
		clickByXpath("//li[text()='Equals']", "Equal Condition");
		clickByXpath("//*[@id='attribute-select-outlined']", "Attribute Value Dropdown");
		clickByXpath("//li[text()='Male']", "Male Attribute Value");
		clickByCSS("div.MuiGrid-root.MuiGrid-item.MuiGrid-grid-xs-2.MuiGrid-grid-md-1 > svg", "Add Filter");
		clickByXpath("//button[contains(@class,'MuiAutocomplete-popupIndicator')]/span", "Attribute Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//li[text()='Gender Type Description']", "Gender Type Description");//Gender Type Description
		clickByXpath("//*[@id='condition-select-outlined']", "Condition Dropdown");
		clickByXpath("//li[text()='Equals']", "Equal Condition");
		clickByXpath("//*[@id='attribute-select-outlined']", "Attribute Value Dropdown");
		clickByXpath("//li[text()='Female']", "Female Attribute Value");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		if(isDisplayedByXpath("(//span[contains(text(),'Gender Type')])[1]", "Gender Type Filter")) {
			reportStep("Pass","SUCCESS: Filter added successfully");
			System.out.println("SUCCESS: Filter added successfully");
			clickByCSS("div.MuiGrid-root.MuiGrid-item.MuiGrid-grid-xs-2.MuiGrid-grid-md-1 > svg", "Add Filter");
			Thread.sleep(2000);
			clickByXpath("//button/span[text()='Search']", "Search Button");
			if(isDisplayedByXpath("//*[contains(text(),'filter conditions has zero results')]", "Zero Result Message")) {
				System.out.println("SUCCESS: Filter Condition has zero results");
				reportStep("Pass","SUCCESS: Filter Condition has zero results");
			}
			else if(isDisplayedByXpath("//h6[text()='Audience Size']", "Audience Size Text")) {
				reportStep("Pass","SUCCESS: Audience Insight is loaded");
				System.out.println("SUCCESS: Audience Insight is loaded");
			}else {
				System.err.println("ERROR: Audience Insight is not loaded and Filter results are not displayed");
				reportStep("Fail","ERROR: Audience Insight is not loaded and Filter results are not displayed");					
			}
		}else {
			reportStep("Fail","ERROR: Filter was not added");
			System.err.println("ERROR: Filter was not added");
		}
	}
}
