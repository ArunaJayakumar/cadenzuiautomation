package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC001_DataRecency extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify Latest Profile Date";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;
	@Test(groups= {"SmokeTest"})
	public void dateCheck() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		try {
			Actions action=new Actions(driver);
			WebElement filter=driver.findElement(By.xpath("//div[@id='aud-disc-audience-lst-profile-date']"));
			Thread.sleep(5000);
			action.moveToElement(filter).build().perform();
			String text=driver.findElement(By.xpath("//div[@id='aud-disc-audience-lst-profile-date']")).getText();
			System.out.println("Profile text is: "+text);
			takeSnap(testName);
			String subtext=text.substring(22);
			//System.out.println("subtext part is: "+subtext);
			//get current system date
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MMM/dd");  
			LocalDate now = LocalDate.now();  
			System.out.println("Today is: "+dtf.format(now)); 

			String year=subtext.substring((subtext.length())-4);
			String month=subtext.substring((subtext.length()-8),(subtext.length())-5);
			int len=subtext.length();
			String day="";
			if(len==14)
				day=subtext.substring((subtext.length()-14),(subtext.length())-12);
			if(len==13)
				day=subtext.substring((subtext.length()-13),(subtext.length())-12);


			System.out.println("Year: "+year+" and Month is: "+month+" and Date is: "+day);
			String dateString=day+"/"+month+"/"+year;
			// System.out.println("Date String= "+dateString);
			SimpleDateFormat cadFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dateOnCadenz=new SimpleDateFormat("dd/MMM/yyyy").parse(dateString);
			String mdy = cadFormat.format(dateOnCadenz);
			System.out.println("Profile date is: "+subtext);
			//System.out.println("Date on Cadenz to date format: "+dateOnCadenz);
			//System.out.println("Date after format: "+mdy);

			LocalDate diff=now.plusDays(-2);

			//System.out.println("Difference is: "+diff);

			if(mdy.equals(diff.toString()))	
				reportStep("Pass", "Date&Time on Homepage is: '"+subtext+"'");
			else 
				reportStep("Fail", "ProfileDate is '"+subtext+"' which not 2 days less than current date");
		}
		catch(Exception e) {
			reportStep("Fail", "Not able to find profile date");
		}
	}
}