package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC009_AdvancedFilter extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if Advanced Filter is accesible";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;


	@Test(groups= {"SmokeTest"})
	public void advancedFilter() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		takeSnap(testName);
		clickByXpath("//div[@id='aud-disc-audience-search-adv-filter-button']//span", "Advanced Filters");
		takeSnap(testName);
		Thread.sleep(2000);
		if(isDisplayedByXpath("//div[@id='rcDialogTitle0']", "Advanced Filter Label")) {
			reportStep("Pass", "Advanced Filter is opened");
			System.out.println("Advanced Filter is opened");
		}
		else {
			reportStep("Pass", "Advanced Filter is not opened");
			System.err.println("Advanced Filter is not opened");
		}
		clickByXpath("//div[@class='ant-col rule--field']//div[@class='ant-select-selection__placeholder']", "Attribute Advanced Filter Dropdown");
		clickByXpath("//li[text()='Active Subscriber Indicator']", "Active Subscriber Indicator");

		clickByXpath("//div[@class='ant-col rule--operator']//div[@class='ant-select-selection__placeholder']", "Attribute Advanced Filter Operator Dropdown");
		clickByXpath("//li[text()='=='] | //li[text()='Equals']", "Equals Condition");

		clickByXpath("//div[@class='ant-col rule--value']//div[@class='ant-select-selection__placeholder']", "Attribute Advanced Filter Value Dropdown");
		clickByXpath("//li[text()='false']", "False Value");

		clickByXpath("//span[text()='Submit']", "Submit Advanced Filter");

		Thread.sleep(2000);
		clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");
		try {
			String audienceSize1=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
			reportStep("Pass", "Audience size is: "+audienceSize1);
			System.out.println("Audience size is: "+audienceSize1);
		}
		catch(Exception e) {
			reportStep("Fail","Not able to get Audience Size");
			System.err.println("Not able to get Audience Size");
		}
	}
}
