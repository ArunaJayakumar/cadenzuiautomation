package audience;

import utils.GenericWrappers;
import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC076_AttributeGroupAudiencePreviewValidation extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the attribute group names in Audience Preview";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void attributeGroupAudiencePreviewValidation() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		GenericWrappers gw = new GenericWrappers();
		String role = gw.role;
		clickByXpath("//*[@id='aud-result-previewOption-tab']/span", "Audience Preview and Export");
		explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");
		//		Navigate to Preview to extract attributes
		clickByXpath("//*[@id='target-audience-previewOptionBtn']", "Preview Popup Button");
		explicitWaitForVisibility("//*[@id='audience-preview-dialog-title-Grid']", "Select attribute for Preview Popup");
		if(isDisplayedByXpath("//*[@id='audience-preview-dialog-title-Grid']", "Select attribute for Preview Popup")) {
			System.out.println("SUCCESS: Audience Preview Popup is loaded");
			reportStep("Pass", "SUCCESS: Audience Preview Popup is loaded");
			clickByXpath("//*[@id='audience-preview-export-select-all']", "Select All Preview");
			clickByXpath("//*[@id='auidence-preview-dialog-btn-add']/span", "Add Preview");
			List<WebElement> attgrpElm = driver.findElements(By.xpath("//label[@class='MuiFormControlLabel-root']//span[contains(@class,'MuiTypography-body')]"));
			ArrayList<String> attgrpName = new ArrayList<String>();
			for(WebElement i:attgrpElm) {
				attgrpName.add(i.getText());
			}
			String[] attgrp = {"Audience/Persona","Behavioural","Campaign History","Customer PII","Demographic/Affluence","Globe ID","Loyalty & Retention","Profitability","Geographics"};
			ArrayList<String> completeattgrpNames = new ArrayList<String>(Arrays.asList(attgrp));
			Collections.sort(attgrpName);
			Collections.sort(completeattgrpNames);
			boolean flag = true;
			for(String agn:completeattgrpNames) {
				flag = attgrpName.contains(agn);
			}
//			System.out.println(flag);
			if(flag) {
				if(attgrpName.equals(completeattgrpNames)) {
					System.out.println("SUCCESS: All Attribute Groups are present for user role "+role);
					reportStep("Pass", "SUCCESS: All Attribute Groups are present for user role "+role);
				}else {
					System.out.println("SUCCESS: "+attgrpName.size()+" Attribute Groups are present in Audience Preview and Export for user role "+role+". Displayed Attribute Groups are - "+attgrpName);
					reportStep("Pass", "SUCCESS: "+attgrpName.size()+" Attribute Groups are present in Audience Preview and Export for user role "+role+". Displayed Attribute Groups are - "+attgrpName);
				}
			}else {
				attgrpName.removeAll(completeattgrpNames);
				reportStep("Fail", "ERROR: "+attgrpName.size()+" attribute groups names are present that are incorrect. Incorrect Attribute group name - "+attgrpName);
				reportStep("Fail", "Expected Attribute group names are - "+completeattgrpNames);
				System.err.println("ERROR: "+attgrpName.size()+" attribute groups names are present that are incorrect. Incorrect Attribute group name - "+attgrpName+"\nExpected Attribute group names are - "+completeattgrpNames);
			}
		}else {
			reportStep("Fail","ERROR: Audience Preview section is not loaded");
			System.err.println("ERROR: Audience Preview section is not loaded");
		}
	}
}
