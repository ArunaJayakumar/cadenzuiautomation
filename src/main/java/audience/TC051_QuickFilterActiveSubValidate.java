package audience;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;

public class TC051_QuickFilterActiveSubValidate extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To validate Gender Quick Filters";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test (groups= {"SmokeTest"})
	public void quickFilterActiveSubValidate() throws InterruptedException ,IOException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		if(isDisplayedByXpath("//div[@id='qf-card']", "Quick Filter Icon")) {
			clickByXpath("//div[@id='qf-card']", "Quick Filter Icon");
			reportStep("Pass","Clicked on Quick Filter Search button");
			System.out.println("Clicked on Quick Filter Search button");
			Thread.sleep(1000);
			
//			Field Validation
			clickByXpath("//span[text()='No']", "'No' Filter Value");
			clickByXpath("//span[text()='Yes']", "'Yes' Filter Value");
			clickByXpath("//span[text()='Apply']", "QuickFilter Apply Button");
			explicitWaitForVisibility("//div[@class='search-tag-main']//span[contains(text(),'Active Subscriber Indicator CONTAINS')]", "Active Subscriber Filter");
			String defAudSize = driver.findElement(By.xpath("//*[@id='audience-sizeValue']")).getText();
			if(isDisplayedByXpath("//div[@class='search-tag-main']//span[contains(text(),'Active Subscriber Indicator CONTAINS')]", "Active Subscriber Filter")) {
				clickByXpath("//*[@id='aud-disc-audience-search-button']/span", "Search Button");
				explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
				if(isDisplayedByXpath("//*[@id='audience-sizeValue']", "Audience Size Value")) {
					String audienceSize = driver.findElement(By.xpath("//*[@id='audience-sizeValue']")).getText();
					if(audienceSize.equalsIgnoreCase(defAudSize)) {
						reportStep("Pass", "SUCCESS: Audience size of "+audienceSize+" is displayed successfully after applying Active Subscriber Filter and is same as default audience size, as expected");
						System.out.println("SUCCESS: Audience size of "+audienceSize+" is displayed successfully after applying Active Subscriber Filter and is same as default audience size, as expected");
					}else {
						reportStep("Fail", "ERROR: Audience Size value is not same as default size");
						System.err.println("ERROR: Audience Size value is not same as default size");
					}
				}else {
					reportStep("Fail", "ERROR: Audience Size value is not displayed");
					System.err.println("ERROR: Audience Size value is not displayed");
				}
			}else {
				reportStep("Fail", "ERROR: Active Subscriber filter is not displayed");
				System.err.println("ERROR: Active Subscriber filter is not displayed");
			}
		}else {
			reportStep("Fail", "ERROR: Quick Filter Icon is not displayed");
			System.err.println("ERROR: Quick Filter Icon is not displayed");
		}

	}
}
