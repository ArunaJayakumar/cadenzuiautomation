package audience;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;

public class TC006_QuickFilter extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To verify Quick Filter search";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;

	@Test (groups= {"SmokeTest"})
	public void quickFilter() throws InterruptedException ,IOException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//div[@id='qf-card']", "Quick Filter Icon");
		clickByXpath("//div[@id='qf-card']", "Quick Filter Icon");
		reportStep("Pass","Clicked on Quick Filter Search button");
		System.out.println("Clicked on Quick Filter Search button");  	

		clickByXpath("//div[@id='active_subscriber_indicator-formGroup']//span[text()='Yes']", "Active Subscriber Yes Option");
		clickByXpath("//span[text()='Apply']", "Apply");
		Thread.sleep(1000);
		clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");
		System.out.println("Clicked on Search button successfully");
		Thread.sleep(5000);

		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			reportStep("Pass", "The Search results are ready to the user");
			System.out.println("The Search results are ready to the user");
		}else {
			reportStep("Fail","Not able to get Search results");
			System.err.println("Not able to get Search results");
		}
		try {
			String audienceSize1=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
			reportStep("Pass", "Audience size is: "+audienceSize1);
			System.out.println("Audience size is: "+audienceSize1);
		}
		catch(Exception e) {
			reportStep("Fail","Not able to get Audience Size");
			System.err.println("Not able to get Audience Size");
		}
	}
}
