package audience;


import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC002_ConceptSearch extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the Concept attributes";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void conceptSearch() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//*[@id='aud-disc-audience-concept-search-expandMore-icon']", "Concepts Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//ul[@id='aud-disc-audience-concept-search-component-popup']//div[text()='Gen Z']", "Gen Z");
		clickByXpath("//button[@id='aud-disc-audience-concept-search-btn']/span", "Search Button");
		try {
			if(driver.findElement(By.xpath("//div[contains(@id,'Gen-Z')]/span")).isDisplayed()) {
				reportStep("Pass","Concept added successfully");
				System.out.println("Concept added successfully");
			}

		} catch (NoSuchElementException e) {
			reportStep("Fail","Concept was not added");
			System.err.println("Concept was not added");
		}

		try {
			String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
			reportStep("Pass", "Audience size is: "+audienceSize);
			System.out.println("Audience size is: "+audienceSize);
		}
		catch(Exception e) {
			reportStep("Fail","Not able to get Audience Size");
			System.err.println("Not able to get Audience Size");
		}

		try {
			if(driver.findElement(By.xpath("//h6[@id='audience-sizeTxt']")).isDisplayed()) {
				reportStep("Pass","Audience Insight is loaded");
				System.out.println("Audience Insight is loaded");
			}
		} catch (NoSuchElementException e) {
			try {
				if(driver.findElement(By.xpath("//span[@id='aud-result-noSubscriber-text1']")).isDisplayed()) {
					System.out.println("Concept Search Condition has zero results");
					reportStep("Pass","Concept Search Condition has zero results");
				}
			} catch (NoSuchElementException e2) {
				System.err.println("Audience Insight is not loaded and Concept results are not displayed");
				reportStep("Fail","Audience Insight is not loaded and Concept results are not displayed");					
			}
		}
	}
}
