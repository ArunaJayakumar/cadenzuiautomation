package audience;


import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC070_NoConceptSearch extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the No Concept Search error message";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void noConceptSearch() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			System.out.println("Audience Insight is loaded");
			reportStep("Pass", "Audience Insight is loaded");
			clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
			Thread.sleep(1000);
			clickByXpath("//button[@id='aud-disc-audience-concept-search-btn']/span", "Search Button");
			explicitWaitForVisibility("//div[text()='Please select any concept !']", "'Please select any concept !' Error Message");
			if(isDisplayedByXpath("//div[text()='Please select any concept !']", "'Please select any concept !' Error Message")) {
				reportStep("Pass", "'Please select any concept !' error message is displayed successfully");
				System.out.println("'Please select any concept !' error message is displayed successfully");
			}else {
				reportStep("Fail", "'Please select any concept !' error message is not displayed");
				System.err.println("'Please select any concept !' error message is not displayed");
			}
		}else {
			System.err.println("Audience Page not loaded");
			reportStep("Fail","Audience Page not loaded");
		}
	}
}
