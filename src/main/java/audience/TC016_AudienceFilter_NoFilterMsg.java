package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC016_AudienceFilter_NoFilterMsg extends ProjectWrappers {

	@BeforeClass
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify the Audience Insight Graphs by removing all filters";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void audFilterNoFilterMsg() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//div[@class='search-tag-main']//span[contains(text(),'lifestage')]", "Lifestage default filter");
		clickByXpath("//button[@id='aud-disc-audience-attr-clear-btn']/span", "Clear Button");
		try {
			if(driver.findElement(By.xpath("//div[@class='search-tag-main']")).isDisplayed()) {
				reportStep("Fail","Filters are not getting cleared when Clear button is pressed");
				System.err.println("Filters are not getting cleared when Clear button is pressed");
			}
		} catch (NoSuchElementException e) {
			System.out.println("Default Filters are cleared successfully");
			clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");
			Thread.sleep(5000);
			try {
				if(driver.findElement(By.xpath("//div[text()='Please add any filters to search !!']")).isDisplayed()) {
					reportStep("Pass","No Filter alert message is displayed");
					System.out.println("No Filter alert message is displayed");
				}
			} catch (NoSuchElementException ee) {
				reportStep("Fail","No Filter alert message is missing");
				System.err.println("No Filter alert message is missing");
			}
		}
	}
}