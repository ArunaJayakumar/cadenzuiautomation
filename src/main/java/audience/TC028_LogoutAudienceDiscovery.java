package audience;

import java.io.IOException;
import java.time.Duration;
//import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;

public class TC028_LogoutAudienceDiscovery extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {
		testName = this.getClass().getSimpleName();
		description="To verify Logout Popup and functionality from Audience Discovery page";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	//@Parameters({ "environment" })
	@Test (groups= {"SmokeTest"})
	public void logoutAudienceDiscovery() throws InterruptedException ,IOException {


		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			openLogoutPopup();

			//			Logout Popup Field Validation block
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
			isDisplayedByXpath("//div[text()='Confirm Logout']", "Confirm Logout text");
			isDisplayedByXpath("//div[@id='logout-text']", "Logout Text Message");
			isDisplayedByXpath("//div[@id='logout-actionBtn']//span[text()='Cancel']", "Cancel Button");
			isDisplayedByXpath("//div[@id='logout-actionBtn']//span[text()='Logout']", "Logout Button");
			isDisplayedByXpath("//div[@role='dialog']//span[@class='MuiIconButton-label']", "Close X Button");

			//			Checking Logout Functionality if clicking on Cancel or Close button closes the popup
			clickByXpath("//div[@id='logout-actionBtn']//span[text()='Cancel']", "Cancel Button");
			Thread.sleep(1000);
			isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
			openLogoutPopup();
			clickByXpath("//div[@role='dialog']//span[@class='MuiIconButton-label']", "Close X Button");
			Thread.sleep(1000);
			isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

			//			Checking Logout functionality
			openLogoutPopup();
			clickByXpath("//button/span[text()='Logout']", "Logout Button");
			explicitWaitForVisibility("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle");
			if(isDisplayedByXpath("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle")) {
				System.out.println("Logout was successful and user is currently on the application's login page");
				reportStep("Pass", "Logout was successful and user is currently on the application's login page");
			}else {
				System.err.println("Logout was unsuccessful and user is not on the application's login page");
				reportStep("Fail", "Logout was unsuccessful and user is not on the application's login page");
			}
		}else {
			reportStep("Fail", "Audience Discovery page is not loaded");
			System.err.println("Audience Discovery page is not loaded");
		}
	}
	public void openLogoutPopup() throws InterruptedException {
		clickByXpath("//button[@id='logout-dropDown']/span", "Logout Dropdown");
		clickByXpath("//*[@id='logout-txt']", "Logout");
		Thread.sleep(1000);
	}
}
