package audience;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;

public class TC032_LogoutRelogin extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {
		testName = this.getClass().getSimpleName();
		description="To verify application Relogin after Logout";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	//@Parameters({ "environment" })
	@Test (groups= {"SmokeTest"})
	public void reloginPostlogout() throws InterruptedException ,IOException {


		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			openLogoutPopup();

			//			Checking Logout functionality
			clickByXpath("//button/span[text()='Logout']", "Logout Button");
			explicitWaitForVisibility("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle");
			if(isDisplayedByXpath("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle")) {
				System.out.println("Logout was successful and user is currently on the application's login page");
				reportStep("Pass", "Logout was successful and user is currently on the application's login page");

				clickByXpath("//button[@class='btn btn-primary btn-block sign-in-btn']", "SignInwithGoogle");
				Thread.sleep(5000);

				//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
				explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
				clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
				clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
				explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

				if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
					System.out.println("Login was successful and user is currently on the Audience Discovery page");
					reportStep("Pass", "Login was successful and user is currently on the Audience Discovery page");
				}else {
					System.err.println("Login was unsuccessful and user is not on the Audience Discovery page");
					reportStep("Fail", "Login was unsuccessful and user is not on the Audience Discovery page");
				}
			}else {
				System.err.println("Logout was unsuccessful and user is not on the application's login page");
				reportStep("Fail", "Logout was unsuccessful and user is not on the application's login page");
			}
		}else {
			reportStep("Fail", "Audience Discovery page is not loaded");
			System.err.println("Audience Discovery page is not loaded");
		}
	}
	public void openLogoutPopup() throws InterruptedException {
		clickByXpath("//button[@id='logout-dropDown']/span", "Logout Dropdown");
		clickByXpath("//*[@id='logout-txt']", "Logout");
		Thread.sleep(1000);
	}
}
