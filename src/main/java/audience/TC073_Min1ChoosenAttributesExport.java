package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC073_Min1ChoosenAttributesExport extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if audience data can be exported from Audience Preview and Export";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void min1AttExport() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='aud-result-previewOption-tab']/span", "Audience Preview and Export");
		explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");

		if(isDisplayedByXpath("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header")) {
			System.out.println("Audience Preview section is loaded");
			reportStep("Pass", "Audience Preview section is loaded");

			//			Navigate to Export popup
			clickByXpath("//*[@id='target-audience-saveOptionBtn']", "Export Popup Button");
			clickByXpath("//*[@id='audience-preview-export-wd-expand-all-wd-select-all']", "Select All Selected Export");
			clickByXpath("//*[@id='audience-export-dialog-content-operation-remove-btn']/span", "Remove Preview");
			clickByXpath("//button[@id='audience-export-dialogBox-submit-btn']", "Export Button");
			
			explicitWaitForVisibility("//*[text()='Minimum 1 attribute has to be selected for export']", "Minimum 1 Attribute Export Selected Message");
			Thread.sleep(1000);
			if(isDisplayedByXpath("//*[text()='Minimum 1 attribute has to be selected for export']", "Minimum 1 Attribute Export Selected Message")) {
				System.out.println("SUCCESS: 'Minimum 1 attribute has to be selected for export' alert message is displayed");
				reportStep("Pass", "SUCCESS: 'Minimum 1 attribute has to be selected for export' alert message is displayed");
			}else {
				reportStep("Fail", "ERROR: 'Minimum 1 attribute has to be selected for export' alert message is not displayed");
				System.err.println("ERROR: 'Minimum 1 attribute has to be selected for export' alert message is not displayed");
			}
		}else {
			reportStep("Fail","ERROR: Audience Preview section is not loaded");
			System.err.println("ERROR: Audience Preview section is not loaded");
		}
	}
}
