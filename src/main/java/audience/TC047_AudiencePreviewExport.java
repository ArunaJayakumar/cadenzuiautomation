package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC047_AudiencePreviewExport extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if audience data can be exported from Audience Preview and Export";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void audPreviewExport() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='aud-result-previewOption-tab']/span", "Audience Preview and Export");
		explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");
		Thread.sleep(1000);

		if(isDisplayedByXpath("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header")) {
			System.out.println("Audience Preview section is loaded");
			reportStep("Pass", "Audience Preview section is loaded");

			//			Navigate to Export popup
			clickByXpath("//*[@id='target-audience-saveOptionBtn']", "Export Popup Button");
			clickByXpath("//button[@id='audience-export-dialogBox-submit-btn']", "Export Button");
			
			explicitWaitForVisibility("//div[text()='Download will begin shortly']", "Export InProgress Alert Message");
			Thread.sleep(2000);
			if(isDisplayedByXpath("//div[text()='Download will begin shortly']", "Export InProgress Alert Message")) {
				System.out.println("SUCCESS: Audience Data Export commenced successfully and 'Export/Download will begin shortly !' alert message is displayed");
				reportStep("Pass", "SUCCESS: Audience Data Export commenced successfully and 'Export/Download will begin shortly !' alert message is displayed");
			}else {
				reportStep("Fail","ERROR: Audience Data Export Alert Message is not displayed");
				System.err.println("ERROR: Audience Data Export Alert Message is not displayed");
			}
		}else {
			reportStep("Fail","ERROR: Audience Preview section is not loaded");
			System.err.println("ERROR: Audience Preview section is not loaded");
		}
	}
}
