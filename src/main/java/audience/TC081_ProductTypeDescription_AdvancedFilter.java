package audience;

import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
public class TC081_ProductTypeDescription_AdvancedFilter extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the Audience Size for Product Type Description - Wireless Mobile Telepony and Wireless Broadband using Advanced Filter";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void wirelessMobileTelephonyAdvFilter() throws InterruptedException, IOException, ParseException {
		String[] attValueNames = {"Wireless Mobile Telephony","Wireless Broadband"};
		String[] segmentSize = new String[attValueNames.length];

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		Thread.sleep(1000);
		clickByCSS("div.segment-header-section > svg", "Edit Hierarchy");
		if(isDisplayedByXpath("//div[text()='Select Discovery Hierarchy']","Segment Hierarchy")) {
			System.out.println("Select Discovery Hierarchy dialog box is displayed");
			reportStep("Pass","Select Discovery Hierarchy dialog box is displayed");
			clickByXpath("//span[@title='Un-select']", "Un-select");
			enterByXpath("//input[@id='editDisHierarchy-searchAttribute-textField']", "Product Type Description", "SegmentName");
			clickByXpath("//*[@id='editDisHierarchy-Product-Type-Description-availableAttrLabel']/span[@class='segments-move-icon-right']", "ProductTypeDescription Move to Right");
			clickByXpath("//*[@id='editDisHierarchy-Product-Type-Description-selectedAttr-moveup-lessIcon']", "Move ProductTypeDescription Up");
			clickByXpath("//span[text()='Submit']", "Submit Segment");
			Thread.sleep(3000);
			if(isDisplayedByXpath("//div[@id='aud-insightSegment-container']//div[contains(text(),'Product Type Description')]","ProductTypeDescription Segment")) {
				reportStep("Pass", "Added Segment successfully");
				System.out.println("Added Segment successfully");
//				String segmentWMTAudienceSize = driver.findElement(By.xpath("//*[@id='Wireless-Mobile-Telephony--countlabel']")).getText();
//				String segmentWBAudienceSize = driver.findElement(By.xpath("//*[@id='Wireless-Broadband--countlabel']")).getText();
				for(int i=0;i<attValueNames.length;i++) {
					String attValXpath = attValueNames[i].replace(' ', '-');
					segmentSize[i]=driver.findElement(By.xpath("//*[@id='"+attValXpath+"--countlabel']")).getText();
				}
				for(int i=0;i<attValueNames.length;i++) {
					SizeValidation(attValueNames[i], segmentSize[i]);
				}
//				SizeValidation("Wireless Mobile Telephony", segmentWMTAudienceSize);
//				SizeValidation("Wireless Broadband", segmentWBAudienceSize);
			}else {
				reportStep("Fail", "Adding Segment was unsuccessful");
				System.err.println("Adding Segment was unsuccessful");
			}
		}else {
			System.err.println("Select Discovery Hierarchy dialog is not opened");
			reportStep("Fail","Select Discovery Hierarchy dialog is not opened");
		}

	}
	void SizeValidation(String attributeName, String segmentAudienceSize) throws InterruptedException {
		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-adv-filter-button']//span", "Advanced Filters");
		if(isDisplayedByXpath("//div[@id='rcDialogTitle0']", "Advanced Filter Label")) {
			reportStep("Pass", "Advanced Filter is opened");
			System.out.println("Advanced Filter is opened");
			clickByXpath("//div[@class='ant-col rule--field']//div[@class='ant-select-selection__placeholder']", "Attribute Advanced Filter Dropdown");
			clickByXpath("//li[text()='Product Type Description']", "Product Type Description");

			clickByXpath("//div[@class='ant-col rule--operator']//div[@class='ant-select-selection__placeholder']", "Attribute Advanced Filter Operator Dropdown");
			clickByXpath("//li[text()='=='] | //li[text()='Equals']", "Equals Condition");

			clickByXpath("//div[@class='ant-col rule--value']//div[@class='ant-select-selection__placeholder']", "Attribute Advanced Filter Value Dropdown");
			clickByXpath("//li[contains(text(),'"+attributeName+"')]", attributeName+" Value");

			clickByXpath("//span[text()='Submit']", "Submit Advanced Filter");
			Thread.sleep(1000);
			clickByXpath("//button/span[text()='Search']", "Search Button");
			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			Thread.sleep(1000);
			try {
				if(driver.findElement(By.xpath("//h6[text()='Audience Size']")).isDisplayed()) {
					reportStep("Pass","Audience Insight is loaded");
					System.out.println("Audience Insight is loaded");
					String verifyAudienceSize = driver.findElement(By.xpath("//*[@id='audience-sizeValue']")).getText();
					if(verifyAudienceSize.equalsIgnoreCase(segmentAudienceSize)) {
						reportStep("Pass", "SUCCESS: Audience Size, after Advanced Filter Search operation, for Product Type Description - "+attributeName+" is same as the size showcased in Segment Hierarchy");
						System.out.println("SUCCESS: Audience Size, after Advanced Filter Search operation, for Product Type Description - "+attributeName+" is same as the size showcased in Segment Hierarchy");
					}else {
						reportStep("Fail", "ERROR: Audience Size, after Advanced Filter Search operation, for Product Type Description - "+attributeName+" is different from the size showcased in Segment Hierarchy");
						System.out.println("ERROR: Audience Size, after Advanced Filter Search operation, for Product Type Description - "+attributeName+" is different from the size showcased in Segment Hierarchy");
					}
					clickByXpath("//span[text()='Clear']", "Clear");
					clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
					clickByXpath("//div[@id='aud-disc-audience-search-adv-filter-button']//span", "Advanced Filters");
					clickByXpath("//button[@class='ant-btn ant-btn-danger ant-btn-sm ant-btn-icon-only']", "Delete Advanced Filter");
					clickByXpath("//div[@class='ant-modal-confirm-btns']/button[contains(@class,'ant-btn-danger')]", "Yes Delete Button");
					clickByXpath("//span[text()='Cancel']", "Cancel Button");
				}
			} catch (NoSuchElementException e) {
				try {
					if(driver.findElement(By.xpath("//span[contains(text(),'filter conditions has zero results')]")).isDisplayed()) {
						System.err.println("ERROR: Audience size doesn't match with the "+attributeName+" Segment value size");
						reportStep("Fail", "ERROR: Audience size doesn't match with the "+attributeName+" Segment value size");
						clickByXpath("//span[text()='Clear']", "Clear");
						clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
						clickByXpath("//div[@id='aud-disc-audience-search-adv-filter-button']//span", "Advanced Filters");
						clickByXpath("//button[@class='ant-btn ant-btn-danger ant-btn-sm ant-btn-icon-only']", "Delete Advanced Filter");
						clickByXpath("//div[@class='ant-modal-confirm-btns']/button[contains(@class,'ant-btn-danger')]", "Yes Delete Button");
						clickByXpath("//span[text()='Cancel']", "Cancel Button");
					}
				} catch (NoSuchElementException e2) {
					System.err.println("ERROR: Audience Insight is not loaded and Filter results are not displayed");
					reportStep("Fail","ERROR: Audience Insight is not loaded and Filter results are not displayed");					
				}
			}
		}
		else {
			reportStep("Pass", "Advanced Filter is not opened");
			System.err.println("Advanced Filter is not opened");
		}
	}
}
