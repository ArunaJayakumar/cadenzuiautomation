package audience;

import utils.GenericWrappers;
import utils.GetAttributeWithBL;
import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC045_AttributePreviewValidation extends ProjectWrappers {
//	String role = "1A";
//	String role = System.getenv("ROLE");
	GenericWrappers gw = new GenericWrappers();
	String role = gw.role;

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = (this.getClass().getSimpleName())+"_"+role;
		description="To extract and verify the attributes from Audience Preview and Export";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void audPreviewAttValidation() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='aud-result-previewOption-tab']/span", "Audience Preview and Export");
		explicitWaitForVisibility("//*[@id='target-audience-header']", "Audience Preview Table Header");
		Thread.sleep(1000);

		if(isDisplayedByXpath("//*[@id='target-audience-header']", "Audience Preview Table Header")) {
			System.out.println("Audience Preview section is loaded");
			reportStep("Pass", "Audience Preview section is loaded");

//			Navigate to Preview popup
			clickByXpath("//*[@id='target-audience-previewOptionBtn']", "Preview Popup Button");
			clickByXpath("//*[@id='audience-preview-export-select-all']", "Select All Preview");
			clickByXpath("//*[@id='auidence-preview-dialog-btn-add']/span", "Add Preview");

			
			//		Get Attributes list from preview page
			List<WebElement> attPreviewTabList = driver.findElements(By.xpath("//div[contains(@id,'audience-preview-export-attribute-')]"));
			ArrayList<String> attPreviewList = new ArrayList<String>();
			for(WebElement j:attPreviewTabList) {
				attPreviewList.add(j.getText());
			}
			Collections.sort(attPreviewList);
			clickByXpath("//*[@id='audience-preview-dialog-cancel-btn']", "Cancel Preview Popup Button");
			
//			Navigate to Export popup
			clickByXpath("//*[@id='target-audience-saveOptionBtn']", "Export Popup Button");
			clickByXpath("//*[@id='audience-preview-export-select-all']", "Select All Preview");
			clickByXpath("//*[@id='audience-export-dialog-content-operation-add-btn']/span", "Add Export");

			//		Get Attributes list from preview page
			List<WebElement> attExportTabList = driver.findElements(By.xpath("//div[contains(@id,'audience-preview-export-attribute-')]"));
			ArrayList<String> attExportList = new ArrayList<String>();
			for(WebElement j:attExportTabList) {
				attExportList.add(j.getText());
			}
			Collections.sort(attExportList);
			
			if(attPreviewList.equals(attExportList)) {
				System.out.println("SUCCESS: Attributes in preview popup is same as attributes in export popup");
				reportStep("Pass", "SUCCESS: Attributes in preview popup is same as attributes in export popup");
			}else {
				System.err.println("ERROR: Attributes in preview popup is not same as attributes in export popup");
				reportStep("Fail", "ERROR: Attributes in preview popup is not same as attributes in export popup");
			}
			
			//		List of elements which are case incorrect in attribute filter (attlist)
			String[] applicationString= {"Monthly Roaming Uc", "Monthly Vas Uc", "Nsat Indicator", "Nsat Probability", "Postpaid Arpu Oc 30days", "Postpaid Arpu Oc 60days", "Postpaid Arpu Oc 90days", "Postpaid Arpu Rc 30days", "Postpaid Arpu Rc 60days", "Postpaid Arpu Rc 90days", "Postpaid Arpu Uc 30days", "Postpaid Arpu Uc 60days", "Postpaid Arpu Uc 90days","Nps Consistent Detractor Indicator","Nps Consistent Non Detractor Indicator","Nps Journey Detractor Indicator","Nps Promo Indicator","Nps Rewards Indicator","Nps Topup Indicator","Lifestage Aa","Imsi Number Value"};
			ArrayList<String> applicationList=new ArrayList<String>(Arrays.asList(applicationString));

			//		List of elements which are case incorrect in requirement
			String[] requirementString= {"Monthly Roaming UC", "Monthly Vas UC", "NSAT Indicator", "NSAT Probability", "Postpaid Arpu OC 30days", "Postpaid Arpu OC 60days", "Postpaid Arpu OC 90days", "Postpaid Arpu RC 30days", "Postpaid Arpu RC 60days", "Postpaid Arpu RC 90days", "Postpaid Arpu UC 30days", "Postpaid Arpu UC 60days", "Postpaid Arpu UC 90days","NPS Consistent Detractor Indicator","NPS Consistent Non Detractor Indicator","NPS Journey Detractor Indicator","NPS Promo Indicator","NPS Rewards Indicator","NPS Topup Indicator","Lifestage AA","IMSI Number Value"};
			ArrayList<String> requirementList=new ArrayList<String>(Arrays.asList(requirementString));

			//		Get attributes list for the specific role from Excel
			ArrayList<String> excelAttList=GetAttributeWithBL.getAttributesWithBlacklist("Attributes", role);
//			excelAttList.replaceAll(String::toUpperCase);
			Collections.sort(excelAttList);
			System.out.println(excelAttList.size()+" Attributes are listed from excel");
			reportStep("Pass", excelAttList.size()+" Attributes are listed from excel");

			//		Validation for Attribute Preview
			System.out.println("\n***AUDIENCE PREVIEW***\n "+attPreviewList.size()+" Attributes are listed in the Audience Preview page");
			reportStep("Pass", "\n***AUDIENCE PREVIEW***\n "+attPreviewList.size()+" Attributes are listed in the Audience Preview page");
			ArrayList<String> tempPreview = new ArrayList<String>();
			if(attPreviewList.equals(excelAttList)) {
				System.out.println("SUCCESS: Attributes extracted from Audience Preview is same as attributes mentioned in requirements");
				reportStep("Pass","SUCCESS: Attributes extracted from Audience Preview is same as attributes mentioned in requirements");
			}else {
				if(attPreviewList.size() >= excelAttList.size()) {
					tempPreview=tempArrList(attPreviewList, tempPreview);
					tempPreview.removeAll(excelAttList);
					tempPreview.removeAll(applicationList);
					excelAttList.removeAll(attPreviewList);
					excelAttList.removeAll(requirementList);
					if(tempPreview.size()==0 && excelAttList.size()==0) {
						System.out.println("SUCCESS: Attributes are listed as expected in Audience Preview");
						reportStep("Pass", "SUCCESS: Attributes are listed as expected in Audience Preview");	
					}else {
						System.err.println("ERROR1: "+tempPreview.size()+" Audience Preview attributes that are missing from Requirements - "+tempPreview+"\n"+excelAttList.size()+" Requirement Attributes that are missing in Audience Preview - \n"+excelAttList);
						reportStep("Fail",("ERROR1: "+tempPreview.size()+" Audience Preview attributes that are missing from Requirements - "+tempPreview+"\n"+excelAttList.size()+" Requirement Attributes that are missing in Audience Preview - \n"+excelAttList));
					}
				}else {
					tempPreview=tempArrList(excelAttList, tempPreview);
					tempPreview.removeAll(attPreviewList);
					tempPreview.removeAll(requirementList);
					attPreviewList.removeAll(excelAttList);
					attPreviewList.removeAll(applicationList);
					if(tempPreview.size()==0 && attPreviewList.size()==0) {
						System.out.println("SUCCESS: Attributes are listed as expected in Audience Preview");
						reportStep("Pass", "SUCCESS: Attributes are listed as expected in Audience Preview");	
					}else {
						System.err.println("ERROR2: "+attPreviewList.size()+" Audience Preview attributes that are missing from Requirements - \n"+attPreviewList+"\n"+tempPreview.size()+" Requirement Attributes that are missing in Audience Preview - \n"+tempPreview);
						reportStep("Fail",("ERROR2: "+attPreviewList.size()+" Audience Preview attributes that are missing from Requirements - \n"+attPreviewList+"\n"+tempPreview.size()+" Requirement Attributes that are missing in Audience Preview - \n"+tempPreview));
					}
				}
			}
		}else {
			reportStep("Fail","ERROR: Audience Preview section is not loaded");
			System.err.println("ERROR: Audience Preview section is not loaded");
		}
	}
	public ArrayList<String> tempArrList(ArrayList<String> x, ArrayList<String> temp) {
		for(int i=0;i<x.size();i++) {
			temp.add(x.get(i));
		}
		return temp;
	}
}
