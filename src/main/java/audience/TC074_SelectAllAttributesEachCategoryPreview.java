package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC074_SelectAllAttributesEachCategoryPreview extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the category based 'select all' option in preview";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void selectAllAttributesEachCategoryPreview() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='aud-result-previewOption-tab']/span", "Audience Preview and Export");
		explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");
//		Navigate to Preview to extract attributes
		clickByXpath("//*[@id='target-audience-previewOptionBtn']", "Preview Popup Button");
		explicitWaitForVisibility("//*[@id='audience-preview-dialog-title-Grid']", "Select attribute for Preview Popup");
		if(isDisplayedByXpath("//*[@id='audience-preview-dialog-title-Grid']", "Select attribute for Preview Popup")) {
			System.out.println("SUCCESS: Audience Preview Popup is loaded");
			reportStep("Pass", "SUCCESS: Audience Preview Popup is loaded");
			clickByXpath("//*[@id='audience-preview-export-wd-expand-all-wd-select-all']", "Select All Selected Preview");
			clickByXpath("//*[@id='auidence-preview-dialog-btn-remove']/span", "Remove Preview");
			clickByXpath("//*[@id='audience-preview-export--Demographic/Affluence']", "Demographic_Affluence Select All Preview");
			clickByXpath("//*[@id='auidence-preview-dialog-btn-add']/span", "Add Preview");
			clickByXpath("//button[@id='audience-preview-dialog-submit-btn']", "Preview Button");
			explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");
			Thread.sleep(1000);
			if(isDisplayedByXpath("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header")) {
				System.out.println("SUCCESS: Audience Preview section is loaded with all Demographic and Affluence attributes");
				reportStep("Pass", "SUCCESS: Audience Preview section is loaded with all Demographic and Affluence attributes");

			}else {
				reportStep("Fail", "ERROR: Audience Preview section is not loaded with all Demographic and Affluence attributes");
				System.err.println("ERROR: Audience Preview section is not loaded with all Demographic and Affluence attributes");
			}
		}else {
			reportStep("Fail","ERROR: Audience Preview section is not loaded");
			System.err.println("ERROR: Audience Preview section is not loaded");
		}

	}
}
