package audience;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.google.common.base.Function;
import utils.ProjectWrappers;

public class TC003_Audience extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {
		testName = this.getClass().getSimpleName();
		description="To verify Audience Filter search";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;

	//@Parameters({ "environment" })
	@Test (groups= {"SmokeTest"})
	public void AudienceTestFilter() throws InterruptedException ,IOException {


		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='aud-disc-filter-dropdown']", "Attribute Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//li[text()='Active Subscriber Indicator']", "Active Subscriber Indicator");
		clickByXpath("//*[@id='condition-select-outlined']", "Condition Dropdown");
		clickByXpath("//li[text()='=='] | //li[text()='Equals']", "Equals Condition");
		clickByXpath("//*[@id='attribute-select-outlined']", "Attribute Value Dropdown");
		clickByXpath("//li[text()='true']", "Active Subscriber Attribute Value");
		clickByCSS("div[id='aud-disc-audience-search-add-button']>svg", "Add Filter");
		Thread.sleep(3000);
		clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");

		try {
			if(driver.findElement(By.xpath("//span[contains(text(),'Active Subscriber Indicator')]")).isDisplayed()) {
				reportStep("Pass","Filter added successfully");
				System.out.println("Filter added successfully");
			}

		} catch (NoSuchElementException e) {
			reportStep("Fail","Filter was not added");
			System.err.println("Filter was not added");
		}	

		try {
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
			Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>()
			{
				public WebElement apply(WebDriver arg0) {
					System.out.println("Checking for the element!!");
					WebElement element = arg0.findElement(By.xpath("//h6[@id='audience-sizeTxt']"));
					if(element != null)
					{
						System.out.println("Target element found");
						element.click();

					}
					return element;
				}
			};
			wait.until(function);
			reportStep("Pass", "The Search results are ready to the user");
		}
		catch(Exception e) {
			reportStep("Fail","Not able to get Search results");

		}

		try {
			String audienceSize1=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
			reportStep("Pass", "Audience size is: "+audienceSize1);
			System.out.println("Audience size is: "+audienceSize1);
		}
		catch(Exception e) {
			reportStep("Fail","Not able to get Audience Size");
			System.err.println("Not able to get Audience Size");
		}

		try {
			if(driver.findElement(By.xpath("//span[@id='aud-result-noSubscriber-text1']")).isDisplayed()) {
				System.out.println("Filter Condition has zero results");
				reportStep("Pass","Filter Condition has zero results");
			}
		} catch (NoSuchElementException e2) {
			try {
				if(driver.findElement(By.xpath("//h6[@id='audience-sizeTxt']")).isDisplayed()) {
					System.out.println("Search Filter has some results displayed");
					reportStep("Pass","Search Filter has some results displayed");					
				}
			} catch (NoSuchElementException e4) {
				System.err.println("Audience Insight is not loaded and Filter results are not displayed");
				reportStep("Fail","Audience Insight is not loaded and Filter results are not displayed");					
			}
		}
	}
}
