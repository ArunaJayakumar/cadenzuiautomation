package audience;


import utils.GetAttributeForRole;
import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC017_AudienceFilterAttributes extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the Audience Filters Attributes";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void attributeFilter() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(2000);
		
		//List of elements which are case incorrect in attribute filter (attlist)
		String[] applicationString= {"Monthly Roaming Uc", "Monthly Vas Uc", "Nsat Indicator", "Nsat Probability", "Postpaid Arpu Oc 30days", "Postpaid Arpu Oc 60days", "Postpaid Arpu Oc 90days", "Postpaid Arpu Rc 30days", "Postpaid Arpu Rc 60days", "Postpaid Arpu Rc 90days", "Postpaid Arpu Uc 30days", "Postpaid Arpu Uc 60days", "Postpaid Arpu Uc 90days"};
		ArrayList<String> applicationList=new ArrayList<String>(Arrays.asList(applicationString));
		
		//List of elements which are case incorrect in requirement
		String[] requirementString= {"Monthly Roaming UC", "Monthly Vas UC", "NSAT Indicator", "NSAT Probability", "Postpaid Arpu OC 30days", "Postpaid Arpu OC 60days", "Postpaid Arpu OC 90days", "Postpaid Arpu RC 30days", "Postpaid Arpu RC 60days", "Postpaid Arpu RC 90days", "Postpaid Arpu UC 30days", "Postpaid Arpu UC 60days", "Postpaid Arpu UC 90days"};
		ArrayList<String> requirementList=new ArrayList<String>(Arrays.asList(requirementString));
		try {
			if(driver.findElement(By.xpath("//h6[text()='Audience Size']")).isDisplayed()) {
				System.out.println("Audience Insight is loaded");
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","Audience Insight not loaded");
		}
		clickByXpath("//button[contains(@class,'MuiAutocomplete-popupIndicator')]/span", "Attribute Filter Dropdown");
		Thread.sleep(2000);
		
		//Get attributes in filter
		ArrayList<String> attList = new ArrayList<String>();
		List<WebElement> attObj = driver.findElements(By.xpath("//li[contains(@id,'size-small-outlined-option-')]"));
		//List<WebElement> attObj = driver.findElements(By.xpath("//li[contains(@id,'autocomplete-filter-option-')]"));
		for(int j=0;j<attObj.size();j++) {
			attList.add(attObj.get(j).getText());
		}
		Collections.sort(attList);
		System.out.println("Attributes list in the Filter is: \n"+attList);
		
		//Get the attributes list for the role from excel
		ArrayList<String> excelAttList=GetAttributeForRole.getAttributesWOBlacklist("Attributes", "1A");
		System.out.println("List from excel is: \n"+excelAttList);
		
		//Validation
		ArrayList<String> temp = new ArrayList<String>();
		if(attList.equals(excelAttList)) {
			System.out.println("Attributes extracted from Filter is same as attributes mentioned in requirements");
			reportStep("Pass","Attributes extracted from Filter is same as attributes mentioned in requirements");
		}else {
			if(attList.size() >= excelAttList.size()) {
				temp=tempArrList(attList, temp);
				temp.removeAll(excelAttList);
				excelAttList.removeAll(attList);
				if(temp.equals(applicationList) && (excelAttList.equals(requirementList))) {
					System.out.println("Attributes are listed as expected");
					reportStep("Pass", "Attributes are listed as expected");
				}
				else {
				System.err.println("1Filter attributes listed in the application but missing in Requirements:\n"+temp+"\n1Attributes listed in requirment but missing in application:\n"+excelAttList);
				reportStep("Fail",("\n1Filter attributes listed in the application but missing in Requirements:\n"+temp+"\n1Attributes listed in requirment but missing in application:\n"+excelAttList));
				}
				}
			else {
				temp=tempArrList(excelAttList, temp);
				temp.removeAll(attList);
				attList.removeAll(excelAttList);
				if(temp.equals(applicationList) && (excelAttList.equals(requirementList))) {
					System.out.println("Attributes are listed as expected");
					reportStep("Pass", "Attributes are listed as expected");
				}
				else {
				System.err.println("2Filter attributes listed in the application but missing in Requirements:\n"+attList+"\n2Attributes listed in requirment but missing in application:\n"+temp);
				reportStep("Fail",("2Filter attributes listed in the application but missing in Requirements:\n"+attList+"\n2Attributes listed in requirment but missing in application:\n"+temp));
				}
				}
		}
		
		
	}
	public ArrayList<String> tempArrList(ArrayList<String> x, ArrayList<String> temp) {
		for(int i=0;i<x.size();i++) {
			temp.add(x.get(i));
		}
		return temp;
	}
}
