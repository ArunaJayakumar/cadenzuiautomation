package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC062_SearchTwoRules extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify two rule Advanced Filter with AND operator";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;


	@Test(groups= {"SmokeTest"})
	public void searchTwoRules() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-adv-filter-button']//span", "Advanced Filters");
		Thread.sleep(2000);
		if(isDisplayedByXpath("//div[@id='rcDialogTitle0']", "Advanced Filter Label")) {
			reportStep("Pass", "SUCCESS: Advanced Filter is opened");
			System.out.println("SUCCESS: Advanced Filter is opened");
			clickByXpath("//div[@class='ant-col rule--field']//div[@class='ant-select-selection__placeholder']", "Attribute Filter Dropdown");
			clickByXpath("//li[text()='Sim Lte Capable Indicator']", "Sim Lte Capable Indicator Attribute");
			clickByXpath("//div[@class='ant-col rule--operator']//div[@class='ant-select-selection__placeholder']", "Attribute Operator Dropdown");
			clickByXpath("//li[text()='=='] | //li[text()='Equals']", "Equals Condition");
			clickByXpath("//div[@class='ant-col rule--value']//div[@class='ant-select-selection__placeholder']", "Attribute Value Dropdown");
			clickByXpath("//li[text()='true']", "'true' Value");
			if (isDisplayedByXpath("//div[@id='advSearch-query-content'][contains(text(),'Sim Lte')]", "SIM LTE search")) {
				reportStep("Pass", "SUCCESS: SIM LTE Capable Indicator is selected successfully");
				System.out.println("SUCCESS: SIM LTE Capable Indicator is selected successfully");
				clickByXpath("//button[contains(@class,'ADD-RULE')]", "Add Rule");
				if (isDisplayedByXpath("//div[text()='Select field']", "Second Row Empty Attribute")) {
					reportStep("Pass", "SUCCESS: Second filter row is displayed");
					System.out.println("SUCCESS: Second filter row is displayed");
					clickByXpath("//div[@class='ant-col rule--field']//div[@class='ant-select-selection__placeholder']", "Second Attribute Filter Dropdown");
//					clickByXpath("//div[contains(@style,'105px')]//li[contains(@class,'-dropdown-menu-item')][text()='Gender Type Description']", "Gender Type Description Second Attribute");//For local execution
					clickByXpath("//div[contains(@style,'106px')]//li[contains(@class,'-dropdown-menu-item')][text()='Gender Type Description']", "Gender Type Description Second Attribute");// For Jenkins execution
					clickByXpath("//div[@class='ant-col rule--operator']//div[text()='Select operator']", "Second Attribute Operator Dropdown");
					clickByXpath("//div[contains(@class,'-active')]//li[text()='=='] | //div[contains(@class,'-active')]//li[text()='Equals']", "Equals Condition");
					clickByXpath("//div[@class='group--children']/div[2]//div[text()='Value']", "Second Attribute Value Dropdown");
					clickByXpath("//li[text()='Male']", "'Male' Value");

					clickByXpath("//span[text()='Submit']", "Submit");
					Thread.sleep(1000);
					explicitWaitForVisibility("//span[contains(text(),'Sim Lte')]", "SIM LTE Applied Advanced Filter");
					explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
					Thread.sleep(1000);
					if (isDisplayedByXpath("//span[contains(text(),'Sim Lte')]", "SIM LTE Applied Advanced Filter")) {
						reportStep("Pass", "SUCCESS: Two rules filter condition is added to the 'Added Filters' section");
						System.out.println("SUCCESS: Two rules filter condition is added to the 'Added Filters' section");
						clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");
						explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
						Thread.sleep(1500);
						try {
							if(driver.findElement(By.xpath("//*[contains(text(),'filter conditions has zero results')]")).isDisplayed()) {
								System.out.println("SUCCESS: Filter Condition has zero results");
								reportStep("Pass", "SUCCESS: Filter Condition has zero results");
							}
						} catch (NoSuchElementException e) {
							if(isDisplayedByXpath("//h6[text()='Audience Size']", "Audience Size Text")) {
								reportStep("Pass", "SUCCESS: Audience Discovery page is loaded with new audience search results");
								System.out.println("SUCCESS: Audience Discovery page is loaded with new audience search results");
							}else {
								System.err.println("ERROR: Audience Discovery page is not loaded and Filter results are not displayed");
								reportStep("Fail", "ERROR: Audience Discovery page is not loaded and Filter results are not displayed");					
							}
						}
					}else {
						reportStep("Fail", "ERROR: Two rules filter condition is not added to the 'Added Filters' section");
						System.err.println("ERROR: Two rules filter condition is not added to the 'Added Filters' section");
					}
				} else {
					reportStep("Fail", "ERROR: Second filter row is not displayed");
					System.err.println("ERROR: Second filter row is not displayed");
				}
			} else {
				reportStep("Fail", "ERROR: SIM LTE Capable Indicator selected search is not displayed");
				System.err.println("ERROR: SIM LTE Capable Indicator selected search is not displayed");
			}
		}
		else {
			reportStep("Fail", "ERROR: Advanced Filter is not opened");
			System.err.println("ERROR: Advanced Filter is not opened");
		}
	}
}
