package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
//import java.util.concurrent.TimeUnit;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC061_SearchAttributeAdvFilter extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify all the buttons and functionalities of Advanced Filter";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;


	@Test(groups= {"SmokeTest"})
	public void searchAttributeAdvFilter() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-adv-filter-button']//span", "Advanced Filters");
		Thread.sleep(2000);
		if(isDisplayedByXpath("//div[@id='rcDialogTitle0']", "Advanced Filter Label")) {
			reportStep("Pass", "SUCCESS: Advanced Filter is opened");
			System.out.println("SUCCESS: Advanced Filter is opened");
			clickByXpath("//*[@aria-label='Close']", "Close Advanced Filter");
			clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
			clickByXpath("//div[@id='aud-disc-audience-search-adv-filter-button']//span", "Advanced Filters");
			clickByXpath("//*[@id='advanceFilter-dailog-cancelModelBtn']/span", "Cancel Advanced Filter");
			if (isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
				clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
				clickByXpath("//div[@id='aud-disc-audience-search-adv-filter-button']//span", "Advanced Filters");
				selectSIMLTEAttribute();
				clickByXpath("//button[@class='ant-btn ant-btn-danger ant-btn-sm ant-btn-icon-only']", "Delete Advanced Filter");
				clickByXpath("//div[@class='ant-modal-confirm-btns']/button[@class='ant-btn']", "Cancel Delete Button");
				clickByXpath("//button[@class='ant-btn ant-btn-danger ant-btn-sm ant-btn-icon-only']", "Delete Advanced Filter");
				clickByXpath("//div[@class='ant-modal-confirm-btns']/button[contains(@class,'ant-btn-danger')]", "Yes Delete Button");
				if (isDisplayedByXpath("//div[text()='Select field']", "Empty Filter")) {
					reportStep("Pass", "SUCCESS: Selected Filter was cleared successfully using the delete button");
					System.out.println("SUCCESS: Selected Filter was cleared successfully using the delete button");
					selectSIMLTEAttribute();
					clickByXpath("//div[@class='group--conjunctions']//button[1]", "Not Operator");
					if (isDisplayedByXpath("//div[@id='advSearch-query-content'][contains(text(),'NOT')]", "NOT Operator in SIM LTE Attribute")) {
						reportStep("Pass", "SUCCESS: NOT Operator is applied to SIM LTE Capable Indicator Attribute");
						System.out.println("SUCCESS: NOT Operator is applied to SIM LTE Capable Indicator Attribute");
						clickByXpath("//button[contains(@class,'ADD-RULE')]", "Add Rule");
						if (isDisplayedByXpath("//div[text()='Select field']", "Second Row Empty Attribute")) {
							reportStep("Pass", "SUCCESS: Second filter row is displayed");
							System.out.println("SUCCESS: Second filter row is displayed");
							if (isDisplayedByXpath("//*[@class='group--children']/div[2]//button", "Second row delete button")) {
								clickByXpath("//*[@class='group--children']/div[2]//button", "Second row delete button");
								reportStep("Pass", "SUCCESS: Second row is deleted");
								System.out.println("SUCCESS: Second row is deleted");
								clickByXpath("//*[contains(@class,'ADD-GROUP')]", "Add Group");
								if (isDisplayedByXpath("//div[text()='Select field']", "Second Row Empty Attribute")) {
									reportStep("Pass", "SUCCESS: Second group is displayed with new search filter row");
									System.out.println("SUCCESS: Second group is displayed with new search filter row");
									clickByXpath("//*[@class='group--children hide--line']//button", "Second group row delete button");
									reportStep("Pass", "SUCCESS: Second group row is deleted");
									System.out.println("SUCCESS: Second group row is deleted");
									if (isDisplayedByXpath("//button[contains(@class,'ADD-DELETE')]", "Delete Second Group")) {
										clickByXpath("//button[contains(@class,'ADD-DELETE')]", "Delete Second Group");
										reportStep("Pass", "SUCCESS: Second group is deleted successfully");
										System.out.println("SUCCESS: Second group is deleted successfully");
										clickByXpath("//span[text()='Submit']", "Submit");
										explicitWaitForVisibility("//span[contains(text(),'NOT')]", "Applied Advanced Filter");
										Thread.sleep(1000);
										if (isDisplayedByXpath("//span[contains(text(),'NOT')]", "Applied Advanced Filter")) {
											reportStep("Pass", "SUCCESS: Advanced Filter is reflected on the Audience Discovery page");
											System.out.println("SUCCESS: Advanced Filter is reflected on the Audience Discovery page");
											clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");
											Thread.sleep(3000);
											
											driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
											try {
												if(driver.findElement(By.xpath("//*[contains(text(),'filter conditions has zero results')]")).isDisplayed()) {
													System.out.println("SUCCESS: Filter Condition has zero results");
													reportStep("Pass", "SUCCESS: Filter Condition has zero results");
												}
											} catch (NoSuchElementException e) {
												if(isDisplayedByXpath("//h6[text()='Audience Size']", "Audience Size Text")) {
													reportStep("Pass", "SUCCESS: Audience Discovery page is loaded with new audience search results");
													System.out.println("SUCCESS: Audience Discovery page is loaded with new audience search results");
												}else {
													System.err.println("ERROR: Audience Discovery page is not loaded and Filter results are not displayed");
													reportStep("Fail", "ERROR: Audience Discovery page is not loaded and Filter results are not displayed");					
												}
											}
										} else {
											reportStep("Fail", "ERROR: Advanced Filter is not reflected on the Audience Discovery page");
											System.err.println("ERROR: Advanced Filter is not reflected on the Audience Discovery page");
										}
									} else {
										reportStep("Fail", "ERROR: Second group is not deleted");
										System.err.println("ERROR: Second group is not deleted");
									}
								} else {
									reportStep("Fail", "ERROR: Second group filter row is not added");
									System.err.println("ERROR: Second group filter row is not added");
								}
								
							} else {
								reportStep("Fail", "ERROR: Second row delete button is not displayed");
								System.err.println("ERROR: Second row delete button is not displayed");
							}
						} else {
							reportStep("Fail", "ERROR: Second filter row is not displayed");
							System.err.println("ERROR: Second filter row is not displayed");
						}
					} else {
						reportStep("Fail", "ERROR: NOT Operator is not applied to SIM LTE Capable Indicator Attribute");
						System.err.println("ERROR: NOT Operator is not applied to SIM LTE Capable Indicator Attribute");
					}
				} else {
					reportStep("Fail", "ERROR: Selected Filter was not cleared");
					System.err.println("ERROR: Selected Filter was not cleared");
				}
				
			} else {
				reportStep("Fail", "ERROR: Advanced Filter popup is not closed");
				System.err.println("ERROR: Advanced Filter popup is not closed");
			}
		}
		else {
			reportStep("Fail", "ERROR: Advanced Filter is not opened");
			System.err.println("ERROR: Advanced Filter is not opened");
		}
	}
	public void selectSIMLTEAttribute() {
		clickByXpath("//div[@class='ant-col rule--field']//div[@class='ant-select-selection__placeholder']", "Attribute Filter Dropdown");
		clickByXpath("//li[text()='Sim Lte Capable Indicator']", "Sim Lte Capable Indicator Attribute");
		clickByXpath("//div[@class='ant-col rule--operator']//div[@class='ant-select-selection__placeholder']", "Attribute Operator Dropdown");
		clickByXpath("//li[text()='=='] | //li[text()='Equals']", "Equals Condition");
		clickByXpath("//div[@class='ant-col rule--value']//div[@class='ant-select-selection__placeholder']", "Attribute Value Dropdown");
		clickByXpath("//li[text()='true']", "'true' Value");
	}
}
