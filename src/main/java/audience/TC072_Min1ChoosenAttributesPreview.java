package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC072_Min1ChoosenAttributesPreview extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if minimum of 1 attribute to be available in the chosen attribute section is mandatory for preview";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void min1AttPreview() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='aud-result-previewOption-tab']/span", "Audience Preview and Export");
		explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");
//		Navigate to Preview to extract attributes
		clickByXpath("//*[@id='target-audience-previewOptionBtn']", "Preview Popup Button");
		explicitWaitForVisibility("//*[@id='audience-preview-dialog-title-Grid']", "Select attribute for Preview Popup");
		if(isDisplayedByXpath("//*[@id='audience-preview-dialog-title-Grid']", "Select attribute for Preview Popup")) {
			System.out.println("SUCCESS: Audience Preview Popup is loaded");
			reportStep("Pass", "SUCCESS: Audience Preview Popup is loaded");
			clickByXpath("//*[@id='audience-preview-export-wd-expand-all-wd-select-all']", "Select All Selected Preview");
			clickByXpath("//*[@id='auidence-preview-dialog-btn-remove']/span", "Remove Preview");
			clickByXpath("//button[@id='audience-preview-dialog-submit-btn']", "Preview Button");
			explicitWaitForVisibility("//*[text()='Minimum 1 attribute has to be selected for preview']", "Minimum 1 Attribute Preview Selected Message");
			Thread.sleep(1000);
			if(isDisplayedByXpath("//*[text()='Minimum 1 attribute has to be selected for preview']", "Minimum 1 Attribute Selected Message")) {
				System.out.println("SUCCESS: 'Minimum 1 attribute has to be selected for preview' alert message is displayed");
				reportStep("Pass", "SUCCESS: 'Minimum 1 attribute has to be selected for preview' alert message is displayed");

			}else {
				reportStep("Fail", "ERROR: 'Minimum 1 attribute has to be selected for preview' alert message is not displayed");
				System.err.println("ERROR: 'Minimum 1 attribute has to be selected for preview' alert message is not displayed");
			}
		}else {
			reportStep("Fail","ERROR: Audience Preview section is not loaded");
			System.err.println("ERROR: Audience Preview section is not loaded");
		}
	}
}
