package audience;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;

public class TC050_QuickFilterFieldDisplayedValidation extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To validate Quick Filter Field display";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test (groups= {"SmokeTest"})
	public void quickFilterFieldDisplayedValidation() throws InterruptedException ,IOException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		if(isDisplayedByXpath("//div[@id='qf-card']", "Quick Filter Icon")) {
			clickByXpath("//div[@id='qf-card']", "Quick Filter Icon");
			reportStep("Pass","Clicked on Quick Filter Search button");
			System.out.println("Clicked on Quick Filter Search button");
			Thread.sleep(1000);
			
//			Field Validation
			fieldDisplayedValidation("//div[@id='qf-headerTitle']", "Quick Filter Header Title");
			fieldDisplayedValidation("//*[@id='active_subscriber_indicator-title']", "Active Subscriber Filter Title");
			fieldDisplayedValidation("//*[@id='gender_type_description-title']", "Gender Type Filter Title");
			clickByXpath("//*[@id='POST-formLabel']//span[text()='POST']", "POST Filter Value");
			fieldDisplayedValidation("//*[@id='payment_category_code-title']", "Payment Category Code Filter Title");
			clickByXpath("//span[text()='No']", "'No' Filter Value");
			fieldDisplayedValidation("//span[text()='No']", "'No' Filter Value");
			fieldDisplayedValidation("//span[text()='Yes']", "'Yes' Filter Value");
			fieldDisplayedValidation("//span[text()='Female']", "Female Filter Value");
			fieldDisplayedValidation("//span[text()='Male']", "Male Filter Value");
//			fieldDisplayedValidation("//*[@id='gender-type-description-formGroup']//span[text()='Unknown']", "Unknown Gender Filter Value");
//			fieldDisplayedValidation("//*[@id='payment-category-code-formGroup']//span[text()='Unknown']", "Unknown Payment Category Filter Value");
			fieldDisplayedValidation("//*[@id='POST-formLabel']//span[text()='POST']", "POST Filter Value");
			fieldDisplayedValidation("//*[@id='PRE-formLabel']//span[text()='PRE']", "PRE Filter Value");
			fieldDisplayedValidation("//span[text()='Apply']", "QuickFilter Apply Button");
			fieldDisplayedValidation("//span[text()='Reset']", "QuickFilter Reset Button");
			
		}else {
			reportStep("Fail", "ERROR: Quick Filter Icon is not displayed");
			System.err.println("ERROR: Quick Filter Icon is not displayed");
		}

	}
	public void fieldDisplayedValidation(String xpath, String fieldName) {
		if(isDisplayedByXpath(xpath, fieldName)) {
			reportStep("Pass", "SUCCESS: "+fieldName+" is displayed");
			System.out.println("SUCCESS: "+fieldName+" is displayed");
			
		}else {
			reportStep("Fail", "ERROR: "+fieldName+" is not displayed");
			System.err.println("ERROR: "+fieldName+" is not displayed");
		}
	}
}
