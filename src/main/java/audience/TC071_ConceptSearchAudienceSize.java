package audience;


import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC071_ConceptSearchAudienceSize extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the concepts search audience size change";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void conceptSearchAudienceSize() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			System.out.println("Audience Insight is loaded");
			reportStep("Pass", "Audience Insight is loaded");
			clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
			String audienceSize1=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
			clickByXpath("//button[@title='Open']/span", "Concepts Filter Dropdown");
			Thread.sleep(1000);
			clickByXpath("//ul[@id='aud-disc-audience-concept-search-component-popup']//div[text()='Gen Z']", "Gen Z");
			clickByXpath("//button[@id='aud-disc-audience-concept-search-btn']/span", "Search Button");
			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			String audienceSize2=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
			clickByXpath("//button[@title='Open']/span", "Concepts Filter Dropdown");
			Thread.sleep(1000);
			clickByXpath("//ul[@id='aud-disc-audience-concept-search-component-popup']//div[text()='Beauty Skin Care Fan']", "Beauty Skin Care Fan");
			clickByXpath("//button[@id='aud-disc-audience-concept-search-btn']/span", "Search Button");
//			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			Thread.sleep(3000);
			String audienceSize3="0";
			try {
				if(driver.findElement(By.xpath("//h6[@id='audience-sizeTxt']")).isDisplayed()) {
					audienceSize3=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
				}
			} catch (NoSuchElementException e) {
				try {
					if(driver.findElement(By.xpath("//*[contains(text(),'filter conditions has zero results')]")).isDisplayed()) {
						System.out.println("Concept Search Condition has zero results");
						reportStep("Pass","Concept Search Condition has zero results");
					}
				} catch (NoSuchElementException e2) {
					System.err.println("Audience Insight is not loaded and Concept results are not displayed");
					reportStep("Fail","Audience Insight is not loaded and Concept results are not displayed");					
				}
			}
//			String audienceSize3=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
			System.out.println("Default Audience Size = "+audienceSize1+", Audience Size with one concept search = "+audienceSize2+" and Audience Size with two concepts search = "+audienceSize3);
			reportStep("Pass", "Default Audience Size = "+audienceSize1+", Audience Size with one concept search = "+audienceSize2+" and Audience Size with two concepts search = "+audienceSize3);
			if(!(audienceSize1.equalsIgnoreCase(audienceSize2)&&audienceSize2.equalsIgnoreCase(audienceSize3)&&audienceSize3.equalsIgnoreCase(audienceSize1))) {
				reportStep("Pass", "Audience Size was refreshed successfully and audience size with two concepts search was different from default audience size");
				System.out.println("Audience Size was refreshed successfully and audience size with two concepts search was different from default audience size");
			}else {
				reportStep("Fail", "Audience Size was not refreshed and audience size with two concepts search is same as default audience size");
				System.err.println("Audience Size was not refreshed and audience size with two concepts search is same as default audience size");
			}
		}else {
			System.err.println("Audience Page not loaded");
			reportStep("Fail","Audience Page not loaded");
		}
	}
}
