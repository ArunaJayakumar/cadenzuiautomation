package audience;


import utils.ProjectWrappers;

import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC021_ConceptSearch extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the Concept attributes";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void dateCheck() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(10000);

		try {
			if(driver.findElement(By.xpath("//h6[text()='Audience Size']")).isDisplayed()) {
				System.out.println("Audience Insight is loaded");
			}
		} catch (NoSuchElementException e) {
			System.err.println("Audience Page not loaded");
			reportStep("Fail","Audience Page not loaded");
		}
		clickByXpath("//span[text()='Concepts']", "Concepts");
		clickByXpath("//button[@title='Open']/span", "Concepts Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//div[text()='Gen Z']", "Gen Z");
		clickByXpath("//button/span[text()='Search']", "Search Button");
		try {
			if(driver.findElement(By.xpath("//div[@class='search-tag-main']/div/span[contains(text(),'Gen Z')]")).isDisplayed()) {
				reportStep("Pass","Concept added successfully");
				System.out.println("Concept added successfully");
			}

		} catch (NoSuchElementException e) {
			reportStep("Fail","Concept was not added");
			System.err.println("Concept was not added");
		}
		
		try {
    		String audienceSize=getTextByXpath("//h4[@class='MuiTypography-root MuiTypography-h4']","Audience Size");
    		reportStep("Pass", "Audience size is: "+audienceSize);
    		System.out.println("Audience size is: "+audienceSize);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to get Audience Size");
    		System.err.println("Not able to get Audience Size");
    		
    	}
		
		try {
			if(driver.findElement(By.xpath("//h6[text()='Audience Size']")).isDisplayed()) {
				reportStep("Pass","Audience Insight is loaded");
				System.out.println("Audience Insight is loaded");
			}
		} catch (NoSuchElementException e) {
			try {
				if(driver.findElement(By.xpath("//span[contains(text(),'filter conditions has zero results')]")).isDisplayed()) {
					System.out.println("Concept Search Condition has zero results");
					reportStep("Pass","Concept Search Condition has zero results");
				}
			} catch (NoSuchElementException e2) {
				System.err.println("Audience Insight is not loaded and Concept results are not displayed");
				reportStep("Fail","Audience Insight is not loaded and Concept results are not displayed");					
			}
		}
	}
}
