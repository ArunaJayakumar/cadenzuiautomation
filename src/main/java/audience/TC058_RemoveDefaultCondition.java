package audience;


import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC058_RemoveDefaultCondition extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To successfully apply categorical attribute filters";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void dateCheck() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//span[text()='Clear']", "Clear Button");

		clickByXpath("//button/span[text()='Search']", "Search Button");
		explicitWaitForVisibility("//div[text()='Please add any filters to search !!']", "Add Filter Alert Message");
		Thread.sleep(1000);
		if(isDisplayedByXpath("//div[text()='Please add any filters to search !!']", "Add Filter Alert Message")) {
			reportStep("Pass","SUCCESS: 'Please add any filters to search !!' alert message is displayed added successfully");
			System.out.println("SUCCESS: 'Please add any filters to search !!' alert message is displayed added successfully");
		}else {
			reportStep("Fail","ERROR: alert message was not displayed");
			System.err.println("ERROR: alert message was not displayed");
		}
	}
}
