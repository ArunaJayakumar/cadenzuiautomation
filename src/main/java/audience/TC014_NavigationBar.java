package audience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC014_NavigationBar extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the navigation bar from main menu";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void navigationBar() throws InterruptedException, IOException, ParseException {
		int navBarMenusCount=4;
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//button[@id='appIcon']/span", "App Navigation Icon");
		Thread.sleep(1000);

		List<WebElement> tablist = driver.findElements(By.xpath("//span[@class='MuiTypography-root MuiListItemText-primary MuiTypography-body1 MuiTypography-displayBlock']"));
		ArrayList<String> navTabNames = new ArrayList<String>();
		for(int i=0;i<tablist.size();i++) {
			navTabNames.add(tablist.get(i).getText());
		}
		ArrayList<String> missingNames = new ArrayList<String>();
		//		navTabNames.remove(1);// Retain this for debug
		//		navTabNames.remove(2);// Retain this for debug
		int flag=0;
		for(int i=0;i<navBarMenusCount;i++) {
			if(!(navTabNames.contains(missingNavTabs(i)))) {
				missingNames.add(missingNavTabs(i));
				flag++;
			}
		}
		if(flag==0) {
			System.out.println("Menus displayed on frontend: "+navTabNames+"\nAll Menus are present");
			reportStep("Pass","All Navigation Menus are present. Menus available on frontend are: "+navTabNames);
		}else {
			System.err.println("Missing Menus - "+missingNames);
			reportStep("Fail","Navigation Menus that are absent - "+missingNames);
		}
	}
	public String missingNavTabs(int i) {
		switch (i) {
		case 0:return "Audience Discovery";
		case 1:return "Profiles";
		case 2:return "Saved Audiences";
		case 3:return "Attribute Glossary";
		default: return "";
		}
	}
}
