package audience;


import utils.ProjectWrappers;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC022_ConceptSave extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify the Concept attributes";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void conceptSave() throws InterruptedException, IOException, ParseException {
		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//*[@id='aud-disc-audience-concept-search-expandMore-icon']", "Concepts Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//div[text()='Gen Z']", "Gen Z");
		clickByXpath("//button[@id='aud-disc-audience-concept-search-btn']/span", "Concept Search Button");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMMyyyy");  
		LocalDate now = LocalDate.now();  
		System.out.println("Today is: "+dtf.format(now)); 
		String conceptName="Concept"+dtf.format(now);
		System.out.println("Concept name is: "+conceptName);
		if(isDisplayedByXpath("//div[@class='search-tag-main']//span[contains(text(),'Gen Z')]", "Gen Z Concept")) {
			reportStep("Pass","Gen Z Concept added successfully");
			System.out.println("Gen Z Concept added successfully");
		}else {
			reportStep("Fail","Gen Z Concept was not added");
			System.err.println("Gen Z Concept was not added");
		}

		try {
			String audienceSize1=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
			reportStep("Pass", "Audience size is: "+audienceSize1);
			System.out.println("Audience size is: "+audienceSize1);
		}
		catch(Exception e) {
			reportStep("Fail","Not able to get Audience Size");
			System.err.println("Not able to get Audience Size");
		}

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "Concept More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Concept Save");

		for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
			driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
		enterByXpath("//input[@id='name']", conceptName, "Audience Name");
		clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span", "Save Audience");

		Thread.sleep(2000);
		if(isDisplayedByXpath("//*[text()='Saved the audience successfully']", "Saved Audience Alert")) {
			reportStep("pass", "The audience is saved and appropriate success message is displayed");
			System.out.println("The audience is saved and appropriate success message is displayed");
		}else {
			reportStep("fail", "Audience is not saved ");
			System.err.println("Audience is not saved ");
		}

		try {
			if(driver.findElement(By.xpath("//h6[@id='audience-sizeTxt']")).isDisplayed()) {
				reportStep("Pass","Audience page is loaded");
				System.out.println("Audience page is loaded");
			}
		} catch (NoSuchElementException e) {
			try {
				if(driver.findElement(By.xpath("//span[@id='aud-result-noSubscriber-text1']")).isDisplayed()) {
					System.out.println("Concept Search Condition has zero results");
					reportStep("Pass","Concept Search Condition has zero results");
				}
			} catch (NoSuchElementException e2) {
				System.err.println("Audience page is not loaded and Concept results are not displayed");
				reportStep("Fail","Audience page is not loaded and Concept results are not displayed");					
			}
		}

		//		Including a delete saved concept block so that the script can be executed multiple times in a day without manually deleting the saved audience from frontend
		//		Remove this block once Concept Delete test case is automated and included in sanity suite
		cleanTestData(conceptName);
	}

	public void cleanTestData(String SavedConceptName) throws InterruptedException {
		clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
		enterByXpath("//input[@placeholder='Search Audiences']", SavedConceptName, "Search Audiences");

		clickByXpath("//button[@title='Delete Rows']", "Delete Icon");
	//	clickByXpath("//*[@id='saveAud-checkIcon']", "Save Edit Audience Button");
		clickByXpath("//span[text()='Confirm']", "Save Edit Audience Button");
		Thread.sleep(2000);
	}
}
