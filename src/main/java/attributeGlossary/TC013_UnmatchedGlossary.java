package attributeGlossary;

import java.io.File;
import java.io.FileInputStream;

import utils.ProjectWrappers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC013_UnmatchedGlossary extends ProjectWrappers {

	public  TC013_UnmatchedGlossary() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = "TC013_UnmatchedGlossary";
		description="To Load glossary and extract the all attribute list for verification";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;
	int i=0;

	@Test(groups= {"SmokeTest"})
	public void dateCheck() throws InterruptedException, IOException, ParseException {


		driver=getDriver();

		invokeApp(testName,true);
		Thread.sleep(2000);
		try {
			clickByXpath("//*[@id='Path_1831_1_']", "Glossary Icon");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[text()='Attribute Glossary Description']")).isDisplayed();
			//takeSnap(i++);
		} catch (NoSuchElementException e) {
			//takeSnap(i++);
			reportStep("Fail","Glossary page not loaded");
		}
		List<WebElement> tablist = driver.findElements(By.xpath("//button[contains(@id,'vertical-tab')]/span"));
		ArrayList<String> attList = new ArrayList<String>();
		for(int i=0;i<tablist.size();i++) {
			driver.findElement(By.xpath("//button[@id='vertical-tab-"+i+"']/span")).click();
			List<WebElement> temp = driver.findElements(By.xpath("//tr/th/b"));
			for(int j=0;j<temp.size();j++) {
				attList.add(temp.get(j).getText());
			}
		}
		Collections.sort(attList);
		FileInputStream fis = new FileInputStream("./Data/GlossaryAttributeList.xlsx");
		XSSFWorkbook excel = new XSSFWorkbook(fis);
		int sheets = excel.getNumberOfSheets();
		ArrayList<String> excelAttList = new ArrayList<String>();
		for(int i=0;i<sheets;i++) {
			if(excel.getSheetName(i).equalsIgnoreCase("1A")) {
				XSSFSheet sheet = excel.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				Row rowPosition = rows.next();
				Iterator<Cell> cellPosition = rowPosition.cellIterator();
				while(cellPosition.hasNext()) {
					Cell cellNameObj = cellPosition.next();
					if(cellNameObj.getStringCellValue().equalsIgnoreCase("Glossary Attribute List")) {
						while(rows.hasNext()) {
							Cell cp=rows.next().cellIterator().next();
							excelAttList.add(cp.getStringCellValue());
						}
					}
				}
			}
		}
		excel.close();
		Collections.sort(excelAttList);
		
		System.out.println("Excel list is: \n"+excelAttList);
		ArrayList<String> temp = new ArrayList<String>();
		if(attList.equals(excelAttList)) {
			System.out.println("Attributes extracted from Glossary is same as attributes mentioned in requirements");
			reportStep("Pass","Attributes extracted from Glossary is same as attributes mentioned in requirements");
		}else {
			if(attList.size() >= excelAttList.size()) {
				temp=tempArrList(attList, temp);
				temp.removeAll(excelAttList);
				excelAttList.removeAll(attList);
				System.out.println("1Mismatched Glossary Attributes -"+temp+"\nMismatched Requirement Attributes - "+excelAttList);
				reportStep("Fail",("Mismatched Glossary Attributes -"+temp+"\nMismatched Requirement Attributes - "+excelAttList));
			}else {
				temp=tempArrList(excelAttList, temp);
				temp.removeAll(attList);
				attList.removeAll(excelAttList);
				System.out.println("2Mismatched Glossary Attributes -"+attList+"\nMismatched Requirement Attributes - "+temp);
				reportStep("Fail",("Mismatched Glossary Attributes -"+attList+"\nMismatched Requirement Attributes - "+temp));
			}
		}
	}
	public ArrayList<String> tempArrList(ArrayList<String> x, ArrayList<String> temp) {
		for(int i=0;i<x.size();i++) {
			temp.add(x.get(i));
		}
		return temp;
	}
	public void takeSnap(int i) {
		try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			File dest= new File("./screenshots/Audience"+i+".png");
			FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
			System.err.println("Error while taking screenshot");

		} catch (IOException e) {
			System.err.println("Error while creating/copying the image file");
		}
		i++;
	}
}
