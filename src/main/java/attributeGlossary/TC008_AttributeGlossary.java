package attributeGlossary;

import utils.GetAttributeForRole;
import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC008_AttributeGlossary extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = "TC008_Glossary";
		description="To Load glossary and extract the all attribute list and verification";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void glossaryCheck() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(2000);
		
		try {
			Thread.sleep(7000);
			clickByXpath("//*[@id='Path_1831_1_']", "Glossary Icon");
			//clickByXpath("(//div[@class='MuiListItemIcon-root'])[4]", "Glossary Icon");
			Thread.sleep(2000);
			if(driver.findElement(By.xpath("//div[text()='Attribute Glossary Description']")).isDisplayed()) {
				System.out.println("Glossary page is loaded");
				reportStep("Pass", "Glossary page is loaded");
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","Glossary page not loaded");
			System.err.println("Glossary page not loaded");
		}
		
		//Get Attributes list from glossary page
		List<WebElement> tablist = driver.findElements(By.xpath("//button[contains(@id,'vertical-tab')]/span"));
		ArrayList<String> glossaryAttList = new ArrayList<String>();
		for(int i=0;i<tablist.size();i++) {
			driver.findElement(By.xpath("//button[@id='vertical-tab-"+i+"']/span")).click();
			List<WebElement> temp = driver.findElements(By.xpath("//tr/th/b"));
			for(int j=0;j<temp.size();j++) {
				glossaryAttList.add(temp.get(j).getText());
			}
		}
		Collections.sort(glossaryAttList);
		System.out.println("Attributes list in the Filter is: \n"+glossaryAttList);
		
		//List of elements which are case incorrect in attribute filter (attlist)
				String[] applicationString= {"Monthly Roaming Uc", "Monthly Vas Uc", "Nsat Indicator", "Nsat Probability", "Postpaid Arpu Oc 30days", "Postpaid Arpu Oc 60days", "Postpaid Arpu Oc 90days", "Postpaid Arpu Rc 30days", "Postpaid Arpu Rc 60days", "Postpaid Arpu Rc 90days", "Postpaid Arpu Uc 30days", "Postpaid Arpu Uc 60days", "Postpaid Arpu Uc 90days"};
				ArrayList<String> applicationList=new ArrayList<String>(Arrays.asList(applicationString));
				
				//List of elements which are case incorrect in requirement
				String[] requirementString= {"Monthly Roaming UC", "Monthly Vas UC", "NSAT Indicator", "NSAT Probability", "Postpaid Arpu OC 30days", "Postpaid Arpu OC 60days", "Postpaid Arpu OC 90days", "Postpaid Arpu RC 30days", "Postpaid Arpu RC 60days", "Postpaid Arpu RC 90days", "Postpaid Arpu UC 30days", "Postpaid Arpu UC 60days", "Postpaid Arpu UC 90days"};
				ArrayList<String> requirementList=new ArrayList<String>(Arrays.asList(requirementString));
				
		
		//Get attributes list for the specific role from Excel
		ArrayList<String> excelGlossaryList=GetAttributeForRole.getAttributesWOBlacklist("Attributes", "1A");
		System.out.println("List from excel is: \n"+excelGlossaryList);
		
		//Validation
		ArrayList<String> tempGlossary = new ArrayList<String>();
		if(glossaryAttList.equals(excelGlossaryList)) {
			System.out.println("Attributes extracted from Glossary is same as attributes mentioned in requirements");
			reportStep("Pass","Attributes extracted from Glossary is same as attributes mentioned in requirements");
	reportStep("Fail",("Glossary attributes that are missing from Requirements - "+glossaryAttList+"\nRequirement Attributes that are missing from frontend - "+tempGlossary));
			}	if(glossaryAttList.equals(excelGlossaryList)) {
				System.out.println("Attributes extracted from Glossary is same as attributes mentioned in requirements");
				reportStep("Pass","Attributes extracted from Glossary is same as attributes mentioned in requirements");
			}else {
				if(glossaryAttList.size() >= excelGlossaryList.size()) {
					tempGlossary=tempArrList(glossaryAttList, tempGlossary);
					tempGlossary.removeAll(excelGlossaryList);
					tempGlossary.removeAll(applicationList);
					excelGlossaryList.removeAll(glossaryAttList);
					excelGlossaryList.removeAll(requirementList);
					if(tempGlossary.size()==0 && excelGlossaryList.size()==0) {
						System.out.println("Glossary attributes are listed as expected in Glossary");
						reportStep("Pass", "Glossary attributes are listed as expected in Glossary");	
					}else {
					System.err.println("\n1Glossary attributes that are missing from Requirements - "+tempGlossary+"\n1Requirement Attributes that are missing in Glossary - \n"+excelGlossaryList);
					reportStep("Fail",("\n1Glossary attributes that are missing from Requirements - "+tempGlossary+"\n1Requirement Attributes that are missing in Glossary - \n"+excelGlossaryList));
					}
					}else {
					tempGlossary=tempArrList(excelGlossaryList, tempGlossary);
					tempGlossary.removeAll(glossaryAttList);
					tempGlossary.removeAll(applicationList);
					glossaryAttList.removeAll(excelGlossaryList);
					glossaryAttList.removeAll(requirementList);
					if(tempGlossary.size()==0 && glossaryAttList.size()==0) {
						System.out.println("Glossary attributes are listed as expected in Glossary");
						reportStep("Pass", "Glossary attributes are listed as expected in Glossary");	
					}else {
					System.err.println("\n2Glossary attributes that are missing from Requirements - \n"+glossaryAttList+"\n2Requirement Attributes that are missing in Glossary - \n"+tempGlossary);
					reportStep("Fail",("\n2Glossary attributes that are missing from Requirements - \n"+glossaryAttList+"\n2Requirement Attributes that are missing in Glossary - \n"+tempGlossary));
				}
					}
			}
		}
	
	public ArrayList<String> tempArrList(ArrayList<String> x, ArrayList<String> temp) {
		for(int i=0;i<x.size();i++) {
			temp.add(x.get(i));
		}
		return temp;
	}
}
