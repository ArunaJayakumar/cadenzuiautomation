package sharedAudienceConcepts;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
import utils.GenericWrappers;

public class TC240_ShareFromSavedException_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience cannot be shared to other users from the saved audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;
	

	@Test(groups= {"SmokeTest"})
	public void shareFromSavedException_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();	
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
		String Username_2 = GenericWrappers.Username_2;
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		createsSavedAudTestData();

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "SharedFromSectionException", "Search Audiences");
		
		clickByXpath("//button[@title='Share Audience']", "Share");
		
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 
		
		try {
			if(driver.findElement(By.xpath("//*[text()='Shared the audience successfully']")).isDisplayed())
				reportStep("fail", "The success message should not be displayed as no users are selected");	
			else
				reportStep("pass", "The audience is not shared because no users are selected and no message is displayed");
		}
		catch(Exception e) {
			reportStep("pass", "The audience alert is not displayed because no users are selected and no message is displayed");
		}	
		
		Thread.sleep(1000);
		driver.navigate().refresh();
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "SharedFromSectionException", "Search Audiences");
		clickByXpath("//button[@title='Share Audience']", "Share");
		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']/span","Cancel"); 
		
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Shared the audience successfully')]")).isDisplayed())
				reportStep("fail", "The success message should not be displayed as no users are selected");	
			else
				reportStep("pass", "The audience is not shared because no users are selected and no message is displayed");
		}
		catch(Exception e) {
			reportStep("pass", "The audience alert is not displayed because no users are selected and no message is displayed");
		}	
		
		TrackActivity(Username_2);
		Thread.sleep(2000);
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);

//		Thread.sleep(5000);
//				
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
				
		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
		
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has shared SharedFromSectionException with you as viewer')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("fail","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("pass","Notification is not visible as the owner did not share the audience with the user");
		}
	}
	
	public void createsSavedAudTestData() throws InterruptedException {
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			selectFilters(false);
			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
			clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");
			for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
				driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
			enterByXpath("//input[@id='name']", "SharedFromSectionException", "Audience Name");
			clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span", "Save Audience");
			explicitWaitForVisibility("//*[text()='Saved the audience successfully']", "Saved Audience Alert");
		}else {
			reportStep("Fail", "ERROR: Audience Discovery Page is not loaded");
			System.err.println("ERROR: Audience Discovery Page is not loaded");
		}
	}
	public void TrackActivity(String Username_2) throws InterruptedException {
	
		clickByXpath("//a[contains(text(),'SharedFromSectionException')]", "Click Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		if(isDisplayedByXpath("//div[contains(text(),'No Recent Activity')]","No Activity"))
			reportStep("pass","Shared Activity is not visible as user did not share the audience");
		else
			reportStep("fail","Shared Activity is present in the Track Activity Section");
		clickAction.click(Trackicon).build().perform();				
	}
	
	
}