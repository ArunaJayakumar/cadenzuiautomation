package sharedAudienceConcepts;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC265_UpdateShareAudNoRaceException_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the user cannot update an existing shared audience when the audience name is same as the existing audience";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void updateShareAudNoRaceException_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'EditedAudName')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		updateFilters(false);

		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.CONTROL + "a");
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.DELETE);
		enterByXpath("//input[@id='name']", "UpdateNewAud", "Audience Name");

		clickByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update"); 
		Thread.sleep(2000);
		try {
			if(driver.findElement(By.xpath("//*[text()='A saved audience already exists with the same name']")).isDisplayed())
				reportStep("Pass", "User is not able to change the audience name as audience with the same name already exists");
			else
				reportStep("Fail", "Audience name is changed eventhough the name is same as the existing audience");
		}
		catch(Exception e) {
			reportStep("Fail", "Same Audience Name alert is not displayed");
		}
	}
}
