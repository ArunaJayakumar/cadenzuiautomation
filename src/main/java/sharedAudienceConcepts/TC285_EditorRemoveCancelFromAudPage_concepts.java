package sharedAudienceConcepts;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC285_EditorRemoveCancelFromAudPage_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if all the users are not removed from an audience by editor with reshare from the shared audience page";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void editorRemoveCancelFromAudPage_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true,true);
		String Username_1 = GenericWrappers.Username_1;
				
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		updateSharedAudTestData();
		Thread.sleep(1000);
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		
		List<WebElement> userCount = driver.findElements(By.xpath("//div[contains(@class,'shared-User jss')]"));
		int i = -1;
		int editor = 0;
		for(WebElement user:userCount) {
			if(user.getText().contains(Username_1)) {
				editor = i;
				break;
			}
			else {
				i=i+1;
			}
		}
		List<WebElement> removeAllUsers = driver.findElements(By.xpath("//button[@testid='userAccess']")); 

		for(int j=0;j<removeAllUsers.size();j++) {
			if(j==editor)
				continue;
			removeAllUsers.get(j).click();
			Thread.sleep(1000);
			
			clickByXpath("//li[contains(text(),'Remove')]","Remove"); 	
		}
		Thread.sleep(2000);
		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']/span","Cancel button clicked by user");
				
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Updated the audience successfully')]")).isDisplayed())
				reportStep("fail", "Users are removed from the audience");
			else
				reportStep("pass", "Users are not removed from the audience as the operation was cancelled");
		}
		catch(Exception e) {
			reportStep("pass", "Alert message was not displayed as the operation was cancelled");
		}	

		Thread.sleep(1000);
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		try {
			if(driver.findElement(By.xpath("//span[contains(text(),'"+GenericWrappers.Username_3+"')]")).isDisplayed())
				reportStep("Pass", "User is not removed by editor with reshare");
			else
				reportStep("Fail", "User with Editor with reshare role has removed the other user from the audience");
		}
		catch(Exception e) {
			reportStep("Fail","Username is not displayed as the user was removed from the audience");
		}
	}

	public void updateSharedAudTestData() throws InterruptedException {
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'UpdateNewAud')]", "Click Audience Name");
		Actions actions = new Actions(driver);
		
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		WebElement selectUser2=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser2.sendKeys(GenericWrappers.Username_3);
		Thread.sleep(1000);
		
		WebElement element1 = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element1).click().perform();
		
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 		
	}
}
