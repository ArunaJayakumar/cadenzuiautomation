package sharedAudienceConcepts;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC287_UpdateShareDeletebyOwner_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the user cannot update an existing shared audience when the audience is deleted by the owner";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;	

	@Test(groups= {"SmokeTest"})
	public void updateShareDeletebyOwner_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		String originalWindow = driver.getWindowHandle();	
		invokeApp(testName, true);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		//Login as 2nd user
		driver.switchTo().newWindow(WindowType.WINDOW);
		invokeApp(testName, true,true);
		String secondWindow = driver.getWindowHandle();	
					
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'EditedAudName')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		//Switch back to the old tab or window
		driver.switchTo().window(originalWindow);
		
		Thread.sleep(1000);
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		Thread.sleep(1000);
		clickByXpath("//button[@title='Delete Rows']","Delete Icon");
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']","Confirm Delete");

		if(driver.findElement(By.xpath("//div[contains(text(),'Deleted the audience successfully')]")).isDisplayed())
			reportStep("Pass", "Owner deletes the shared audience");
		else
			reportStep("Fail", "Failed to delete the shared audience by owner");
		
		driver.switchTo().window(secondWindow);
		
		selectFilters(false);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");

		clickByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update"); 
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Shared Audience is deleted by owner')]")).isDisplayed())
				reportStep("pass", "The audience cannot be updated as it was deleted by the owner after the user has loaded it");
			else
				reportStep("fail", "Audience is not updated");
		}
		catch(Exception e) {
			reportStep("Fail","Update Audience alert message is not shown");

		}
		WebElement element = driver.findElement(By.xpath("//button[@id='saveAud-dialogModel-updateBtn']"));
		String enabled = element.getAttribute("disabled");
		if(enabled.contains("true")) {
			reportStep("pass", "The update button is disabled");
		}
		else
			reportStep("fail","The update button is not disabled");	
		cleanData();
	}
	public void cleanData() {
		driver.navigate().refresh();
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "SameNamedAud", "Search Audiences");
		clickByXpath("//button[@title='Delete Rows']","Delete Icon");
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']","Confirm Delete");
		
	}
}

