package sharedAudienceConcepts;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC288_UpdateShareDelByOwnerException_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the user cannot update an existing shared audience when the audience name is same as the existing audience";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;	

	@Test(groups= {"SmokeTest"})
	public void updateShareDelByOwnerException_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		String originalWindow = driver.getWindowHandle();	
		invokeApp(testName, true);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		//Login as 2nd user
		driver.switchTo().newWindow(WindowType.WINDOW);
		invokeApp(testName, true,true);
		String secondWindow = driver.getWindowHandle();	

		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			selectFilters(false);
			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
			clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");
			for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
				driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
			enterByXpath("//input[@id='name']", "SameNamedAud", "Audience Name");
			clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span", "Save Audience");
		}else {
			reportStep("Fail", "ERROR: Audience Discovery Page is not loaded");
			System.err.println("ERROR: Audience Discovery Page is not loaded");
		}
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'UpdateNewAud')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		//Switch back to the old tab or window
		driver.switchTo().window(originalWindow);
		
		Thread.sleep(1000);
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		Thread.sleep(1000);
		clickByXpath("//button[@title='Delete Rows']","Delete Icon");
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']","Confirm Delete");

		if(driver.findElement(By.xpath("//div[contains(text(),'Deleted the audience successfully')]")).isDisplayed())
			reportStep("Pass", "Owner deletes the shared audience");
		else
			reportStep("Fail", "Failed to delete the shared audience by owner");
		
		driver.switchTo().window(secondWindow);
		
		updateFilters(false);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");

		clickByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update"); 
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Shared Audience is deleted by owner')]")).isDisplayed())
				reportStep("pass", "The audience cannot be updated as it was deleted by the owner after the user has loaded it");
			else
				reportStep("fail", "Audience is not updated");
		}
		catch(Exception e) {
			reportStep("Fail","Audience alert message is not saved");

		}
		WebElement element = driver.findElement(By.xpath("//button[@id='saveAud-dialogModel-updateBtn']"));
		String enabled = element.getAttribute("disabled");
		if(enabled.contains("true")) {
			reportStep("pass", "The update button is disabled");
		}
		else
			reportStep("fail","The update button is not disabled");	
		
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.CONTROL + "a");
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.DELETE);
		enterByXpath("//input[@id='name']", "SameNamedAud", "Enter Audience Name");
		
		clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']","Save Audience");

		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'A Saved Audience already exists with the same name')]")).isDisplayed())
				reportStep("Pass", "User is not able to change the audience name as audience with the same name already exists");
			else
				reportStep("Fail", "Audience name is changed eventhough the name is same as the existing audience");
			
		}
		catch(Exception e) {
			reportStep("Fail", "Alert Message not displayed");
		}
	}
}

