package sharedAudienceConcepts;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC279_EditorCancelFromSection_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience is not shared to other users from the shared audience section when the operation is cancelled by users with the role of editor with reshare";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void editorCancelFromSection_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
		String Username_2 = GenericWrappers.Username_2;
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		updateSharedAudTestData(Username_2);
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
								
		//Login as 2nd user
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
				
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");

		
		Actions actions = new Actions(driver);
		
		clickByXpath("//button[@title='Share Audience']", "Share");
		
		
		
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(GenericWrappers.Username_3);

		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();

		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']/span","Cancel button clicked by user");
		
		try {
			if(driver.findElement(By.xpath("//*[text()='Updated the audience successfully']")).isDisplayed())
				reportStep("Fail", "The audience is not shared from the shared audience section by user with editor with reshare role as user cancelled the operation");
			else
				reportStep("pass", "The audience is shared from the shared audience section by user with editor with reshare role");
		}
		catch(Exception e) {
			reportStep("pass","Shared Audience alert message is not shown as user cancelled the operation");
		}	
		try {
			if(driver.findElement(By.xpath("//span[contains(text(),'"+GenericWrappers.Username_3+"')]")).isDisplayed())
				reportStep("Fail", "Audience not shared to others by editor with reshare as the operation was cancelled");
			else
				reportStep("Pass", "Audience is shared to others by editor with reshare user");
		}
		catch(Exception e) {
			reportStep("pass","User3 is not visible as the user2 has cancelled the operation");
		}
	
	}
	public void updateSharedAudTestData(String Username_2) throws InterruptedException {
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		clickByXpath("//a[contains(text(),'UpdateNewAud')]", "Saved Audience");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions actions = new Actions(driver);
		
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(Username_2);
				
		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();
			
		clickByXpath("//button[@testid='userAccess']", "User role Popup");

		clickByXpath("//div[contains(text(),'Editor')]","Editor Role"); 
		clickByXpath("//input[@type='checkbox' and contains(@class,'jss')]","Reshare Option");

		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Share"); 				
	}
}