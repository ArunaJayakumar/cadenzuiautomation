package sharedAudienceConcepts;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;
import utils.GenericWrappers;

public class TC242_ShareExistingAudienceException_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience cannot be shared to other users after loading the saved audience";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;
	
	

	@Test(groups= {"SmokeTest"})
	public void shareExistingAudienceException_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		createsSavedAudTestData();

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");
		clickByXpath("//a[contains(text(),'ShareExistingException')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 
		
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Shared the audience successfully')]")).isDisplayed())
				reportStep("fail", "The success message should not be displayed as no users are selected");	
			else
				reportStep("pass", "The audience is not shared because no users are selected and no message is displayed");
		}
		catch(Exception e) {
			reportStep("pass", "The audience alert is not shown because no users are selected and no message is displayed");
		}	
		Thread.sleep(1000);
		
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		
		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']/span","Cancel"); 	
		
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Shared the audience successfully')]")).isDisplayed())
				reportStep("fail", "The success message should not be displayed as no users are selected");	
			else
				reportStep("pass", "The audience is not shared because no users are selected and no message is displayed");
		}
		catch(Exception e) {
			reportStep("pass", "The audience alert is not shown because no users are selected and no message is displayed");
		}
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Saved Audiences");
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);		
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
		
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has shared ShareExistingException with you as viewer')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("fail","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("pass","Notification is not visible as the owner did not share the audience with the user");
		}		
	}
	public void createsSavedAudTestData() throws InterruptedException {
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			selectFilters(false);
			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
			clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");
			for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
				driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
			enterByXpath("//input[@id='name']", "ShareExistingException", "Audience Name");
			clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span", "Save Audience");
			explicitWaitForVisibility("//*[text()='Saved the audience successfully']", "Saved Audience Alert");
		}else {
			reportStep("Fail", "ERROR: Audience Discovery Page is not loaded");
			System.err.println("ERROR: Audience Discovery Page is not loaded");
		}
	}
}
