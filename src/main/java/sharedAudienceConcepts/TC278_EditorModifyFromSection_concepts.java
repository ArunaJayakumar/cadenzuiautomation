package sharedAudienceConcepts;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC278_EditorModifyFromSection_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the other users role can be modified from the shared audience section by users with the role of editor with reshare";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void editorModifyFromSection_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true,true);
				
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		clickByXpath("//button[@title='Share Audience']", "Share");
		
		List<WebElement> userCount = driver.findElements(By.xpath("//div[contains(@class,'shared-User jss')]"));
		int i = -1;
		for(WebElement user:userCount) {
			if(user.getText().contains(GenericWrappers.Username_3)) {
				break;
			}
			else {
				i=i+1;
			}
		}
		List<WebElement> listButton = driver.findElements(By.xpath("//button[@testid='userAccess']"));
		listButton.get(i).click();

		clickByXpath("//div[contains(text(),'Editor')]","Editor Role"); 

		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 
		Thread.sleep(2000);
		try {
			if(driver.findElement(By.xpath("//*[text()='Updated the audience']")).isDisplayed())
				reportStep("pass", "User role has been modified from the shared audience section by user with editor with reshare role");
			else
				reportStep("fail", "Audience is not shared ");
		}
		catch(Exception e) {
			reportStep("Fail","Shared Audience alert message is not shown");
		}	
		driver.navigate().refresh();
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");

//		clickByXpath("//button[@title='Share Audience']", "Share");
//
//		Thread.sleep(2000);
//		String User3_Role = listButton.get(i).getText();
//		
//		if(User3_Role.contains("Editor"))
//			reportStep("Pass", "User role has been changed by editor with reshare");
//		else
//			reportStep("Fail", "User role is not changed from viewer to editor by editor with reshare");
//
//		clickByXpath("//button[@id='editInsight-dialogBox-cancel-btn']/span","Cancel button clicked by user");
	}
}
