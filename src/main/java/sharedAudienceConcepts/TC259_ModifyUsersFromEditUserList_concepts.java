package sharedAudienceConcepts;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC259_ModifyUsersFromEditUserList_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the shared audience user list can be modified by clicking the edit icon from the shared audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void modifyUsersFromEditUserList_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		Thread.sleep(2000);
		Actions actions = new Actions(driver);
		
		clickByXpath("//button[(@title='Edit')]", "Pencil Icon is Clicked");
		explicitWaitForVisibility("//*[name()='svg' and @data-icon='user-edit']","User List Edit Icon");
		WebElement clickEditIcon3 = driver.findElement(By.xpath("//*[name()='svg' and @data-icon='user-edit']"));
		actions.click(clickEditIcon3).build().perform();
		clickByXpath("//button[@testid='userAccess']", "User role Popup");

		Thread.sleep(1000);

		clickByXpath("//div[contains(text(),'Editor')]","Editor Role"); 
		
		Thread.sleep(1000);
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 
		Thread.sleep(2000);
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Updated the audience')]")).isDisplayed())
				reportStep("pass", "User role is modified successfully by clicking the edit icon and appropriate success message is displayed");
			else
				reportStep("fail", "User role not modified from the shared audience section by clicking the edit icon");
		}
		catch(Exception e) {
			reportStep("Fail","Shared Audience alert message is not shown");
		}	
		
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
				
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
		
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has changed you from viewer to editor for EditedAudName')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("pass","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("fail","Notification is not visible");
		}	
	}
}

