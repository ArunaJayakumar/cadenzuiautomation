package sharedAudienceConcepts;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC291_UpdateReload_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the user can reload the previous user's changes";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;	

	@Test(groups= {"SmokeTest"})
	public void updateReload_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		String originalWindow = driver.getWindowHandle();	
		invokeApp(testName, true);
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		//Login as 2nd user
		driver.switchTo().newWindow(WindowType.WINDOW);
		invokeApp(testName, true,true);
		String secondWindow = driver.getWindowHandle();	
					
//		Thread.sleep(4000);
//						
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'UpdateNewAud')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		//Switch back to the old tab or window
		driver.switchTo().window(originalWindow);
		
		Thread.sleep(1000);
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'UpdateNewAud')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		updateFilters(false);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");

		clickByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update"); 

		try {
			if(driver.findElement(By.xpath("//*[text()='Sucessfully updated the audience !']")).isDisplayed())
				reportStep("pass", "The audience was updated by owner after other user opened it");
			else
				reportStep("fail", "Audience is not updated");
		}
		catch(Exception e) {
			reportStep("Fail","Update Audience alert message is not shown");
		}
		Thread.sleep(1000);
		driver.switchTo().window(secondWindow);
		
		selectFilters(false);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");

		clickByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update"); 
		
		Thread.sleep(1000);
		clickByXpath("//span[contains(text(),'Reload') and @class='MuiButton-label']","Reload");
		
		Thread.sleep(1000);
		clickByXpath("//span[contains(text(),'Yes') and @class='MuiButton-label']","Confirm Reload");
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Audience fetched successfully')]")).isDisplayed())
				reportStep("pass", "The changes made by 1st user are reloaded");
			else
				reportStep("fail", "Audience is not updated");
		}
		catch(Exception e) {
			reportStep("Fail","Update Audience alert message is not saved");
		}
		if (getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size").contains(audienceSize))
			reportStep("pass","Changes made are successfully Reloaded");
		else
			reportStep("fail","Changes are not reflected");
		
	}
}

