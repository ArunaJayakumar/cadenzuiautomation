package sharedAudienceConcepts;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC290_UpdateOverwriteCancel_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if other user's changes are not overwritten when the current user cancels the operation";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;	

	@Test(groups= {"SmokeTest"})
	public void updateCancel_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		String originalWindow = driver.getWindowHandle();	
		invokeApp(testName, true);
		String Username_2 = GenericWrappers.Username_2;
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		//Login as 2nd user
		driver.switchTo().newWindow(WindowType.WINDOW);
		invokeApp(testName, true,true);
		String secondWindow = driver.getWindowHandle();	
//		Thread.sleep(4000);
//						
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'UpdateNewAud')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		//Switch back to the old tab or window
		driver.switchTo().window(originalWindow);
		
		Thread.sleep(1000);
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'UpdateNewAud')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		selectFilters(false);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");

		clickByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update"); 

		try {
			if(driver.findElement(By.xpath("//*[text()='Sucessfully updated the audience !']")).isDisplayed())
				reportStep("pass", "The audience was updated by owner after other user opened it");
			else
				reportStep("fail", "Audience is not updated");
		}
		catch(Exception e) {
			reportStep("Fail","Update Audience alert message is not shown");
		}
		Thread.sleep(1000);
		driver.switchTo().window(secondWindow);
		
		updateFilters(false);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		String overwrittenAudienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");

		clickByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update"); 
		
		clickByXpath("//span[contains(text(),'Cancel') and @class='MuiButton-label']","Cancel Overwrite");
		
		if(isDisplayedByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update Button"))
			reportStep("pass", "Audience update is cancelled by the user");
		else
			reportStep("fail", "Audience is updated");

		Thread.sleep(1000);
		//clickByXpath("//button[@id='saveAud-dialogModel-closeIcon']","Close Modal");
		driver.switchTo().window(originalWindow);
		Thread.sleep(1000);
		
		driver.navigate().refresh();
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
				
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_2+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has modified the UpdateNewAud.')]"));
		if(notiUsername.size()>0 && notification.size()>0) 
			reportStep("fail","Notification is visible in the shared user notification area");
		else 
			reportStep("pass","Notification is not visible as overwrite was cancelled");
		actions2.click(clickNoti).build().perform();
		Thread.sleep(2000);
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'UpdateNewAud')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		if (overwrittenAudienceSize.contains(getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size")))
			reportStep("fail","Overwritten changes are reflected");
		else
			reportStep("pass","Overwritten changes are not reflected as overwrite was cancelled");
		
		TrackActivity();
	}
	
	public void TrackActivity() throws InterruptedException {
		Thread.sleep(2000);
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		List<WebElement> activity = driver.findElements(By.xpath("(//div[@class='MuiCardContent-root'])[1]//*[contains(text(),'Filters Added')]"));
		List<WebElement> filter = driver.findElements(By.xpath("(//div[@class='MuiCardContent-root'])[1]//*[contains(text(),'"+GenericWrappers.UpdateConcept+"')]"));
		if(activity.size()>0 && filter.size()>0) { 
			reportStep("fail","Update Activity is visible in the track activity section");
		}
		else {
			reportStep("pass","Activity is not present in the Track Activity as the update was cancelled");
		}
		clickAction.click(Trackicon).build().perform();		
	}	
}

