package sharedAudienceConcepts;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
import utils.GenericWrappers;

public class TC247_ViewerSharedSectionException_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the share button is disabled for the audience where the user has the role of viewer in the shared audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void viewerSharedSectionException_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();	
		invokeApp(testName, true);
		String Username_2 = GenericWrappers.Username_2;
//		Thread.sleep(5000);
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");

		Actions actions = new Actions(driver);
		
		clickByXpath("//button[@title='Share Audience']", "Share");
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(Username_2);
		
		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();

		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 
		
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "ShareExistingException", "Search Audiences");
		
		List<WebElement> shareButton = driver.findElements(By.xpath("//button[@title='Share Audience']")); 
		if((shareButton.size()) == 0) {
			reportStep("pass", "Share button is disabled for the viewer");
		}
		else {
			reportStep("fail", "Share button is enabled for the viewer");
		}
	} 
}