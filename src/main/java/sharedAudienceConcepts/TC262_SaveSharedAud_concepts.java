package sharedAudienceConcepts;

import java.io.IOException;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC262_SaveSharedAud_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the user can save the existing shared audience";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void saveSharedAud_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'EditedAudName')]", "Created Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		updateFilters(false);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.CONTROL + "a");
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.DELETE);
		enterByXpath("//input[@id='name']", "UpdateNewAud", "Audience Name");

		WebElement createNew=driver.findElement(By.xpath("//button[@id='saveAud-dialogModel-saveBtn']/span")); 
		JavascriptExecutor exec4=(JavascriptExecutor)driver;
		exec4.executeScript("arguments[0].click();", createNew);

		try {
			if(driver.findElement(By.xpath("//*[text()='Saved the audience successfully']")).isDisplayed())
				reportStep("pass", "The audience is saved and appropriate success message is displayed");
			else
				reportStep("fail", "Audience is not saved ");
		}
		catch(Exception e) {
			reportStep("Fail","Save Audience alert message is not saved");

		}
			
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		Thread.sleep(4000);
		if(driver.findElement(By.xpath("//a[contains(text(),'UpdateNewAud')]")).isDisplayed())
			reportStep("Pass", "'Shared Audience is edited and saved as a new audience");
		else
			reportStep("Fail", "Audience is not saved");		
	}
}
