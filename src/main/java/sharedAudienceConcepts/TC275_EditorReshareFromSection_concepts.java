package sharedAudienceConcepts;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC275_EditorReshareFromSection_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience can be shared to other users from the shared audience section by users with the role of editor with reshare";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void editorReshareFromSection_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true,true);
				
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "UpdateNewAud", "Search Audiences");
		Thread.sleep(2000);
		Actions actions = new Actions(driver);
		
		clickByXpath("//button[@title='Share Audience']", "Share");		
		
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(GenericWrappers.Username_3);
		
		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();
		
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 	
		Thread.sleep(2000);
		try {
			if(driver.findElement(By.xpath("//*[text()='Updated the audience successfully']")).isDisplayed())
				reportStep("pass", "The audience is shared from the shared audience section by user with editor with reshare role");
			else
				reportStep("fail", "Audience is not shared ");
		}
		catch(Exception e) {
			reportStep("Fail","Shared Audience alert message is not shown");
		}	
		Thread.sleep(2000);
		if(driver.findElement(By.xpath("//span[contains(text(),'"+GenericWrappers.Username_3+"')]")).isDisplayed())
			reportStep("Pass", "User with Editor with reshare role has shared an audience");
		else
			reportStep("Fail", "Audience not shared to others by editor with reshare");
	}
}