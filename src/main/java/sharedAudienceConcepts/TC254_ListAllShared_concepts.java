package sharedAudienceConcepts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC254_ListAllShared_concepts  extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the shared audience section is shown with the required columns";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void listAllShared_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		
		//Expected Column Names
		String[] exptabnames = {"Actions","Audience Name", "Audience Size", "Profile Date","Created Date", "Last Updated","Shared Date","Owned By","Shared With","Description","Segments"};
		ArrayList<String> expectedColumnNames = new ArrayList<String>();
		for(String i:exptabnames) {
			expectedColumnNames.add(i);
		}
		System.out.println("Expected Column Names - "+expectedColumnNames);

		//		Extracted Column Names
		List<WebElement> actualColumnObj = driver.findElements(By.xpath("//th[@scope='col']/span"));
		ArrayList<String> actualColumnNames = new ArrayList<String>();
		for(WebElement i:actualColumnObj) {
			actualColumnNames.add(i.getText());
		}
		List<WebElement> actualColumnObjExtra = driver.findElements(By.xpath("//th[@scope='col']/div"));
		for(WebElement i:actualColumnObjExtra) {
			actualColumnNames.add(i.getText());
		}
		System.out.println("Actual Column Names - "+actualColumnNames);

		//		Validate shared audience page contents
		if(actualColumnNames.equals(expectedColumnNames)) 
			reportStep("Pass", "SUCCESS: All column names are correct");
		else
			reportStep("Fail", "ERROR: Columns do not match with the expected column name");
				
		
		List<WebElement> noRecordsDisplay = driver.findElements(By.xpath("//*[text()='No records to display']"));
		if(noRecordsDisplay.size()>0)
			reportStep("pass", "'No records to display' message is displayed because no audience is present in the shared audience section");
		else {
			List<WebElement> shareAudiences = driver.findElements(By.xpath("//tr[@class='MuiTableRow-root']"));
			if(shareAudiences.size()>1) 
				reportStep("pass", "Audiences shared with this user/shared by this user is displayed");
			else
				reportStep("fail","Shared audiences not displayed");
		}
			
	}

}

