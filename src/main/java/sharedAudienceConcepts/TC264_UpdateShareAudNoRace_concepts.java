package sharedAudienceConcepts;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC264_UpdateShareAudNoRace_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the user can update an existing shared audience";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;	

	@Test(groups= {"SmokeTest"})
	public void updateShareAudNoRace_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//span[contains(text(),'EditedAudName')]", "Created Audience Name");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		updateFilters(false);

		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");

		clickByXpath("//button[@id='saveAud-dialogModel-updateBtn']/span","Update"); 
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Sucessfully updated the audience !')]")).isDisplayed())
				reportStep("pass", "The audience is updated and appropriate success message is displayed");
			else
				reportStep("fail", "Audience is not updated ");
		}
		catch(Exception e) {
			reportStep("Fail","Update Audience alert message is not saved");
		}
		
		TrackActivity();
		//Login as 2nd user
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);			
//		Thread.sleep(5000);
//				
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
				
		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
				
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has modified the EditedAudName.')]"));
		if(notiUsername.size()>0 && notification.size()>0) {  
			reportStep("pass","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("fail","Notification is not visible");
		}		
	}

	public void TrackActivity() throws InterruptedException {
				
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		boolean activity = isDisplayedByXpath("//span[text()='Filters Added']","Track Filters");
		if(activity) { 
			reportStep("pass","Update Activity is present in the Track Activity Section");
		}
		else {
			reportStep("fail","Update Activity is not visible");
		}
		clickAction.click(Trackicon).build().perform();		
	}
}

