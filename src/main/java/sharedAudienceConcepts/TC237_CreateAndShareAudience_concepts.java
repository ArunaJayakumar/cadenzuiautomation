package sharedAudienceConcepts;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
import utils.GenericWrappers;

public class TC237_CreateAndShareAudience_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience can be shared to other users immediately after creating an audience and a notification is sent";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;


	@Test(groups= {"SmokeTest"})
	public void createAndShareAudience_concepts() throws InterruptedException ,IOException {
		driver=getDriver();	
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
		String Username_2 = GenericWrappers.Username_2;
		
//		Thread.sleep(50000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");	
		selectFilters(false);	
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-concept-more-save-btn']//span", "Save");
		for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
			driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
		enterByXpath("//input[@id='name']", "CreateandShare", "Audience Name");

		clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span","Save Audience"); 
		try {
			if(driver.findElement(By.xpath("//*[text()='Saved the audience successfully']")).isDisplayed())
				reportStep("pass", "The audience is saved and appropriate success message is displayed");
			else
				reportStep("fail", "Audience is not saved");
		}
		catch(Exception e) {
			reportStep("Fail","Save Audience alert message is not saved");

		}
		
		Actions actions = new Actions(driver);
		
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(Username_2);
		Thread.sleep(1000);
		
		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Share"); 
		try {
			if(driver.findElement(By.xpath("//*[text()='Shared the audience successfully']")).isDisplayed())
				reportStep("pass", "The audience is shared and appropriate success message is displayed");
			else
				reportStep("fail", "Audience is not shared");
		}
		catch(Exception e) {
			reportStep("Fail","Shared Audience alert message is not shown");
		}	
		Thread.sleep(2000);				
		TrackActivity(Username_2);
		//Login as 2nd user
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
		
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
		
		actions2.click(clickNoti).build().perform();	
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has shared CreateandShare with you as viewer')]"));
		if(notiUsername.size()>0 && notification.size()>0) {
			reportStep("pass","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("fail","Notification is not visible");
		}		
	}

	public void TrackActivity(String Username_2) throws InterruptedException {
		Thread.sleep(2000);
		
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		boolean trackUsername = isDisplayedByXpath("//span[text()='"+ Username_2 +"']","Track Username");
		boolean activity = isDisplayedByXpath("//span[text()='Shared']","Track Activity");
		if(trackUsername && activity) { 
			reportStep("pass","Shared Activity is present in the Track Activity Section");
		}
		else {
			reportStep("fail","Shared Activity is not visible");
		}
		clickAction.click(Trackicon).build().perform();
	}
	
}
