package sharedAudienceConcepts;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC268_DeleteLocalShared_concepts  extends ProjectWrappers {
	
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the user can delete the copy of his audience from the shared audience section";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;
	
	@Test(groups= {"SmokeTest"})
	public void deleteLocalShared_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true,true);
				
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Audiences");
		
		clickByXpath("//button[@title='Delete Rows']", "Delete Audience");
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']","Confirm Delete");
		Thread.sleep(2000);
		if(driver.findElement(By.xpath("//div[contains(text(),'You have successfully removed yourself from the shared audience')]")).isDisplayed())
			reportStep("Pass", "Shared User has deleted his shared audience copy from the shared audience section");
		else
			reportStep("Fail", "Audience is not deleted");
		
		driver.close();
		driver=getDriver();
		invokeApp(testName, true);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		
		enterByXpath("//input[@placeholder='Search Audiences']", "EditedAudName", "Search Shared Audiences");
		
		if(driver.findElement(By.xpath("//*[contains(text(),'Audience is not shared with any Users')]")).isDisplayed())
			reportStep("Pass", "Shared User deleted their copy of the audience, changes are reflected to the Owner");
		else
			reportStep("Fail", "Shared User deleted their copy of the audience, changes are not reflected to the Owner");
	}
}
