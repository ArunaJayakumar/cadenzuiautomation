package sharedAudienceConcepts;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
import utils.GenericWrappers;

public class TC249_RemoveUsersFromAudiencePage_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the users can be removed from an audience by the owner from the shared audience page";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void removeUsersFromAudiencePage_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
		String Username_2 = GenericWrappers.Username_2;
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		enterByXpath("//input[@placeholder='Search Audiences']", "CreateandShare", "Search Audiences");
		clickByXpath("//span[contains(text(),'CreateandShare')]", "Click Audience Name");
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		clickByXpath("//button[@testid='userAccess']", "User role Popup");
		
		clickByXpath("//li[contains(text(),'Remove')]","Remove User"); 
		
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Submit"); 
		
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Updated the audience successfully')]")).isDisplayed())
				reportStep("pass", "The user is removed from the audience and appropriate success message is displayed");
			else
				reportStep("fail", "User not removed from the audience");
		}
		catch(Exception e) {
			reportStep("Fail","Shared Audience alert message is not shown");
		}	
		
		TrackActivity(Username_2);
		Thread.sleep(2000);
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
		
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
				
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has removed you from CreateandShare')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("pass","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("fail","Notification is not visible");
		}			
	}

	public void TrackActivity(String Username_2) throws InterruptedException {
	
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		boolean trackUsername = isDisplayedByXpath("//span[text()='"+Username_2+"']","Track Username");
		boolean activity = isDisplayedByXpath("//span[text()='UnShared']","Track Username");
		if(trackUsername && activity) { 
			reportStep("pass","UnShared Activity is present in the Track Activity Section");
		}
		else {
			reportStep("fail","UnShared Activity is not visible");
		}
		clickAction.click(Trackicon).build().perform();
	}
}