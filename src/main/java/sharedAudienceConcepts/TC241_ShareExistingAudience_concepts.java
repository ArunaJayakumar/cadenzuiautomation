package sharedAudienceConcepts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;
import utils.GenericWrappers;

public class TC241_ShareExistingAudience_concepts extends ProjectWrappers {
	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience can be shared to other users after loading the saved audience";
		author="Prakash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void shareExistingAudience_concepts() throws InterruptedException ,IOException {
		
		driver=getDriver();		
		invokeApp(testName, true);
		String Username_1 = GenericWrappers.Username_1;
		String Username_2 = GenericWrappers.Username_2;
//		Thread.sleep(5000);
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		enterByXpath("//input[@placeholder='Search Audiences']", "SharedFromSectionException", "Search Audiences");
		clickByXpath("//a[contains(text(),'SharedFromSectionException')]", "Click Audience");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		Actions actions = new Actions(driver);

		clickByXpath("//button[@id='aud-disc-audience-concept-more-btn']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-share-aud-button']//span", "Share");
		WebElement selectUser=driver.findElement(By.cssSelector("input[class='MuiInputBase-input MuiFilledInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiFilledInput-inputAdornedEnd']")); 
		selectUser.sendKeys(Username_2);
		Thread.sleep(1000);
		
		WebElement element = driver.findElement(By.xpath("//div[@id='user-info-container']"));		
		actions.moveToElement(element).click().perform();
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']/span","Share"); 
		
		try {
			if(driver.findElement(By.xpath("//*[contains(text(),'Shared the audience successfully')]")).isDisplayed())
				reportStep("pass", "The audience is shared and appropriate success message is displayed");
			else
				reportStep("fail", "Audience is not shared ");
		}
		catch(Exception e) {
			reportStep("Fail","Shared Audience alert message is not shown");
		}	
		
		TrackActivity(Username_2);
		
		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		
		enterByXpath("//input[@placeholder='Search Audiences']", "SharedFromSectionException", "Search Audiences");
		if(driver.findElement(By.xpath("//span[contains(text(),'SharedFromSectionException')]")).isDisplayed())
			reportStep("Pass", "'Audience is present in the shared audience section of the Owner");
		else
			reportStep("Fail", "Audience is not shared");
		driver.close();
		driver=getDriver();
		invokeApp(testName, true,true);
		
//		Thread.sleep(5000);
//		
//		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
//		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
//		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions actions2 = new Actions(driver);
		WebElement clickNoti = driver.findElement(By.xpath("//span[@class='MuiBadge-root']//*[name()='svg' and contains(@class,'MuiSvgIcon-root jss')]"));
		
		actions2.click(clickNoti).build().perform();
		List<WebElement> notiUsername = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(., '"+Username_1+"')]")); 
		List<WebElement> notification = driver.findElements(By.xpath("(//div[contains(@class, 'exKQrA')])[1]//div[contains(.,'has shared SharedFromSectionException with you as viewer')]"));
		if(notiUsername.size()>0 && notification.size()>0) { 
			reportStep("pass","Notification is visible in the shared user notification area");
		}
		else {
			reportStep("fail","Notification is not visible");
		}
		cleanData();
	}
	
	public void cleanData() throws InterruptedException, IOException {
		driver.close();
		driver=getDriver();
		invokeApp(testName, true);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//div[@id='nav-savedAudiences-btn']", "Saved Audiences Section");
		clickByXpath("//button[@id='shared-audience-tab']","Shared Audiences Section");
		clickByXpath("//input[@type='checkbox']","Checkbox to display the audience owned by Owner");
		
		enterByXpath("//input[@placeholder='Search Audiences']", "SharedFromSectionException", "Search Audiences");

		clickByXpath("//button[@title='Delete Rows']","Delete");
		clickByXpath("//button[@id='editInsight-dialogBox-submit-btn']","Confirm Delete");

		Thread.sleep(2000);
		
		if(driver.findElement(By.xpath("//div[contains(text(),'Deleted the audience successfully')]")).isDisplayed())
			reportStep("Pass", "Successfully cleaned up test data");
		else
			reportStep("Fail", "Failed to delete test data");
	}
	
	public void TrackActivity(String Username_2) throws InterruptedException {
		
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		Actions clickAction = new Actions(driver);
		WebElement Trackicon = driver.findElement(By.xpath("//*[name()='svg' and @id='aud-disc-activity-icon']"));
		clickAction.click(Trackicon).build().perform();
		boolean trackUsername = driver.findElement(By.xpath("//span[text()='"+ Username_2 +"']")).isDisplayed();
		boolean activity = driver.findElement(By.xpath("//span[text()='Shared']")).isDisplayed();
		if(trackUsername && activity) { 
			reportStep("pass","Shared Activity is present in the Track Activity Section");
		}
		else {
			reportStep("fail","Shared Activity is not visible");
		}
		clickAction.click(Trackicon).build().perform();	
	}
}

