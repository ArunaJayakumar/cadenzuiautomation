package profiles;

//import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
//import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC028_Profile9Digits extends ProjectWrappers{

	public  TC028_Profile9Digits() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To verify Profiles Filter search";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;
	//@Parameters({ "environment" })
	@Test(groups= {"SmokeTest"})
	public void profileTest() throws InterruptedException, IOException {

		//		int i=0;
		String currentEnv = new GenericWrappers().env;
		String nineDigitMSISDN = "";
		if(currentEnv.equalsIgnoreCase("prod")) {
			nineDigitMSISDN = "272135531";
		}else if (currentEnv.equalsIgnoreCase("uat")) {
			nineDigitMSISDN = "270007300";
		}else if (currentEnv.equalsIgnoreCase("dev")) {
			nineDigitMSISDN = "123456789";
		}

		driver=getDriver();

		invokeApp(testName, true);

		Thread.sleep(3000);

		try {
			WebElement ele=	driver.findElement(By.xpath("(//div[@class='MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button'])[1]"));
			JavascriptExecutor exec=(JavascriptExecutor)driver;
			exec.executeScript("arguments[0].click();", ele);

			Thread.sleep(3000);
			reportStep("Pass","Profiles is launched successfully");
			System.out.println("Profiles is launched successfully");
		}
		catch(Exception e) {
			reportStep("Fail","Not able to open Profiles");
			//	takeSnap(i++);
		}
		try {
			enterByXpath("//input[@id='profileSearchBar']", nineDigitMSISDN, "MSISDN value");//277920690     279050454
			clickByXpath("//button[@id='profile-search-icon']", "Search icon");
			explicitWaitForVisibility("//button[@id='profile-res-summary']/span", "Summary");
			reportStep("Pass","Entered MSISDN and clicked Search");
			System.out.println("Entered MSISDN and clicked Search");
		}
		catch(Exception e) {
			reportStep("Fail","Not able to Search Profiles");
			System.out.println("Not able to Search Profiles");
		}
		try {
			if(driver.findElement(By.xpath("//span[text()='Summary']")).isDisplayed()) {
				reportStep("Pass", "MSISDN search is successful");
				System.out.println("MSISDN search is successful");
			}
			else if(driver.findElement(By.xpath("//div[text()='MSISDN not present']")).isDisplayed()) {
				reportStep("Fail","MSISDN is not present. Please check MSISDN");
				System.out.println("MSISDN is not present. Please check MSISDN");
			}
			else {
				reportStep("Fail", "MSISDN search is unsuccessful.");
				System.out.println("MSISDN search is unsuccessful");
			}
		}
		catch(Exception e) {
			reportStep("Fail","Profiles result is not available.");
		}

	}

	//	public void takeSnap(int i) {
	//		try {
	//			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	//			File dest= new File("./screenshots/Profiles"+i+".png");
	//			FileUtils.copyFile(tmp, dest);
	//		} catch (WebDriverException e) {
	//			System.err.println("Error while taking screenshot");
	//
	//		} catch (IOException e) {
	//			System.err.println("Error while creating/copying the image file");
	//		}
	//		i++;
	//	}

}
