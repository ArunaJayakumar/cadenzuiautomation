package profiles;


import utils.GenericWrappers;
import utils.GetAttributeWithBL;
import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC013_AttributeMSISDNProfile extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = "TC013_AttributeMSISDNProfile";
		description="To Load MSISDN profile and extract the all attribute list for verification";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void profileAttributes() throws InterruptedException, IOException, ParseException {

		driver=getDriver();

		invokeApp(testName,true);
		Thread.sleep(2000);
		
		try {
			WebElement ele=	driver.findElement(By.xpath("(//div[@class='MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button'])[1]"));
	    	JavascriptExecutor exec=(JavascriptExecutor)driver;
	    	exec.executeScript("arguments[0].click();", ele);
	    	
			Thread.sleep(2000);
			if(driver.findElement(By.xpath("//*[@id='profileSearchBar']")).isDisplayed()) {
				System.out.println("Profile Search page is loaded");
				reportStep("Pass", "Profile Search page is loaded");
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","Profile Search page not loaded");
			System.err.println("Profile Search page not loaded");
		}
		
		enterByXpath("//input[@id='profileSearchBar']", new GenericWrappers().msisdnData(), "MSISDN value");
		clickByXpath("//button[@id='profile-search-icon']", "Search icon");
		explicitWaitForVisibility("//button[@id='profile-res-summary']/span", "Summary");
		Thread.sleep(1000);
		
		try {
			if(driver.findElement(By.xpath("//span[text()='Msisdn Value']")).isDisplayed()) {
				System.out.println("MSISDN Profile page loaded successfully");
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","MSISDN Profile page not loaded");
			System.err.println("MSISDN Profile page not loaded");
		}
		
		clickByXpath("//span[text()='Details']", "Details");
		List<WebElement> tablist = driver.findElements(By.xpath("//*[@id='scrollable-auto-tabpanel-1']//div[@class='MuiTabs-flexContainer']/button"));
		ArrayList<String> attList = new ArrayList<String>();
		int size=tablist.size();
		for(int i=0;i<size;i++) {
			driver.findElement(By.xpath("//button[@aria-controls='scrollable-auto-tabpanel-"+i+"']")).click();
			List<WebElement> temp = driver.findElements(By.xpath("//td[@class='MuiTableCell-root']"));
			for(int j=0;j<temp.size();j++) {
				String tempName=temp.get(j).getText();
				attList.add(tempName);
			}
		}
		Collections.sort(attList);
		System.out.println("Attributes list in Profiles is: \n"+attList);
		
		//Get attributes list for specific role from excel sheet
		ArrayList<String> excelAttList=GetAttributeWithBL.getAttributesWithBlacklist("Attributes", "1A");
		System.out.println("List from excel is: \n"+excelAttList);
		
		//Validation
		ArrayList<String> temp = new ArrayList<String>();
		if(attList.equals(excelAttList)) {
			System.out.println("Attributes extracted from MSISDN Profile is same as attributes mentioned in requirements");
			reportStep("Pass","Attributes extracted from MSISDN Profile is same as attributes mentioned in requirements");
		}else {
			if(attList.size() >= excelAttList.size()) {
				temp=tempArrList(attList, temp);
				temp.removeAll(excelAttList);
				excelAttList.removeAll(attList);
				System.err.println("MSISDN Profile attributes that are missing from Requirements - "+temp+"\nRequirement Attributes that are missing from frontend - "+excelAttList);
				reportStep("Fail",("MSISDN Profile attributes that are missing from Requirements - "+temp+"\nRequirement Attributes that are missing from frontend - "+excelAttList));
			}else {
				temp=tempArrList(excelAttList, temp);
				temp.removeAll(attList);
				attList.removeAll(excelAttList);
				System.err.println("2MSISDN Profile attributes that are missing from Requirements - "+attList+"\nRequirement Attributes that are missing from frontend - "+temp);
				reportStep("Fail",("MSISDN Profile attributes that are missing from Requirements - "+attList+"\nRequirement Attributes that are missing from frontend - "+temp));
			}
		}
	}
	public ArrayList<String> tempArrList(ArrayList<String> x, ArrayList<String> temp) {
		for(int i=0;i<x.size();i++) {
			temp.add(x.get(i));
		}
		return temp;
	}
}
