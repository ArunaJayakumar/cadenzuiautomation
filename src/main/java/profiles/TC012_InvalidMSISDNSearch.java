package profiles;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC012_InvalidMSISDNSearch extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To verify Profiles Filter search shows appropriate message when invalid MSISDN is entered";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void invalidMsisdnSearch() throws InterruptedException, IOException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		try {
			clickByXpath("//div[@id='nav-profiles-btn']", "Profile Nav Icon");
			Thread.sleep(2000);
			reportStep("Pass","Profiles is launched successfully");
			System.out.println("Profiles is launched successfully");
		}
		catch(Exception e) {
			reportStep("Fail","Not able to open Profiles");
			System.err.println("Not able to open Profiles");
		}
		try {
			enterByXpath("//input[@id='profileSearchBar']", "989898989", "MSISDN value");
			clickByXpath("//button[@id='profile-search-icon']", "Profile Search Icon");
			reportStep("Pass","Entered MSISDN and clicked Search");
			System.out.println("Entered MSISDN and clicked Search");
		}
		catch(Exception e) {
			reportStep("Fail","Not able to Search Profiles");
			System.err.println("Not able to Search Profiles");
		}
		Thread.sleep(1000);
		try {
			explicitWaitForVisibility("//div[@class='ant-result-title']", "MSISDN error");
			String text=getTextByXpath("//div[@class='ant-result-title']", "MSISDN error");
			if(text.equalsIgnoreCase("The number you entered is invalid"))
			{
				reportStep("Pass", "MSISDN search result for incorrect MSISDN, and 'The number you entered is invalid' message is displayed as expected");
				System.out.println("MSISDN search result for incorrect MSISDN, and 'The number you entered is invalid' message is displayed as expected");
			}
			else{
				reportStep("Fail","'The number you entered is invalid' error is not shown for incorrect MSISDN value");
				System.err.println("'The number you entered is invalid' error is not shown for incorrect MSISDN value");
			}

		}
		catch(Exception e) {
			reportStep("Fail","Profiles result is not available.");
		}
	}
}
