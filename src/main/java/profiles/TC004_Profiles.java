package profiles;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GenericWrappers;
import utils.ProjectWrappers;

public class TC004_Profiles extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To verify Profiles Filter search";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;
	//@Parameters({ "environment" })
	@Test(groups= {"SmokeTest"})
	public void profileTest() throws InterruptedException, IOException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		try {
			clickByXpath("//div[@id='nav-profiles-btn']", "Profile Nav Icon");
			Thread.sleep(2000);
			reportStep("Pass","Profiles is launched successfully");
			System.out.println("Profiles is launched successfully");
		}
		catch(Exception e) {
			reportStep("Fail","Not able to open Profiles");
		}
		try {
			enterByXpath("//input[@id='profileSearchBar']", new GenericWrappers().msisdnData(), "MSISDN value");
			clickByXpath("//button[@id='profile-search-icon']", "Search icon");
			reportStep("Pass","Entered MSISDN and clicked Search");
			System.out.println("Entered MSISDN and clicked Search");
		}
		catch(Exception e) {
			reportStep("Fail","Not able to Search Profiles");
			System.out.println("Not able to Search Profiles");
		}
		explicitWaitForVisibility("//button[@id='profile-res-summary']/span", "Summary");
		Thread.sleep(1000);
		try {
			if(driver.findElement(By.xpath("//button[@id='profile-res-summary']/span")).isDisplayed()) {
				reportStep("Pass", "MSISDN search is successful");
				System.out.println("MSISDN search is successful");
			}
			else if(driver.findElement(By.xpath("//div[text()='The number you entered is invalid']")).isDisplayed()) {
				reportStep("Fail","MSISDN is not present. Please check MSISDN");
				System.out.println("MSISDN is not present. Please check MSISDN");
			}
			else {
				reportStep("Fail", "MSISDN search is unsuccessful");
				System.out.println("MSISDN search is unsuccessful");
			}
		}
		catch(Exception e) {
			reportStep("Fail","Profiles result is not available.");
		}
	}
}
