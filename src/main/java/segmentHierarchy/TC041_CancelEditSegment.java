package segmentHierarchy;

import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;
public class TC041_CancelEditSegment extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify cancel edit segment";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void cancelEditSegment() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);

		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy");

		//		Verify Cancel Edit Segment
		clickByXpath("//*[@id='aud-insightSegment-moreIcon']", "Edit Segment Hierarchy");
		explicitWaitForVisibility("//*[@id='editDisHierarchy-dialogBox-selectedAttr']", "Selected Attributes Text");
		if(isDisplayedByXpath("//*[@id='editDisHierarchy-dialogBox-selectedAttr']", "Selected Attributes Text")) {
			reportStep("Pass", "Opened Edit Segment Hierarchy");
			System.out.println("Opened Edit Segment Hierarchy");
			clickByXpath("//*[@id='editDisHierarchy-dialogBox-cancel-btn']/span", "Cancel Edit Segment");
			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			if(isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy")) {
				reportStep("Pass", "Edit Segment Hierarchy was closed successfully by using cancel button");
				System.out.println("Edit Segment Hierarchy was closed successfully by using cancel button");
			}else {
				reportStep("Fail", "ERROR: Edit Segment Hierarchy was not closed when user clicked the cancel button");
				System.out.println("ERROR: Edit Segment Hierarchy was not closed when user clicked the cancel button");
			}
		}else {
			reportStep("Fail", "ERROR: Failed to load Edit Segment History");
			System.out.println("ERROR: Failed to load Edit Segment History");
		}
	}
}
