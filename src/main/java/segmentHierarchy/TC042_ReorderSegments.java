package segmentHierarchy;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
public class TC042_ReorderSegments extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if Segments can be reordered throught Edit Segment Hierarchy";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void reorderSegments() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy");

		clickByCSS("div.segment-header-section > svg", "Edit Hierarchy");
		Thread.sleep(1000);
		if(isDisplayedByXpath("//div[text()='Select Discovery Hierarchy']","Segment Hierarchy")) {
			reportStep("Pass", "Opened Edit Segment Hierarchy");
			System.out.println("Opened Edit Segment Hierarchy");
			
			List<WebElement> selectedSegObj = driver.findElements(By.xpath("//div[@id='editDisHierarchy-selectedAttr-droppable']//span[contains(@id,'-selectedAttrLabel')]"));
			ArrayList<String> selectedSegList = new ArrayList<String>();
			for(WebElement i:selectedSegObj) {
				selectedSegList.add(i.getText());
			}
			if(selectedSegList.size()==2) {
				verifySegmentReorder();
			}else if(selectedSegList.size()==3) {
				clickByXpath("//span[@title='Un-select']", "Un-select");
				verifySegmentReorder();
			}else {
				reportStep("Fail", "ERROR: Less than 2 OR More than 3 attributes are displayed under selected attributes");
				System.err.println("ERROR: Less than 2 OR More than 3 attributes are displayed under selected attributes");
			}
		}else {
			reportStep("Fail", "ERROR: Edit Segment Hierarchy popup is not displayed");
			System.err.println("ERROR: Edit Segment Hierarchy popup is not displayed");
		}
	}
	public void verifySegmentReorder() {
		clickByXpath("//*[@id='editDisHierarchy-Sim-Lte-Capable-Indicator-availableAttrSelectMovRight-icon']", "Sim LTE Segment Right Move");
		if(isDisplayedByXpath("//*[@id='editDisHierarchy-Sim-Lte-Capable-Indicator-selectedAttrLabel']", "SimLTE Selected Segment")) {
			reportStep("Pass", "Sim LTE Capable Indicator is selected as the 3rd attribute successfully");
			System.out.println("Sim LTE Capable Indicator is selected as the 3rd attribute successfully");
			for(int i=0;i<2;i++) { //Clicking on move up button twice
				clickByXpath("//*[@id='editDisHierarchy-Sim-Lte-Capable-Indicator-selectedAttr-moveup-lessIcon']", "SimLTE Move Up Arrow");
			}
			clickByXpath("//span[text()='Submit']", "Submit Segment");
			explicitWaitForVisibility("//a/div[contains(text(),'Sim Lte Capable Indicator')]", "SimLTE Displayed Segment");
			if(isDisplayedByXpath("//a/div[contains(text(),'Sim Lte Capable Indicator')]", "SimLTE Displayed Segment")) {
				reportStep("Pass", "Sim LTE Capable Indicator was reordered to 1st attribute and is displayed in Audience Segment Hierarchy");
				System.out.println("Sim LTE Capable Indicator was reordered to 1st attribute and is displayed in Audience Segment Hierarchy");
			}else {
				reportStep("Fail", "ERROR: Sim LTE Capable Indicator is not displayed in Audience Segment Hierarchy");
				System.err.println("ERROR: Sim LTE Capable Indicator is not displayed in Audience Segment Hierarchy");
			}
		}else {
			reportStep("Fail", "ERROR: Sim LTE Capable Indicator is not selected");
			System.err.println("ERROR: Sim LTE Capable Indicator is not selected");
		}		
	}
}
