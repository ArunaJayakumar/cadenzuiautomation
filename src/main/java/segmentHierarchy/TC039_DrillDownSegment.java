package segmentHierarchy;

import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

//import utils.GenericWrappers;
import utils.ProjectWrappers;
public class TC039_DrillDownSegment extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if segments hierarchy can be explored by drilling down and up";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void drillDownSegment() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);

		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy");

		//Changing Node 2 from Age Bracket to Brand Type Code
		clickByCSS("div.segment-header-section > svg", "Edit Hierarchy");
		explicitWaitForVisibility("//span[@title='Un-select']", "Edit Hierarchy Popup");
		Thread.sleep(1000);
		clickByXpath("//span[@id='editDisHierarchy-Age-Bracket-Name-selectedAttr-UnSelectBtn']", "Un-select Age Bracket Name");
		enterByXpath("//input[@id='editDisHierarchy-searchAttribute-textField']", "Brand Type Code", "SegmentName");
		clickByXpath("//*[@id='editDisHierarchy-Brand-Type-Code-availableAttrLabel']/span[@class='segments-move-icon-right']", "Payment Category Code Move to Right");
		Thread.sleep(1000);
		clickByXpath("//span[text()='Submit']", "Submit Segment");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		Thread.sleep(1000);


		String defaultAudSize = driver.findElement(By.xpath("//h4[@id='audience-sizeValue']")).getText();

		//		Extract Prepaid Audience Size from Segment Hierarchy
		String preAudSize = driver.findElement(By.xpath("//*[@id='1:-STUDENT-countlabel']")).getText();

		//		Navigate to Node Level 1
		clickByXpath("//*[@id='1:-STUDENT-node-level']", "Student Lifestage Pre Node Level");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		//		Verify updated audience size
		String updatedAudSize = driver.findElement(By.xpath("//h4[@id='audience-sizeValue']")).getText();
		reportStep("Pass", "Prepaid Audience Size = "+preAudSize+" and Updated Audience Size = "+updatedAudSize);
		System.out.println("Prepaid Audience Size = "+preAudSize+" and Updated Audience Size = "+updatedAudSize);
		isDisplayedByXpath("//a/div[contains(text(),'Brand Type Code')]", "Brand Type Code Segment Hierarch");
		if(updatedAudSize.equalsIgnoreCase(preAudSize)) {
			reportStep("Pass", "Node Level 1 drill down was successful");
			System.out.println("Node Level 1 drill down was successful");

			//			Navigate to Node Level 2
			reportStep("Pass", "Navigating to Node Level 2");
			System.out.println("Navigating to Node Level 2");
			Thread.sleep(2000);
			clickByXpath("//*[@id='GHP-PREPAID-node-level'][contains(@style,'left: 0%')]", "GHP-PREPAID Brand Type Code Segment Hierarchy");
			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			Thread.sleep(1000);
			if(isDisplayedByXpath("//div[contains(text(),'GHP-PREPAID')]", "GHP-PREPAID Segment Breadcrumb")) {
				reportStep("Pass", "Node Level 2 drill down was successful");
				System.out.println("Node Level 2 drill down was successful");

				//				Navigate to First Node
				clickByXpath("//a/div[contains(text(),'Lifestage')]", "Lifestage Segment Breadcrumb");
				explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
				updatedAudSize = driver.findElement(By.xpath("//h4[@id='audience-sizeValue']")).getText();
				reportStep("Pass", "Default Audience Size = "+defaultAudSize+" and Updated Audience Size = "+updatedAudSize);
				System.out.println("Default Audience Size = "+defaultAudSize+" and Updated Audience Size = "+updatedAudSize);
				if(updatedAudSize.equalsIgnoreCase(defaultAudSize)) {
					reportStep("Pass", "Navigating back to First Node was successful");
					System.out.println("Navigating back to First Node was successful");
				}else {
					reportStep("Fail", "Navigating back to First Node was unsuccessful");
					System.err.println("Navigating back to First Node was unsuccessful");
				}
			}else {
				reportStep("Fail", "Node Level 2 drill down was unsuccessful");
				System.err.println("Node Level 2 drill down was unsuccessful");
			}
		}else {
			reportStep("Fail", "Node Level 1 drill down was unsuccessful");
			System.err.println("Node Level 1 drill down was unsuccessful");
		}
	}
}
