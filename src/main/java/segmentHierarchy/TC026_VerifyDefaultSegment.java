package segmentHierarchy;

import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC026_VerifyDefaultSegment extends ProjectWrappers{

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if default Segment Hierarchy is displayed";
		author="Aruna";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void defaultSegmentHierarchy() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		boolean flag=false;

		if(isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy")) {

			String title=driver.getCurrentUrl();
			if(title.contains("35.154.")) {
				flag=isDisplayedByXpath("//div[text()=' Brand Type Code ']", "Brand Type Code");
				if(flag) {
					System.out.println("Brand Type Code segment is displayed as expected");
					reportStep("Pass", "Brand Type Code segment is displayed as expected");
				}
				else {
					System.err.println("Brand Type Code segment is NOT displayed as expected");
					reportStep("Fail", "Brand Type Code segment is NOT displayed as expected");
				}
			}else {
				flag=isDisplayedByXpath("//div[text()=' Lifestage ']", "Lifestage");
				if(flag) {
					System.out.println("Lifestage segment is displayed as expected");
					reportStep("Pass", "Lifestage segment is displayed as expected");
				}
				else {
					System.err.println("Lifestage segment is NOT displayed as expected");
					reportStep("Fail", "Lifestage segment is NOT displayed as expected");
				}
			}
		}
	}
}
