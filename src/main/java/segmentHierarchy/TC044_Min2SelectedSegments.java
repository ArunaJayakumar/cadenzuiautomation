package segmentHierarchy;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
public class TC044_Min2SelectedSegments extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if minimum 2 Selected attributes should be selected in Edit Segment Hierarchy";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void min2SelectedSegments() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy");

		clickByCSS("div.segment-header-section > svg", "Edit Hierarchy");
		Thread.sleep(1000);
		if(isDisplayedByXpath("//div[text()='Select Discovery Hierarchy']","Segment Hierarchy")) {
			reportStep("Pass", "Opened Edit Segment Hierarchy");
			System.out.println("Opened Edit Segment Hierarchy");

			List<WebElement> selectedSegObj = driver.findElements(By.xpath("//div[@id='editDisHierarchy-selectedAttr-droppable']//span[contains(@id,'-selectedAttrLabel')]"));
			ArrayList<String> selectedSegList = new ArrayList<String>();
			for(WebElement i:selectedSegObj) {
				selectedSegList.add(i.getText());
			}
			if(selectedSegList.size()==2) {
				verifyMinSegments();				
			}else if(selectedSegList.size()==3) {
				clickByXpath("//span[@title='Un-select']", "Un-select");//clicking unselect to remove 2 attributes
				verifyMinSegments();
			}else {
				reportStep("Fail", "ERROR: Less than 2 OR More than 3 attributes are displayed under selected attributes");
				System.err.println("ERROR: Less than 2 OR More than 3 attributes are displayed under selected attributes");
			}
		}else {
			reportStep("Fail", "ERROR: Edit Segment Hierarchy popup is not displayed");
			System.err.println("ERROR: Edit Segment Hierarchy popup is not displayed");
		}
	}
	public void verifyMinSegments() throws InterruptedException {
		clickByXpath("//span[@title='Un-select']", "Un-select");//clicking unselect to remove 1 attributes
		clickByXpath("//span[text()='Submit']", "Submit Segment");
		Thread.sleep(2000);
		if(isDisplayedByXpath("//div[text()='Please select minimum two attributes for discovery hierarchy']", "Minimum 2 Attributes Alert Message")) {
			reportStep("Pass", "'Please select minimum two attributes for discovery hierarchy' alert message is displayed");
			System.out.println("'Please select minimum two attributes for discovery hierarchy' alert message is displayed");
		}else {
			reportStep("Fail", "'Please select minimum two attributes for discovery hierarchy' alert message is not displayed");
			System.err.println("'Please select minimum two attributes for discovery hierarchy' alert message is not displayed");
		}
	}
}
