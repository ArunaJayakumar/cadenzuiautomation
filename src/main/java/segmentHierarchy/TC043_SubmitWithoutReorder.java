package segmentHierarchy;

import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
public class TC043_SubmitWithoutReorder extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if Edit Segment Hierarchy can be submitted without any changes";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void submitWithoutReorder() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy");

		String defSegText = driver.findElement(By.xpath("//a/div[@class='segment-columnname']")).getText();
		clickByCSS("div.segment-header-section > svg", "Edit Hierarchy");
		Thread.sleep(1000);

		if(isDisplayedByXpath("//div[text()='Select Discovery Hierarchy']","Segment Hierarchy")) {
			reportStep("Pass", "Opened Edit Segment Hierarchy");
			System.out.println("Opened Edit Segment Hierarchy");

			clickByXpath("//span[text()='Submit']", "Submit Segment");
			reportStep("Pass", "Submitted Edit Segment Hierarchy without any changes");
			System.out.println("Submitted Edit Segment Hierarchy without any changes");
			explicitWaitForVisibility("//div[text()='Hierarchical Segments']", "Segment Hierarchy");

			String audienceSegText = driver.findElement(By.xpath("//a/div[@class='segment-columnname']")).getText();
			if(audienceSegText.equalsIgnoreCase(defSegText)) {
				reportStep("Pass", "As expected, no changes occured in Hierarchy Segment");
				System.out.println("As expected, no changes occured in Hierarchy Segment");
			}else {
				reportStep("Fail", "ERROR: Unexpectedly Hierarchy Segment as changed");
				System.err.println("ERROR: Unexpectedly Hierarchy Segment as changed");
			}
		}else {
			reportStep("Fail", "ERROR: Edit Segment Hierarchy popup is not displayed");
			System.err.println("ERROR: Edit Segment Hierarchy popup is not displayed");
		}
	}
}
