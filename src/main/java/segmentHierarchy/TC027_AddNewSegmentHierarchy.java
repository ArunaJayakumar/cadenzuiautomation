package segmentHierarchy;

import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;
public class TC027_AddNewSegmentHierarchy extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if new Segments can be added to Segment Hierarchy";
		author="Aruna";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void addSegmentHierarchy() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy");

		clickByCSS("div.segment-header-section > svg", "Edit Hierarchy");
		try {
			if(isDisplayedByXpath("//div[text()='Select Discovery Hierarchy']","Segment Hierarchy")) {
				System.out.println("Select Discovery Hierarchy dialog box is displayed");
				reportStep("Pass","Select Discovery Hierarchy dialog box is displayed");
			}
		} catch (Exception e) {
			System.err.println("Select Discovery Hierarchy dialog is not opened");
			reportStep("Fail","Select Discovery Hierarchy dialog is not opened");
		}

		clickByXpath("//span[@title='Un-select']", "Un-select");
		/*clickByXpath("//span[text()='Search for customer attribute']", "Enter segment name");
			enterByXpath("//span[text()='Search for customer attribute']", "Mobile Wallet User Bucket", "Enter segment name");*/
		enterByXpath("//input[@id='editDisHierarchy-searchAttribute-textField']", "Payment Category Code", "SegmentName");
		clickByXpath("//*[@id='editDisHierarchy-Payment-Category-Code-availableAttrLabel']/span[@class='segments-move-icon-right']", "Payment Category Code Move to Right");
		clickByXpath("//*[@id='editDisHierarchy-Payment-Category-Code-selectedAttr-moveup-lessIcon']", "Move Payment Category Code Up");
		clickByXpath("//span[text()='Submit']", "Submit Segment");
		explicitWaitForVisibility("//div[@id='aud-insightSegment-container']//div[contains(text(),'Payment Category Code')]", "Payment Category Code Segment");
		Thread.sleep(3000);
		if(isDisplayedByXpath("//div[@id='aud-insightSegment-container']//div[contains(text(),'Payment Category Code')]","Payment Category Code Segment")) {
			reportStep("Pass", "Added Segment successfully");
		}else {
			reportStep("Fail", "Adding Segment was unsuccessful");
		}
	}
}
