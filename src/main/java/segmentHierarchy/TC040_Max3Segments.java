package segmentHierarchy;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.ProjectWrappers;
public class TC040_Max3Segments extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){

		testName = this.getClass().getSimpleName();
		description="To verify if maximum 3 segments can be selected";
		author="Avinash";
		category="Regression";
	}

	public WebDriver driver;

	@Test
	public void max3Segments() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(2000);

		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy");

		//		Verify maxmimum of 3 selected attributes in Segment Hierarchy
		clickByXpath("//*[@id='aud-insightSegment-moreIcon']", "Edit Segment Hierarchy");
		reportStep("Pass", "Opened Edit Segment Hierarchy");
		System.out.println("Opened Edit Segment Hierarchy");
		List<WebElement> selectedSegObj = driver.findElements(By.xpath("//div[@id='editDisHierarchy-selectedAttr-droppable']//span[contains(@id,'-selectedAttrLabel')]"));
		ArrayList<String> selectedSegList = new ArrayList<String>();
		for(WebElement i:selectedSegObj) {
			selectedSegList.add(i.getText());
		}
		if(selectedSegList.size()==2) {
			clickByXpath("//*[@id='editDisHierarchy-Sim-Lte-Capable-Indicator-availableAttrSelectMovRight-icon']", "Sim LTE Segment Right Move");
			verifyMaxSelectSeg();
		}else if(selectedSegList.size()==3) {
			verifyMaxSelectSeg();
		}else {
			reportStep("Fail", "ERROR: 2 or 3 segments are not selected by default");
			System.err.println("ERROR: 2 or 3 segments are not selected by default");
		}
	}

	public void verifyMaxSelectSeg() {
		if(isEnabledByXpath("//div[contains(@class,'draggable-segment')]/span[contains(@class,'disabled')]", "Unselected Attributes Move Disabled")) {
			reportStep("Pass", "As expected, Maximum of only 3 attributes can be selected in Segement Hierarchy");
			System.out.println("As expected, Maximum of only 3 attributes can be selected in Segement Hierarchy");
			clickByXpath("//span[text()='Submit']", "Submit Segment");
			explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
			isDisplayedByXpath("//div[text()='Hierarchical Segments']", "Segment Hierarchy");
		}else {
			reportStep("Fail", "ERROR: More than 3 attribute are allowed for selection under Segment Hierarchy");
		}
	}
}
