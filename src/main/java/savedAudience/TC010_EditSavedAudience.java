package savedAudience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC010_EditSavedAudience extends ProjectWrappers {


	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To Inline edit in Saved Audience page";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;
	int i=0;

	@Test(groups= {"SmokeTest"})
	public void editSavedAudienceInline() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
		reportStep("pass", "Clicked on Saved Audiences icon");
		System.out.println("Clicked on Saved Audiences icon");
		enterByXpath("//input[@placeholder='Search Audiences']", "Created", "Search Audience");

		clickByXpath("//*[@id='saveAud-editIcon']", "Edit Icon");
		try {
			if(driver.findElement(By.xpath("//input[@placeholder='Audience Name']")).isEnabled()) {
				enterByXpath("//input[@placeholder='Audience Name']", "1", "Audience Name inline edit");
				reportStep("Pass", "Updated the Audience Name inline");
				clickByXpath("//*[@id='saveAud-checkIcon']", "Save Edit Audience Button");
				takeSnap(testName);
				Thread.sleep(2000);
				try {
					if(driver.findElement(By.xpath("//div[contains(text(),'Updated the audience successfully')]")).isDisplayed())
						reportStep("Pass", "'Updated the audience successfully' message is displayed");
					else
						reportStep("Fail", "Update is not saved and success message not displayed");
				} catch (Exception e) {
					reportStep("Fail", "Update success alert message not displayed");
				}
				takeSnap(testName);
			}
			else {
				reportStep("Fail", "Not able to edit and save the audience successfully");
				takeSnap(testName);
			}
		} catch (Exception e) {
			reportStep("Fail", "Saved Audience Name is not in endit status");
		}
	}
}