package savedAudience;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.ProjectWrappers;

public class TC005_CreateSavedAudience extends ProjectWrappers{

	@BeforeClass
	public void beforeClass() {

		testName = this.getClass().getSimpleName();
		description="To create a saved audience and verify";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void createSavedAudience() throws InterruptedException ,IOException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='aud-disc-filter-dropdown']", "Attribute Filter Dropdown");
		Thread.sleep(1000);
		clickByXpath("//li[text()='Income Segment Code']", "Income Segment Code");
		clickByXpath("//*[@id='condition-select-outlined']", "Condition Dropdown");
		clickByXpath("//li[text()='=='] | //li[text()='Equals']", "Equal Condition");
		clickByXpath("//*[@id='attribute-select-outlined']", "Attribute Value Dropdown");
		clickByXpath("//li[text()='AB']", "Attribute Value");
		clickByCSS("div[id='aud-disc-audience-search-add-button']>svg", "Add Filter");
		Thread.sleep(2000);
		clickByXpath("//button[@id='aud-disc-audience-search-button']/span", "Search Button");
		try {
			if(driver.findElement(By.xpath("//span[contains(text(),'Income Segment Code')]")).isDisplayed()) {
				reportStep("Pass","Filter added successfully");
				System.out.println("Filter added successfully");
			}

		} catch (NoSuchElementException e) {
			reportStep("Fail","Filter was not added");
			System.err.println("Filter was not added");
		}	

		String audienceSize=getTextByXpath("//h4[@id='audience-sizeValue']","Audience Size");
		reportStep("Pass", "Audience size is: "+audienceSize);
		System.out.println("Audience size is: "+audienceSize);

		clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
		clickByXpath("//div[@id='aud-disc-audience-search-save-aud-button']//span", "Save");
		for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
			driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
		enterByXpath("//input[@id='name']", "Created", "Audience Name");

		WebElement createNew=driver.findElement(By.xpath("//button[@id='saveAud-dialogModel-saveBtn']/span")); 
		JavascriptExecutor exec4=(JavascriptExecutor)driver;
		exec4.executeScript("arguments[0].click();", createNew);

		Thread.sleep(2000);
		try {
			if(driver.findElement(By.xpath("//*[text()='Saved the audience successfully']")).isDisplayed())
				reportStep("pass", "The audience is saved and appropriate success message is displayed");
			else
				reportStep("fail", "Audience is not saved ");
		}
		catch(Exception e) {
			reportStep("Fail","Save Audience alert message is not saved");

		}
	}
}
