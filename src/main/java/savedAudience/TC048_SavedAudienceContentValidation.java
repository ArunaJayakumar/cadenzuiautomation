package savedAudience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC048_SavedAudienceContentValidation extends ProjectWrappers {


	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To validate Saved Audience contents";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void savedAudContentValidation() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		createsSavedAudTestData();

		//		Expected column names
		String[] exptabnames = {"Actions","Audience Name", "Audience Size", "Profile Date", "Last Updated", "Description","Segments"};
		ArrayList<String> expectedColumnNames = new ArrayList<String>();
		for(String i:exptabnames) {
			expectedColumnNames.add(i);
		}
		System.out.println("Expected Column Names - "+expectedColumnNames);

		//		Extracted Column Names
		List<WebElement> actualColumnObj = driver.findElements(By.xpath("//th[@scope='col']//div"));
		ArrayList<String> actualColumnNames = new ArrayList<String>();
		actualColumnNames.add("Actions");
		for(WebElement i:actualColumnObj) {
			actualColumnNames.add(i.getText());
		}
		System.out.println("Actual Column Names - "+actualColumnNames);

		//		Validate saved audience page contents
		if(actualColumnNames.equals(expectedColumnNames)) {
			reportStep("Pass", "SUCCESS: All column names are correct");
			System.out.println("SUCCESS: All column names are correct");
			if(isDisplayedByXpath("//*[@id='saveAud-editIcon']", "Edit Icon")) {
				reportStep("Pass", "SUCCESS: Edit Icon in Saved Audience is displayed");
				System.out.println("SUCCESS: Edit Icon in Saved Audience is displayed");
				//				if(isDisplayedByXpath("//*[@id='saveAud-deleteIcon']", "Delete Icon")) {
				if(isDisplayedByXpath("//tbody//button[2]//span", "Delete Icon")) {
					reportStep("Pass", "SUCCESS: Delete Icon in Saved Audience is displayed");
					System.out.println("SUCCESS: Delete Icon in Saved Audience is displayed");
					if(isDisplayedByXpath("//*[@title='Last Page']", "Last Page Icon")) {
						reportStep("Pass", "SUCCESS: Pagination in Saved Audience is displayed");
						System.out.println("SUCCESS: Pagination in Saved Audience is displayed");
					}else {
						reportStep("Fail", "SUCCESS: Pagination in Saved Audience is not displayed");
						System.err.println("SUCCESS: Pagination in Saved Audience is not displayed");
					}
				}else {
					reportStep("Fail", "ERROR: Delete Icon in Saved Audience is not displayed");
					System.err.println("ERROR: Delete Icon in Saved Audience is not displayed");
				}
			}else {
				reportStep("Fail", "ERROR: Edit Icon in Saved Audience is not displayed");
				System.err.println("ERROR: Edit Icon in Saved Audience is not displayed");
			}
		}else {
			reportStep("Fail", "ERROR: Columns do not match with the expected column name");
			System.err.println("ERROR: Columns do not match with the expected column name");
		}
		cleanSavedAudTestData();
	}

	public void createsSavedAudTestData() {
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
			clickByXpath("//div[@id='aud-disc-audience-search-save-aud-button']//span", "Save");
			for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
				driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
			enterByXpath("//input[@id='name']", "SavedAudDemoData", "Audience Name");
			clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span", "Save Audience");
			explicitWaitForVisibility("//*[text()='Saved the audience successfully']", "Saved Audience Alert");
			if(isDisplayedByXpath("//*[text()='Saved the audience successfully']", "Saved Audience Alert")) {
				clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
				enterByXpath("//input[@placeholder='Search Audiences']", "SavedAudDemoData", "Search Audiences");
			}else {
				reportStep("Fail", "ERROR: Saved Audeince Alert Message was not displayed");
				System.err.println("ERROR: Saved Audeince Alert Message was not displayed");
			}
		}else {
			reportStep("Fail", "ERROR: Audience Discovery Page is not loaded");
			System.err.println("ERROR: Audience Discovery Page is not loaded");
		}
	}

	public void cleanSavedAudTestData() throws InterruptedException {
		clickByXpath("//*[@id='nav-attributeGlossary-icon']", "Attribute Glossary Nav Icon");
		explicitWaitForVisibility("//th[text()='Attribute Name']", "Glossary Attribute Name Column");
		clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
		explicitWaitForVisibility("//th[@scope='col']/span", "Saved Audience Table");
		enterByXpath("//input[@placeholder='Search Audiences']", "SavedAudDemoData", "Search Audiences");

		clickByXpath("//tbody//button[2]//span", "Delete Icon");
		clickByXpath("//span[text()='Confirm']", "Save Edit Audience Button");
		Thread.sleep(1000);
		if(isDisplayedByXpath("//div[contains(text(),'Deleted the audience successfully')]", "Delete Saved Audience Alert Message")) {
			reportStep("Pass", "Test Data is cleaned successfully");
			System.out.println("Test Data is cleaned successfully");
		}else {
			reportStep("Fail", "Delete action is not performed and success message not displayed");
			System.err.println("Delete action is not performed and success message not displayed");
		}
	}
}
