package savedAudience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC049_SavedAudienceSortingValidation extends ProjectWrappers {


	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To validate Saved Audience sorting functionalities";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test(groups= {"SmokeTest"})
	public void savedAudSortingValidation() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		createsSavedAudTestData();
		Thread.sleep(2000);

		//		Extract once Ascending sorted first row
		String firstAscAudNameFirstRowObj = extractWebElmList("//div[text()='Audience Name']", "Audience Name Column",1);
		String firstAscAudSizeFirstRowObj = extractWebElmList("//div[text()='Audience Size']", "Audience Size Column",1);
		String firstAscProfDateFirstRowObj = extractWebElmList("//div[text()='Profile Date']", "Profile Date Column",1);
		String firstAscLastUpdatedFirstRowObj = extractWebElmList("//div[text()='Last Updated']", "Last Updated Column",1);
		String firstAscDescriptionFirstRowObj = extractWebElmList("//div[text()='Description']", "Description Column",1);
		String firstAscSegmentsFirstRowObj = extractWebElmList("//div[text()='Segments']", "Segments Column",1);

		//		Extract once Descending sorted first row
		String firstDscAudNameFirstRowObj = extractWebElmList("//div[text()='Audience Name']", "Audience Name Column",2);
		String firstDscAudSizeFirstRowObj = extractWebElmList("//div[text()='Audience Size']", "Audience Size Column",2);
		String firstDscProfDateFirstRowObj = extractWebElmList("//div[text()='Profile Date']", "Profile Date Column",2);
		String firstDscLastUpdatedFirstRowObj = extractWebElmList("//div[text()='Last Updated']", "Last Updated Column",2);
		String firstDscDescriptionFirstRowObj = extractWebElmList("//div[text()='Description']", "Description Column",2);
		String firstDscSegmentsFirstRowObj = extractWebElmList("//div[text()='Segments']", "Segments Column",2);

		//		Extract twice Ascending sorted first row
		String secondAscAudNameFirstRowObj = extractWebElmList("//div[text()='Audience Name']", "Audience Name Column",4);
		String secondAscAudSizeFirstRowObj = extractWebElmList("//div[text()='Audience Size']", "Audience Size Column",4);
		String secondAscProfDateFirstRowObj = extractWebElmList("//div[text()='Profile Date']", "Profile Date Column",4);
		String secondAscLastUpdatedFirstRowObj = extractWebElmList("//div[text()='Last Updated']", "Last Updated Column",4);
		String secondAscDescriptionFirstRowObj = extractWebElmList("//div[text()='Description']", "Description Column",4);
		String secondAscSegmentsFirstRowObj = extractWebElmList("//div[text()='Segments']", "Segments Column",4);

		//		Extract twice Descending sorted first row
		String secondDscAudNameFirstRowObj = extractWebElmList("//div[text()='Audience Name']", "Audience Name Column",5);
		String secondDscAudSizeFirstRowObj = extractWebElmList("//div[text()='Audience Size']", "Audience Size Column",5);
		String secondDscProfDateFirstRowObj = extractWebElmList("//div[text()='Profile Date']", "Profile Date Column",5);
		String secondDscLastUpdatedFirstRowObj = extractWebElmList("//div[text()='Last Updated']", "Last Updated Column",5);
		String secondDscDescriptionFirstRowObj = extractWebElmList("//div[text()='Description']", "Description Column",5);
		String secondDscSegmentsFirstRowObj = extractWebElmList("//div[text()='Segments']", "Segments Column",5);

		//		Ascending Sort operation validation
		String sortType = "Ascending";
		columnSortValidation(firstAscAudNameFirstRowObj, secondAscAudNameFirstRowObj, "Audience Name", sortType);
		columnSortValidation(firstAscAudSizeFirstRowObj, secondAscAudSizeFirstRowObj, "Audience Size", sortType);
		columnSortValidation(firstAscProfDateFirstRowObj, secondAscProfDateFirstRowObj, "Profile Date", sortType);
		columnSortValidation(firstAscLastUpdatedFirstRowObj, secondAscLastUpdatedFirstRowObj, "Last Updated", sortType);
		columnSortValidation(firstAscDescriptionFirstRowObj, secondAscDescriptionFirstRowObj, "Description", sortType);
		columnSortValidation(firstAscSegmentsFirstRowObj, secondAscSegmentsFirstRowObj, "Segments", sortType);

		//		Descending Sort operation validation
		sortType = "Descending";
		columnSortValidation(firstDscAudNameFirstRowObj, secondDscAudNameFirstRowObj, "Audience Name", sortType);
		columnSortValidation(firstDscAudSizeFirstRowObj, secondDscAudSizeFirstRowObj, "Audience Size", sortType);
		columnSortValidation(firstDscProfDateFirstRowObj, secondDscProfDateFirstRowObj, "Profile Date", sortType);
		columnSortValidation(firstDscLastUpdatedFirstRowObj, secondDscLastUpdatedFirstRowObj, "Last Updated", sortType);
		columnSortValidation(firstDscDescriptionFirstRowObj, secondDscDescriptionFirstRowObj, "Description", sortType);
		columnSortValidation(firstDscSegmentsFirstRowObj, secondDscSegmentsFirstRowObj, "Segments", sortType);

		cleanSavedAudTestData();
	}

	public void createsSavedAudTestData() {
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			clickByXpath("//button[@id='aud-disc-audience-search-more-button']/span", "More");
			clickByXpath("//div[@id='aud-disc-audience-search-save-aud-button']//span", "Save");
			for(int i=0;i<7;i++) // 7 times iteration since 'default' char size = 7
				driver.findElement(By.xpath("//input[@id='name']")).sendKeys(Keys.BACK_SPACE);
			enterByXpath("//input[@id='name']", "SavedAudDemoData", "Audience Name");
			clickByXpath("//button[@id='saveAud-dialogModel-saveBtn']/span", "Save Audience");
			explicitWaitForVisibility("//*[text()='Saved the audience successfully']", "Saved Audience Alert");
			if(isDisplayedByXpath("//*[text()='Saved the audience successfully']", "Saved Audience Alert")) {
				clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
			}else {
				reportStep("Fail", "ERROR: Saved Audeince Alert Message was not displayed");
				System.err.println("ERROR: Saved Audeince Alert Message was not displayed");
			}
		}else {
			reportStep("Fail", "ERROR: Audience Discovery Page is not loaded");
			System.err.println("ERROR: Audience Discovery Page is not loaded");
		}
	}

	public void cleanSavedAudTestData() throws InterruptedException {
		clickByXpath("//*[@id='nav-attributeGlossary-icon']", "Attribute Glossary Nav Icon");
		explicitWaitForVisibility("//th[text()='Attribute Name']", "Glossary Attribute Name Column");
		clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
		explicitWaitForVisibility("//th[@scope='col']/span", "Saved Audience Table");
		enterByXpath("//input[@placeholder='Search Audiences']", "SavedAudDemoData", "Search Audiences");

		clickByXpath("//tbody//button[2]//span", "Delete Icon");
		clickByXpath("//span[text()='Confirm']", "Save Edit Audience Button");
		Thread.sleep(1000);
		if(isDisplayedByXpath("//div[contains(text(),'Deleted the audience successfully')]", "Delete Saved Audience Alert Message")) {
			reportStep("Pass", "Test Data is cleaned successfully");
			System.out.println("Test Data is cleaned successfully");
		}else {
			reportStep("Fail", "Delete action is not performed and success message not displayed");
			System.err.println("Delete action is not performed and success message not displayed");
		}
	}

	public String extractWebElmList(String xpath, String fieldName, int clickCountToExtractRelevantSortData){
		for(int i=0;i<clickCountToExtractRelevantSortData;i++) {
			clickByXpath(xpath, fieldName);
		}
		return driver.findElement(By.xpath("//tr[@index='0']/td[contains(@class,'-alignLeft')]/a")).getText();
	}
	public void columnSortValidation(String firstSortObj, String secondSortObj, String columnName, String sortType) {
		if(secondSortObj.equals(firstSortObj)) {
			System.out.println("firstSortObj - "+firstSortObj);
			System.out.println("secondSortObj - "+secondSortObj);
			reportStep("Pass", "firstSortObj - "+firstSortObj);
			reportStep("Pass", "secondSortObj - "+secondSortObj);
			reportStep("Pass", "SUCCESS: "+sortType+" Sort for "+columnName+" is working as expected");
			System.out.println("SUCCESS: "+sortType+" Sort for "+columnName+" is working as expected");
		}else {
			System.out.println("firstSortObj - "+firstSortObj);
			System.out.println("secondSortObj - "+secondSortObj);
			reportStep("Pass", "firstSortObj - "+firstSortObj);
			reportStep("Pass", "secondSortObj - "+secondSortObj);
			reportStep("Fail", "ERROR: "+sortType+" Sort for "+columnName+" is not working as expected");
			System.err.println("ERROR: "+sortType+" Sort for "+columnName+" is not working as expected");
		}
	}
}
