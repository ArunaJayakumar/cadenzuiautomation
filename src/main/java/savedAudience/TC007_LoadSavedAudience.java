package savedAudience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC007_LoadSavedAudience extends ProjectWrappers {


	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To load an already Saved Audience";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;
	int i=0;

	@Test(groups= {"SmokeTest"})
	public void loadSavedAudience() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
		enterByXpath("//input[@placeholder='Search Audiences']", "Created", "Search Audiences");
		clickByXpath("//a[contains(text(),'Created')]", "Created Audience Name");
		Thread.sleep(7000);
		if(driver.findElement(By.xpath("//div[contains(text(),'Audience fetched successfully !')]")).isDisplayed())
			reportStep("Pass", "'Audience is fetched successully' message is displayed");
		else
			reportStep("Fail", "Success message is not displayed");
		try {
			boolean ai = driver.findElement(By.xpath("//p[text()='Created']")).isDisplayed();
			if(ai==true) 
				reportStep("Pass","Saved Audience is loaded in the Audience page successfully");
			else 
				reportStep("Fail","Saved Audience is not displayed");
		} catch (NoSuchElementException e) {
			reportStep("Fail","Saved Audience Name is not loaded");
		}
	}
}
