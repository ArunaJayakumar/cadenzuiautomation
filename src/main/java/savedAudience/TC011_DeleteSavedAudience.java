package savedAudience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC011_DeleteSavedAudience extends ProjectWrappers {


	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To Inline edit in Saved Audience page";
		author="Aruna";
		category="Smoke";
	}

	public WebDriver driver;
	int i=0;

	@Test(groups= {"SmokeTest"})
	public void deleteSavedAudienceInline() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName, true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");

		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");
		clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
		enterByXpath("//input[@placeholder='Search Audiences']", "Created1", "Search Audiences");

		clickByXpath("(//button[@class='MuiButtonBase-root MuiIconButton-root MuiIconButton-colorInherit'])[3]", "Delete Icon");
		clickByXpath("//span[text()='Confirm']", "Save Edit Audience Button");
		Thread.sleep(2000);
		try {
			if(driver.findElement(By.xpath("//div[contains(text(),'Deleted the audience successfully')]")).isDisplayed())
				reportStep("Pass", "'Deleted the audience successfully' message is displayed");
			else
				reportStep("Fail", "Delete action is not performed and success message not displayed");
		} catch (NoSuchElementException e) {
			reportStep("Fail", "Delete success alert message is not displayed");
		}
	}
}