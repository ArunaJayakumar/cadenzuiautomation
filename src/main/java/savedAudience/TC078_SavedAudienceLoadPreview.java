package savedAudience;

import utils.ProjectWrappers;
import java.io.IOException;
import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC078_SavedAudienceLoadPreview extends ProjectWrappers {

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
		testName = this.getClass().getSimpleName();
		description="To verify if the audience preview can be accessed post loading saved audience";
		author="Avinash";
		category="Smoke";
	}

	public WebDriver driver;

	@Test
	public void savedAudienceLoadPreview() throws InterruptedException, IOException, ParseException {

		driver=getDriver();
		invokeApp(testName,true);
		Thread.sleep(5000);
		//		Force Page Refresh by toggling between Concept and Attributes Search. Mainly for 35box
		explicitWaitForVisibility("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-concept']/span", "Concepts Toggle Button");
		clickByXpath("//button[@id='tb-aud-disc-attribute']/span", "Attributes Toggle Button");
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize");

		clickByXpath("//*[@id='save_5_']", "Saved Audience Icon");
		enterByXpath("//input[@placeholder='Search Audiences']", "doNotDelete", "Search Audiences");
		clickByXpath("//a[contains(text(),'doNotDelete')]", "Audience Name");
		Thread.sleep(1000);
		explicitWaitForVisibility("//h6[@id='audience-sizeTxt']", "AudienceSize");
		if(isDisplayedByXpath("//h6[@id='audience-sizeTxt']", "AudienceSize")) {
			System.out.println("SUCCESS: Audience Discovery is loaded");
			reportStep("Pass", "SUCCESS: Audience Discovery is loaded");
			clickByXpath("//*[@id='aud-result-previewOption-tab']/span", "Audience Preview and Export");
			explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");
			//			Navigate to Preview to extract attributes
			clickByXpath("//*[@id='target-audience-previewOptionBtn']", "Preview Popup Button");
			explicitWaitForVisibility("//*[@id='audience-preview-dialog-title-Grid']", "Select attribute for Preview Popup");
			if(isDisplayedByXpath("//*[@id='audience-preview-dialog-title-Grid']", "Select attribute for Preview Popup")) {
				System.out.println("SUCCESS: Audience Preview Popup is loaded");
				reportStep("Pass", "SUCCESS: Audience Preview Popup is loaded");
				clickByXpath("//*[@id='audience-preview-export-select-all']", "Select All Preview");
				clickByXpath("//*[@id='auidence-preview-dialog-btn-add']/span", "Add Preview");
				clickByXpath("//button[@id='audience-preview-dialog-submit-btn']", "Preview Button");
				Thread.sleep(1000);
				explicitWaitForVisibility("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header");
				if(isDisplayedByXpath("//*[@id='target-audience-tableHeaderRow']", "Audience Preview Table Header")) {
					System.out.println("SUCCESS: Audience Preview section is loaded with all attributes displayed post loading saved audience");
					reportStep("Pass", "SUCCESS: Audience Preview section is loaded with all attributes displayed post loading saved audience");

				}else {
					reportStep("Fail", "ERROR: Audience Preview section is not loaded with all attributes post loading saved audience");
					System.err.println("ERROR: Audience Preview section is not loaded with all attributes post loading saved audience");
				}
			}else {
				reportStep("Fail", "ERROR: Audience Preview popup is not loaded");
				System.err.println("ERROR: Audience Preview popup is not loaded");
			}
		}else {
			reportStep("Fail", "ERROR: Audience Discovery is not loaded");
			System.err.println("ERROR: Audience Discovery is not loaded");
		}
	}
}
